package com.xianzaishi.customercenter.client.customerservice;

import java.util.List;

import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO;
import com.xianzaishi.customercenter.client.customerservice.query.CustomerserviceQuery;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 客服接口
 * 
 * @author zhancang
 */
public interface CustomerService {

  /**
   * 增加一个客服任务
   * 
   * @param customerServiceTask
   * @return
   */
  public Result<Long> insertCustomerServiceTask(CustomerServiceTaskDTO customerServiceTask);

  /**
   * 更新一个客服任务
   * 
   * @param customerServiceTask
   * @return
   */
  public Result<Boolean> updateCustomerServiceTask(CustomerServiceTaskDTO customerServiceTask);

  /**
   * 查询符合条件的客服任务
   * 
   * @param query
   * @return
   */
  public Result<List<CustomerServiceTaskDTO>> queryCustomerServiceTask(CustomerserviceQuery query);
  
  /**
   * 查询符合条件的客服任务总数
   * 
   * @param query
   * @return
   */
  public Result<Integer> queryCustomerServiceTaskCount(CustomerserviceQuery query);
  
  /**
   * 查询符合条件的客服任务
   * 
   * @param query
   * @return
   */
  public Result<CustomerServiceTaskDTO> queryCustomerServiceTaskById(Long id);


}
