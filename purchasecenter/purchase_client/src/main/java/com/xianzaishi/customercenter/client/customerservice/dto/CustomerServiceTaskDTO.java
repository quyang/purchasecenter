package com.xianzaishi.customercenter.client.customerservice.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 客服任务对象
 * 
 * @author zhancang
 */
public class CustomerServiceTaskDTO implements Serializable {

  private static final long serialVersionUID = -255301589650608667L;

  private Date now = new Date();

  private Long id;

  /**
   * 任务类型
   */
  private Short type = CustomerServiceTaskTypeConstants.TYPE_REFUND;

  /**
   * 任务标题
   */
  private String title;

  /**
   * 任务对应业务id
   */
  private String bizId;

  /**
   * 任务状态
   */
  private Short status = CustomerServiceTaskStatusConstants.WAIT_DEAL;

  /**
   * 处理事件客服id
   */
  private Integer serviceUser;

  /**
   * 上帝id
   */
  private Long customerUser;

  /**
   * 上帝要求的处理动作
   */
  private List<ProcessDTO> customerRequireProcess;

  /**
   * 客服实际处理的动作
   */
  private List<ProcessDTO> serviceResponseProcess;
  
  /**
   * 提交个客服的证据
   */
  private List<ProofDataDTO> customerProofList;

  /**
   * 处理方式类型
   */
  private Short processType ;

  /**
   * 创建时间
   */
  private Date gmtCreate = now;

  /**
   * 修改时间
   */
  private Date gmtModified = now;


  /**
   * 退款金额 客服妹子填写后会传的
   */
  private Long refoudAmount;

  /**
   * 退款原因
   */
  private String refoudReason;

  /**
   * 赠送的优惠券id
   */
  private Long couponId;

  public Long getCouponId() {
    return couponId;
  }

  public void setCouponId(Long couponId) {
    this.couponId = couponId;
  }

  public String getRefoudReason() {
    return refoudReason;
  }

  public void setRefoudReason(String refoudReason) {
    this.refoudReason = refoudReason;
  }

  public Long getRefoudAmount() {
    return refoudAmount;
  }

  public void setRefoudAmount(Long refoudAmount) {
    this.refoudAmount = refoudAmount;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public String getBizId() {
    return bizId;
  }

  public void setBizId(String bizId) {
    this.bizId = bizId == null ? null : bizId.trim();
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Integer getServiceUser() {
    return serviceUser;
  }

  public void setServiceUser(Integer serviceUser) {
    this.serviceUser = serviceUser;
  }

  public Long getCustomerUser() {
    return customerUser;
  }

  public void setCustomerUser(Long customerUser) {
    this.customerUser = customerUser;
  }

  public List<ProcessDTO> getCustomerRequireProcess() {
    return customerRequireProcess;
  }

  public void setCustomerRequireProcess(List<ProcessDTO> customerRequireProcess) {
    this.customerRequireProcess = customerRequireProcess;
  }

  public List<ProcessDTO> getServiceResponseProcess() {
    return serviceResponseProcess;
  }

  public void setServiceResponseProcess(List<ProcessDTO> serviceResponseProcess) {
    this.serviceResponseProcess = serviceResponseProcess;
  }

  public Short getProcessType() {
    return processType;
  }

  public void setProcessType(Short processType) {
    this.processType = processType;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }
  
  public List<ProofDataDTO> getCustomerProofList() {
    return customerProofList;
  }

  public void setCustomerProofList(List<ProofDataDTO> customerProofList) {
    this.customerProofList = customerProofList;
  }

  /**
   * 客服事件类型枚举
   * 
   * @author zhancang
   */
  public static class CustomerServiceTaskTypeConstants implements Serializable {

    private static final long serialVersionUID = -346413454453122453L;

    /**
     * 退款方式处理
     */
    public static final Short TYPE_REFUND = 0;


    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return TYPE_REFUND.equals(parameter);
    }
  }

  /**
   * 客服事件类型枚举
   * 
   * @author zhancang
   */
  public static class CustomerServiceTaskStatusConstants implements Serializable {

    private static final long serialVersionUID = -346413412453122453L;

    /**
     * 待处理
     */
    public static final Short WAIT_DEAL = 0;

    /**
     * 处理中
     */
    public static final Short DEALING = 1;

    /**
     * 处理完成
     */
    public static final Short END = 2;

    /**
     * 审核完成
     */
    public static final Short AUDIT = 3;




    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return WAIT_DEAL.equals(parameter) || DEALING.equals(parameter) || END.equals(parameter);
    }
  }

  /**
   * 客服事件处理方式枚举
   * 
   * @author zhancang
   */
  public static class CustomerServiceTaskDealTypeConstants implements Serializable {

    private static final long serialVersionUID = -346413432453122453L;

    /**
     * 无需处理/拒绝处理
     */
    public static final Short NOT_NEED = 0;

    /**
     * 全部退款
     */
    public static final Short REFUND_ALL = 1;

    /**
     * 全部退款并赠送优惠券
     */
    public static final Short REFUND_ALL_AND_GIVE_COUPON = 2;

    /**
     * 部分退款
     */
    public static final Short REFUND_PART = 3;

    /**
     * 部分退款并优惠券
     */
    public static final Short REFUND_PART_AND_GIVE_COUPON = 4;

    /**
     * 赠送优惠券
     */
    public static final Short GIVE_COUPON = 5;



    /**
     * 正在处理中
     */
    public static final Short DEALING = 6;


    /**
     * 提交到审核
     */
    public static final Short COMMIT_AUDIT = 7;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return NOT_NEED.equals(parameter) || REFUND_ALL.equals(parameter)
          || REFUND_PART.equals(parameter) || REFUND_PART_AND_GIVE_COUPON.equals(parameter)
          || GIVE_COUPON.equals(parameter) || REFUND_ALL_AND_GIVE_COUPON.equals(parameter);
    }
  }
}
