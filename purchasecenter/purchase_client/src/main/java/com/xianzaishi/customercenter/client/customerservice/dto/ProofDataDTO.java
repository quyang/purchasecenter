package com.xianzaishi.customercenter.client.customerservice.dto;

import java.io.Serializable;

/**
 * 用户提交信息一个基本对象
 * @author zhancang
 */
public class ProofDataDTO implements Serializable{

  private static final long serialVersionUID = 4221564651999440646L;

  public static final int PIC_FLAG = 2;
  
  private Integer id;

  private String source;

  /**
   * type=2代表是一张图片、type=1代表是一体文字
   */
  private Integer type;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }
  
}
