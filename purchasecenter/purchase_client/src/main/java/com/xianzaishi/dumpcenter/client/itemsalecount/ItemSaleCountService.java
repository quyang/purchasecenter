package com.xianzaishi.dumpcenter.client.itemsalecount;

import java.util.List;

import com.xianzaishi.dumpcenter.client.itemsalecount.dto.ItemSaleCountDTO;
import com.xianzaishi.itemcenter.common.result.Result;

/**
 * 获取商品销量服务，当前只有查询最近7天商品销量接口
 * @author dongpo
 *
 */
public interface ItemSaleCountService {
  
  /**
   * 查询最近7天日均该商品的销量
   * @param skuId
   * @return 返回销量对象
   */
  Result<List<ItemSaleCountDTO>> queryItemSaleCountLast7Days(List<Long> skuIds);

}
