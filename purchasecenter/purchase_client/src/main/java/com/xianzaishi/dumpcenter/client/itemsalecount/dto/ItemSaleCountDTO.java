package com.xianzaishi.dumpcenter.client.itemsalecount.dto;

import java.io.Serializable;

public class ItemSaleCountDTO implements Serializable {
  

  /**
   * serial version UID
   */
  private static final long serialVersionUID = 5312193324797382019L;

  /**
   * 线上销量
   */
  private Double saleCountOnline;
  
  /**
   * 线下销量
   */
  private Double saleCountOffline;

  /**
   * sku id
   */
  private Long skuId;

  public Double getSaleCountOnline() {
    return saleCountOnline;
  }

  public void setSaleCountOnline(Double saleCountOnline) {
    this.saleCountOnline = saleCountOnline;
  }

  public Double getSaleCountOffline() {
    return saleCountOffline;
  }

  public void setSaleCountOffline(Double saleCountOffline) {
    this.saleCountOffline = saleCountOffline;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

}
