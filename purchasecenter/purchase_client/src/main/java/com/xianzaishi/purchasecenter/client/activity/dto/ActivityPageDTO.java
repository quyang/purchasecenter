package com.xianzaishi.purchasecenter.client.activity.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 首页数据对象
 * 
 * @author zhancang
 */
public class ActivityPageDTO implements Serializable {

  private static final long serialVersionUID = -2064491989577298750L;

  /**
   * 页面id
   */
  private Integer pageId;

  /**
   * 页面标题
   */
  private String title;

  /**
   * 楼层数据格式化
   */
  private List<ActivityStepDTO> stepList;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 修改时间
   */
  private Date gmtModified;

  public Integer getPageId() {
    return pageId;
  }

  public void setPageId(Integer pageId) {
    this.pageId = pageId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public List<ActivityStepDTO> getStepList() {
    return stepList;
  }

  public void setStepList(List<ActivityStepDTO> stepList) {
    this.stepList = stepList;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }
}
