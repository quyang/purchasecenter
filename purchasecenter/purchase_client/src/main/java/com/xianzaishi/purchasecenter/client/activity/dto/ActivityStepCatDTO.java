package com.xianzaishi.purchasecenter.client.activity.dto;

import java.io.Serializable;

/**
 * 楼层对象中的某个商品对象
 * 
 * @author zhancang
 */
public class ActivityStepCatDTO implements Serializable {

  private static final long serialVersionUID = 4945265156484310064L;

  /**
   * 商品的图片
   */
  private String picUrl;

  /**
   * 类目id
   */
  private Long catId;

  /**
   * 类目id
   */
  private String catName;
  
  /**
   * 配置活动跳转链接
   */
  private String jumpLink;
  
  /**
   * 跳转固定页面
   */
  private String jumpPage;

  public String getPicUrl() {
    return picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl;
  }

  public Long getCatId() {
    return catId;
  }

  public void setCatId(Long catId) {
    this.catId = catId;
  }

  public String getCatName() {
    return catName;
  }

  public void setCatName(String catName) {
    this.catName = catName;
  }

  public String getJumpLink() {
    return jumpLink;
  }

  public void setJumpLink(String jumpLink) {
    this.jumpLink = jumpLink;
  }

  public String getJumpPage() {
    return jumpPage;
  }

  public void setJumpPage(String jumpPage) {
    this.jumpPage = jumpPage;
  }
  
}
