package com.xianzaishi.purchasecenter.client.activity.dto;

import java.io.Serializable;

/**
 * 楼层对象中的某个图片对象
 * @author zhancang
 */
public class ActivityStepPicDTO implements Serializable{

  private static final long serialVersionUID = 2451327015642916974L;

  /**
   * 楼层图片的目标的地址
   */
  private String targetId;
  
  /**
   * 楼层图片的url
   */
  private String picUrl;
  
  /**
   * 图片标题
   */
  private String titleInfo;
  
  /**
   * 楼层图片的目标类型
   */
  private Short targetType = ActivityPicTypeConstants.PIC_DEFAULT;

  public String getTargetId() {
    return targetId;
  }

  public void setTargetId(String targetId) {
    this.targetId = targetId;
  }

  public String getPicUrl() {
    return picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl;
  }

  public Short getTargetType() {
    return targetType;
  }

  public void setTargetType(Short targetType) {
    this.targetType = targetType;
  }

  public String getTitleInfo() {
    return titleInfo;
  }

  public void setTitleInfo(String titleInfo) {
    this.titleInfo = titleInfo;
  }

  public static class ActivityPicTypeConstants implements Serializable {

    private static final long serialVersionUID = -3464189054053122453L;

    /**
     * 无目标地址固定图片
     */
    public static final Short PIC_DEFAULT = 0;
    
    /**
     * 指向活动图片
     */
    public static final Short PIC_SUBACTIVITYPAGE = 1;

    /**
     * 指向商品图片
     */
    public static final Short PIC_ITEM = 2;
    
    /**
     * 向固定链接发起请求
     */
    public static final Short REQUEST_URL = 3;
    
    /**
     * 跳转固定链接地址
     */
    public static final Short PIC_URL = 4;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return PIC_DEFAULT.equals(parameter) || PIC_SUBACTIVITYPAGE.equals(parameter) || PIC_ITEM.equals(parameter) || PIC_URL.equals(parameter) || REQUEST_URL.equals(parameter);
    }
  }
}
