package com.xianzaishi.purchasecenter.client.contract;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.contract.dto.ContractDTO;

/**
 * 合同接口，包括查询合同、插入合同以及更新合同
 * 
 * @author dongpo
 * 
 */
public interface ContractService {

  /**
   * 按照合同id查询合同信息
   * 
   * @param contractId
   * @return
   */
  public Result<ContractDTO> queryContractByContractId(Integer contractId);

  /**
   * 按照供应商id查询合同信息
   * 
   * @param supplierId
   * @return
   */
  public Result<ContractDTO> queryContractBySupplierId(Integer supplierId);

  /**
   * 插入合同信息
   * 
   * @param contractDto
   * @return
   */
  public Result<Integer> insertContract(ContractDTO contractDto);

  /**
   * 更新合同信息
   * 
   * @param contractDto
   * @return
   */
  public Result<Boolean> updateContract(ContractDTO contractDto);
}
