package com.xianzaishi.purchasecenter.client.equipment;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.equipment.dto.EquipmentDTO;

/**
 * 设备接口，包括查询设备信息，插入设备信息以及更新设备信息
 * 
 * @author dongpo
 * 
 */
public interface EquipmentService {

  /**
   * 根据设备号查询设备信息
   * 
   * @param equipmentId
   * @return
   */
  public Result<EquipmentDTO> queryEquipmentByEquipmentId(Integer equipmentId);


  /**
   * 通过设备名查询设备信息
   * 
   * @param name
   * @return
   */
  public Result<List<EquipmentDTO>> queryEquipmentByName(String name);

  /**
   * 插入设备信息
   * 
   * @param emEquipmentDto
   * @return
   */
  public Result<Integer> insertEquipment(EquipmentDTO equipmentDto);

  /**
   * 更新设备信息
   * 
   * @param emEquipmentDto
   * @return
   */
  public Result<Boolean> updateEquipment(EquipmentDTO equipmentDto);
}
