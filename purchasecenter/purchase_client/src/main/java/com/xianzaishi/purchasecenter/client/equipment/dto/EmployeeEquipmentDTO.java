package com.xianzaishi.purchasecenter.client.equipment.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 员工设备关联表字段
 * 
 * @author dongpo
 * 
 */
public class EmployeeEquipmentDTO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -6626129145431422275L;

  /**
   * 员工号
   */
  private Integer userId;

  /**
   * 设备号
   */
  private Integer equipmentId;

  /**
   * 状态(0：无关系,1：默认关系,2：借用关系)
   */
  private Short status = EmployeeEquipmentStatusConstants.EMPLOYEE_STATUS_NONE;


  /**
   * 创建时间
   */
  private Date gmtCreate;


  public Integer getUserId() {
    return userId == null ? 0 : userId;
  }

  public void setUserId(Integer userId) {
    Preconditions.checkNotNull(userId, "userId is null !");
    Preconditions.checkArgument(userId>0, "userId must be greater than 0");
    this.userId = userId;
  }

  public Integer getEquipmentId() {
    return equipmentId == null ? 0 : equipmentId;
  }

  public void setEquipmentId(Integer equipmentId) {
    Preconditions.checkNotNull(equipmentId, "equipmentId is null !");
    Preconditions.checkArgument(equipmentId>0, "equipmentId must be greater than 0");
    this.equipmentId = equipmentId;
  }


  public Short getStatus() {
    return status == null ? EmployeeEquipmentStatusConstants.EMPLOYEE_STATUS_NONE : status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "cost is null !");
    this.status = status;
  }


  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public static class EmployeeEquipmentStatusConstants implements Serializable {

    private static final long serialVersionUID = -3464189054053122453L;

    /**
     * 无关系
     */
    public static final Short EMPLOYEE_STATUS_NONE = 0;

    /**
     * 默认关系
     */
    public static final Short EMPLOYEE_STATUS_DEFAULT = 1;
    
    /**
     * 借用关系
     */
    public static final Short EMPLOYEE_STATUS_BORROW = 2;


    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == EMPLOYEE_STATUS_NONE || parameter == EMPLOYEE_STATUS_DEFAULT 
          || parameter == EMPLOYEE_STATUS_BORROW;
    }
  }

}
