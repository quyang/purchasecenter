package com.xianzaishi.purchasecenter.client.logmonitor.dto;

import java.io.Serializable;
import java.util.Date;

public class LogMonitorDTO implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -4020587658676041084L;

  /**
   * 主键id
   *
   */
  private Integer id;

  /**
   * 监控对象名称
   */
  private String name;

  /**
   * 日志信息
   */
  private String info;

  /**
   * 监控事件类型
   */
  private Short type;

  /**
   * 数据状态 1 表示最新   0 表示旧数据 插入数据时默认是最新数据
   */
  private Short status = LogMonitorDTOConstants.NEW_DATA;

  private Date gmtCreate;

  private Date gmtModified;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }


  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }


  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  /**
   * 数据类型
   */
  public static class LogMonitorDTOConstants implements Serializable {

    private static final long serialVersionUID = -346413454453122453L;


    /**
     * 新监控数据
     */
    public static final Short NEW_DATA = 1;


    /**
     * 老监控数据
     */
    public static final Short OLD_DATA = 0;


  }


  /**
   * 监控数据类型
   */
  public static class LogMonitorStatus implements Serializable {

    private static final long serialVersionUID = -346413454453122453L;


  }
}
