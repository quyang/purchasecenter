package com.xianzaishi.purchasecenter.client.logmonitorconfig;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.logmonitorconfig.dto.LogMonitorConfigDTO;
import java.util.List;

/**
 * 监控配置信息服务
 * @author quyang
 *
 */
public interface LogMonitorConfigService {

  /**
   * 插入数据
   * @param logMonitorConfigDTO
   * @return
   */
  Result<Boolean> insert(LogMonitorConfigDTO logMonitorConfigDTO);
  
  /**
   * @param logMonitorConfigDTO
   * @return
   */
  Result<Boolean> update(LogMonitorConfigDTO logMonitorConfigDTO);
  
  /**
   * @param query
   * @return
   */
  Result<List<LogMonitorConfigDTO>> select(LogMonitorConfigDTO query);

}
