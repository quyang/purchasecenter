package com.xianzaishi.purchasecenter.client.marketactivity.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by quyang on 2017/4/14.
 */
public class MarketActivityDTO implements Serializable {


  private static final long serialVersionUID = -5825099224953901081L;

  /**
   * 活动id
   */
  private Integer id;

  /**
   * 活动名称
   */
  private String name;
  /**
   * 活动状态 0 有效  1 无效
   */
  private Short status;

  /**
   * 优惠内容
   */
  private String couponInfo;

  /**
   * 活动类型
   */
  private Short type;
  /**
   * 开始时间
   */
  private Date gmtStart;

  /**
   * 结束时间
   */
  private Date gmtEnd;

  private Date gmtCreate;

  private Date gmtModified;

  private Integer startPage;

  private Integer sizePage;


  public Integer getStartPage() {
    return startPage;
  }

  public void setStartPage(Integer startPage) {
    this.startPage = startPage;
  }

  public Integer getSizePage() {
    return sizePage;
  }

  public void setSizePage(Integer sizePage) {
    this.sizePage = sizePage;
  }

  public String getCouponInfo() {
    return couponInfo;
  }

  public void setCouponInfo(String couponInfo) {
    this.couponInfo = couponInfo;
  }

  public Integer getId() {
    return id ;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Date getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(Date gmtStart) {
    this.gmtStart = gmtStart;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public static class MarketActivityStatus {

    /**
     * 有效
     */
    public static final Short EFFECTIVE_STATUS = 0;

    /**
     * 无效
     */
    public static final Short UN_EFFECTIVE_STATUS = 1;


  }

  public static class MarketActivityType {

    /**
     * 0 表示满件活动
     */
    public static final Short ENOUGHT_SUBSTRACT_TYPE = 0;

    /**
     * 1 表示限购活动
     */
    public static final Short LIMIT_BUY_TYPE = 1;

    public static Boolean hasActivityType(Short type) {
      return null != type || ENOUGHT_SUBSTRACT_TYPE.equals(type) || LIMIT_BUY_TYPE.equals(type);
    }

  }

  /**
   * 绑定类型
   */
  public static class MarketActivityBindType {

    /**
     *  0 表示关联的是商品
     */
    public static final Short BIND_GOODS_TYPE = 0;

    /**
     * 1 表示关联的是优惠券
     */
    public static final Short BIND_COUPON_TYPE = 1;

    public static Boolean hasActivityBindType(Short type) {
      return null == type || BIND_GOODS_TYPE.equals(type) || BIND_COUPON_TYPE.equals(type);
    }

  }

}
