package com.xianzaishi.purchasecenter.client.purchase.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 采购单对象，系统自动安装供应商合并
 * 
 * @author zhancang
 */
public class PurchaseOrderDTO implements Serializable {

  private static final long serialVersionUID = 2018138497256613103L;

  private Date now = new Date();

  /**
   * 采购单id
   */
  private Integer purchaseId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 订单总金额
   */
  private Long totalAmount;

  /**
   * 订单实际金额
   */
  private Long actualAmount;

  /**
   * 采购id
   */
  private Integer purchasingAgent;

  /**
   * 
   */
  private Date gmtCreate = now;

  /**
   * 
   */
  private Date gmtModified = now;

  /**
   * 采购单物流状态
   */
  private Short deliveryStatus = PurchaseDeliveryStatusConstants.STATUS_NOT_DISTRIBUTION;

  /**
   * 审核状态
   */
  private Short auditingStatus;

  /**
   * 每笔详细采购单
   */
  private List<PurchaseSubOrderDTO> subOrderList;

  /**
   * 备注
   */
  private String remarks;

  /**
   * 入库id
   */
  private Long storageId;

  public Long getStorageId() {
    return storageId;
  }

  public void setStorageId(Long storageId) {
    this.storageId = storageId;
  }

  public Integer getPurchaseId() {
    return purchaseId == null ? 0 : purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    this.purchaseId = purchaseId;
  }

  public Integer getSupplierId() {
    return supplierId == null ? 0 : supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }

  public Integer getPurchasingAgent() {
    return purchasingAgent == null ? 0 : purchasingAgent;
  }

  public void setPurchasingAgent(Integer purchasingAgent) {
    this.purchasingAgent = purchasingAgent;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public List<PurchaseSubOrderDTO> getSubOrderList() {
    return subOrderList == null ? Collections.<PurchaseSubOrderDTO>emptyList() : subOrderList;
  }

  public void setSubOrderList(List<PurchaseSubOrderDTO> subOrderList) {
    this.subOrderList = subOrderList;
  }

  public Long getTotalAmount() {
    return totalAmount == null ? 0L : totalAmount;
  }

  public void setTotalAmount(Long totalAmount) {
    this.totalAmount = totalAmount;
  }

  public Long getActualAmount() {
    return actualAmount == null ? 0L : actualAmount;
  }

  public void setActualAmount(Long actualAmount) {
    this.actualAmount = actualAmount;
  }

  public Short getDeliveryStatus() {
    return deliveryStatus;
  }

  public void setDeliveryStatus(Short deliveryStatus) {
    this.deliveryStatus = deliveryStatus;
  }

  public Short getAuditingStatus() {
    return auditingStatus;
  }

  public void setAuditingStatus(Short auditingStatus) {
    this.auditingStatus = auditingStatus;
  }

  public String getRemarks() {
    return remarks;
  }

  public void setRemarks(String remarks) {
    this.remarks = remarks;
  }

  /**
   * 采购订单审核状态类
   * 
   * @author dongpo
   */
  public static class PurchaseOrderAuditingStatusConstants implements Serializable {


    /**
     * 
     */
    private static final long serialVersionUID = -287104110495684934L;

    /**
     * 草稿状态
     */
    public static final Short STATUS_DRAFT = 0;

    /**
     * 待审核状态
     */
    public static final Short STATUS_FIRST_AUDITING = 1;

    /**
     * 初审完成，待终审
     */
    public static final Short STATUS_FIRST_AUDITING_PASS = 2;

    /**
     * 终审通过，待供应商审核
     */
    public static final Short STATUS_LAST_AUDITING_PASS = 3;

    /**
     * 供应商审核完成，待配送
     */
    public static final Short STATUS_SUPPLIER_AUDITING_PASS = 4;

    /**
     * 配送中，待收货
     */
    public static final Short STATUS_SUPPLIER_AUDITING_DISTRIBUTION = 5;

    /**
     * 收货完成，采购订单关闭
     */
    public static final Short STATUS_SUPPLIER_AUDITING_RECEIVE = 8;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return STATUS_DRAFT.equals(parameter) || STATUS_FIRST_AUDITING.equals(parameter)
          || STATUS_FIRST_AUDITING_PASS.equals(parameter)
          || STATUS_LAST_AUDITING_PASS.equals(parameter)
          || STATUS_SUPPLIER_AUDITING_PASS.equals(parameter)
          || STATUS_SUPPLIER_AUDITING_DISTRIBUTION.equals(parameter)
          || STATUS_SUPPLIER_AUDITING_RECEIVE.equals(parameter);
    }
  }
  
  /**
   * 采购订单审核状态类
   * 
   * @author dongpo
   */
  public static class PurchaseDeliveryStatusConstants implements Serializable{

    /**
     * serial version UID
     */
    private static final long serialVersionUID = -2602713676442777600L;
    
    /**
     * 未配送
     */
    public static final Short STATUS_NOT_DISTRIBUTION = 0;
    
    /**
     * 配送中，待收货
     */
    public static final Short STATUS_IN_DISTRIBUTION = 1;
    
    /**
     * 部分入库
     */
    public static final Short STATUS_STORAGE_PART = 2;
    
    /**
     * 完全入库
     */
    public static final Short STATUS_STORAGE_ALL = 3;
    
    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return STATUS_NOT_DISTRIBUTION.equals(parameter) || STATUS_IN_DISTRIBUTION.equals(parameter)
          || STATUS_STORAGE_PART.equals(parameter)
          || STATUS_STORAGE_ALL.equals(parameter);
    }
  }

}
