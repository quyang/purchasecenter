package com.xianzaishi.purchasecenter.client.purchase.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * 采购质检单
 * 
 * @author zhancang
 */
public class PurchaseQualityDTO implements Serializable {

  private static final long serialVersionUID = 2008330070457720411L;

  private Date now = new Date();

  /**
   * 质检单id
   */
  private Integer qualityId;

  /**
   * 对应子采购单id
   */
  private Integer purchaseId;

  /**
   * 对应入库结算id
   */
  private Integer storageId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 对应skuid
   */
  private Long skuId;

  /**
   * 质检人id
   */
  private Integer qualityUser;

  /**
   * 质检图片
   */
  private List<String> picList;

  /**
   * 质检内容
   */
  private String content;

  /**
   * 
   */
  private Date gmtCreate = now;
  
  
  

  

  /**
   * 
   */
  private Date gmtModified = now;

  public Integer getQualityId() {
    return qualityId == null ? 0 : qualityId;
  }

  public void setQualityId(Integer qualityId) {
    Preconditions.checkNotNull(qualityId, "qualityId == null");
    Preconditions.checkArgument(qualityId > 0, "qualityId <= 0");
    this.qualityId = qualityId;
  }

  public Integer getPurchaseId() {
    return purchaseId == null ? 0 : purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    Preconditions.checkNotNull(purchaseId, "purchaseId == null");
    Preconditions.checkArgument(purchaseId > 0, "purchaseId <= 0");
    this.purchaseId = purchaseId;
  }

  public Integer getStorageId() {
    return storageId == null ? 0 : storageId;
  }

  public void setStorageId(Integer storageId) {
    Preconditions.checkNotNull(storageId, "storageId == null");
    Preconditions.checkArgument(storageId > 0, "storageId <= 0");
    this.storageId = storageId;
  }

  public Integer getSupplierId() {
    return supplierId == null ? 0 : supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    Preconditions.checkNotNull(supplierId, "supplierId == null");
    Preconditions.checkArgument(supplierId > 0, "supplierId <= 0");
    this.supplierId = supplierId;
  }

  public Long getSkuId() {
    return skuId == null ? 0 : skuId;
  }

  public void setSkuId(Long skuId) {
    Preconditions.checkNotNull(skuId, "skuId == null");
    Preconditions.checkArgument(skuId > 0, "skuId <= 0");
    this.skuId = skuId;
  }

  public Integer getQualityUser() {
    return qualityUser == null ? 0 : qualityUser;
  }

  public void setQualityUser(Integer qualityUser) {
    Preconditions.checkNotNull(qualityUser, "qualityUser == null");
    Preconditions.checkArgument(qualityUser > 0, "qualityUser <= 0");
    this.qualityUser = qualityUser;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content == null ? null : content.trim();
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    Preconditions.checkNotNull(gmtCreate, "gmtCreate == null");
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    Preconditions.checkNotNull(gmtModified, "gmtModified == null");
    this.gmtModified = gmtModified;
  }

  public List<String> getPicList() {
    return picList == null ? Collections.<String>emptyList() : picList;
  }

  public void setPicList(List<String> picList) {
    Preconditions.checkNotNull(picList, "picList == null");
    Preconditions.checkArgument(picList.size() > 0, "picList.size() <= 0");
    this.picList = picList;
  }


  
}
