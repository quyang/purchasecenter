package com.xianzaishi.purchasecenter.client.purchase.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 每笔采购单进货详细
 * 
 * @author zhancang
 */
public class StorageOrderDTO implements Serializable {

  private static final long serialVersionUID = 8143143472611526321L;

  private Date now = new Date();

  /**
   * 进货单id
   */
  private Integer storageId;

  /**
   * 采购id
   */
  private Integer purchaseId;

  /**
   * 进货数量
   */
  private Integer count;

  /**
   * 结算金额
   */
  private Integer settlementPrice;

  /**
   * 入库人id
   */
  private Integer qualityId;

  /**
   * 创建时间
   */
  private Date gmtCreate = now;

  /**
   * 
   */
  private Date gmtModified = now;

  /**
   * 入库类型
   */
  private Short type = StorageOrderTypeConstants.STORAGE_ORDER_NOTEND;

  public Integer getStorageId() {
    return storageId == null ? 0 : storageId;
  }

  public void setStorageId(Integer storageId) {
    Preconditions.checkNotNull(storageId, "storageId == null");
    Preconditions.checkArgument(storageId > 0, "storageId <= 0");
    this.storageId = storageId;
  }

  public Integer getPurchaseId() {
    return purchaseId == null ? 0 : purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    Preconditions.checkNotNull(purchaseId, "purchaseId == null");
    Preconditions.checkArgument(purchaseId > 0, "purchaseId <= 0");
    this.purchaseId = purchaseId;
  }

  public Integer getCount() {
    return count == null ? 0 : count;
  }

  public void setCount(Integer count) {
    Preconditions.checkNotNull(count, "count == null");
    Preconditions.checkArgument(count > 0, "count <= 0");
    this.count = count;
  }

  public Integer getSettlementPrice() {
    return settlementPrice == null ? 0 : settlementPrice;
  }

  public void setSettlementPrice(Integer settlementPrice) {
    Preconditions.checkNotNull(settlementPrice, "settlementPrice == null");
    Preconditions.checkArgument(settlementPrice > 0, "settlementPrice <= 0");
    this.settlementPrice = settlementPrice;
  }

  public Integer getQualityId() {
    return qualityId == null ? 0 : qualityId;
  }

  public void setQualityId(Integer qualityId) {
    Preconditions.checkNotNull(qualityId, "qualityId == null");
    Preconditions.checkArgument(qualityId > 0, "qualityId <= 0");
    this.qualityId = qualityId;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    Preconditions.checkNotNull(gmtCreate, "gmtCreate == null");
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    Preconditions.checkNotNull(gmtModified, "gmtModified == null");
    this.gmtModified = gmtModified;
  }

  public Short getType() {
    return type == null ? StorageOrderTypeConstants.STORAGE_ORDER_NOTEND : type;
  }

  public void setType(Short type) {
    Preconditions.checkNotNull(type, "type == null");
    Preconditions.checkArgument(StorageOrderTypeConstants.isCorrectParameter(type),
        "type not in(0,1)");
    this.type = type;
  }

  /**
   * 采购订单流动状态类
   * 
   * @author zhancang
   */
  public static class StorageOrderTypeConstants implements Serializable {

    private static final long serialVersionUID = -3467189054153122453L;

    /**
     * 入库未完成
     */
    public static final Short STORAGE_ORDER_NOTEND = 0;

    /**
     * 入库完成
     */
    public static final Short STORAGE_ORDER_END = 1;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return STORAGE_ORDER_NOTEND.equals(parameter) || STORAGE_ORDER_END.equals(parameter);
    }
  }
}
