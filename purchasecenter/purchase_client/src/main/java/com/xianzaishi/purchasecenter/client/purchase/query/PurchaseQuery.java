package com.xianzaishi.purchasecenter.client.purchase.query;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.google.common.base.Preconditions;
import com.xianzaishi.itemcenter.common.query.BaseQuery;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderFlowStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderStatusConstants;

/**
 * 采购单查询参数对象
 * @author zhancang
 */
public class PurchaseQuery extends BaseQuery implements Serializable{

  private static final long serialVersionUID = -4513568975251036607L;

  /**
   * skuId
   */
  private Long skuId;

  /**
   * 对应总采购单id
   */
  private Integer parentPurchaseId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 采购员id，数据库中and
   */
  private Integer purchasingAgent;
  
  /**
   * 采购单状态
   */
  private Short status;
  
  /**
   * 采购单流转状态
   */
  private Short flowStatus;
  
  /**
   * 创建人
   */
  private Integer creator;
  
  /**
   * 商品sku id 组
   */
  private List<Long> skuIds;
  
  /**
   * 订单审核状态
   */
  private List<Short> auditingStatus;
  
  /**
   * 订单送货状态
   */
  private Short deliveryStatus;
  
  /**
   * 采购开始时间
   */
  private Date purchaseBegin;
  
  /**
   * 采购结束时间
   */
  private Date purchaseEnd;
  
  /**
   * 采购人,数据库中查询使用or
   */
  private Integer orPurchasingAgent;
  
  /**
   * 供应商id,数据库中查询使用or
   */
  private Integer orSupplierId;
  
  /**
   * 对应总采购单id,数据库中查询使用or
   */
  private Integer orParentPurchaseId;
  
  /**
   * 采购开始时间,数据库中查询使用or
   */
  private Date orPurchaseBegin;
  
  /**
   * 采购结束时间,数据库中查询使用or
   */
  private Date orPurchaseEnd;

  /**
   * 入库id
   */
  private Long storageId;

  public Long getStorageId() {
    return storageId;
  }

  public void setStorageId(Long storageId) {
    this.storageId = storageId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    Preconditions.checkNotNull(skuId, "skuId == null");
    Preconditions.checkArgument(skuId > 0, "skuId <= 0");
    this.skuId = skuId;
  }

  public Integer getParentPurchaseId() {
    return parentPurchaseId;
  }

  public void setParentPurchaseId(Integer parentPurchaseId) {
    Preconditions.checkNotNull(parentPurchaseId, "parentPurchaseId == null");
    Preconditions.checkArgument(parentPurchaseId > 0, "parentPurchaseId <= 0");
    this.parentPurchaseId = parentPurchaseId;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    Preconditions.checkNotNull(supplierId, "supplierId == null");
    Preconditions.checkArgument(supplierId > 0, "supplierId <= 0");
    this.supplierId = supplierId;
  }

  public Integer getPurchasingAgent() {
    return purchasingAgent;
  }

  public void setPurchasingAgent(Integer purchasingAgent) {
    Preconditions.checkNotNull(purchasingAgent, "purchasingAgent == null");
    Preconditions.checkArgument(purchasingAgent > 0, "purchasingAgent <= 0");
    this.purchasingAgent = purchasingAgent;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument(PurchaseSubOrderStatusConstants.isCorrectParameter(status),
        "status not in(0,1,2,3)");
    this.status = status;
  }

  public Short getFlowStatus() {
    return flowStatus;
  }

  public void setFlowStatus(Short flowStatus) {
    Preconditions.checkNotNull(flowStatus, "flowStatus == null");
    Preconditions.checkArgument(PurchaseSubOrderFlowStatusConstants.isCorrectParameter(flowStatus),
        "status not in(0,2,3,5,7,10)");
    this.flowStatus = flowStatus;
  }

  public Integer getCreator() {
    return creator;
  }

  public void setCreator(Integer creator) {
    Preconditions.checkNotNull(creator, "creator == null");
    Preconditions.checkArgument(creator > 0, "creator <= 0");
    this.creator = creator;
  }

  public List<Long> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<Long> skuIds) {
    this.skuIds = skuIds;
  }

  public List<Short> getAuditingStatus() {
    return auditingStatus;
  }

  public void setAuditingStatus(List<Short> auditingStatus) {
    this.auditingStatus = auditingStatus;
  }

  public Short getDeliveryStatus() {
    return deliveryStatus;
  }

  public void setDeliveryStatus(Short deliveryStatus) {
    this.deliveryStatus = deliveryStatus;
  }

  public Date getPurchaseBegin() {
    return purchaseBegin;
  }

  public void setPurchaseBegin(Date purchaseBegin) {
    this.purchaseBegin = purchaseBegin;
  }

  public Date getPurchaseEnd() {
    return purchaseEnd;
  }

  public void setPurchaseEnd(Date purchaseEnd) {
    this.purchaseEnd = purchaseEnd;
  }

  public Integer getOrPurchasingAgent() {
    return orPurchasingAgent;
  }

  public void setOrPurchasingAgent(Integer orPurchasingAgent) {
    this.orPurchasingAgent = orPurchasingAgent;
  }

  public Integer getOrSupplierId() {
    return orSupplierId;
  }

  public void setOrSupplierId(Integer orSupplierId) {
    this.orSupplierId = orSupplierId;
  }

  public Integer getOrParentPurchaseId() {
    return orParentPurchaseId;
  }

  public void setOrParentPurchaseId(Integer orParentPurchaseId) {
    this.orParentPurchaseId = orParentPurchaseId;
  }

  public Date getOrPurchaseBegin() {
    return orPurchaseBegin;
  }

  public void setOrPurchaseBegin(Date orPurchaseBegin) {
    this.orPurchaseBegin = orPurchaseBegin;
  }

  public Date getOrPurchaseEnd() {
    return orPurchaseEnd;
  }

  public void setOrPurchaseEnd(Date orPurchaseEnd) {
    this.orPurchaseEnd = orPurchaseEnd;
  }
  
}
