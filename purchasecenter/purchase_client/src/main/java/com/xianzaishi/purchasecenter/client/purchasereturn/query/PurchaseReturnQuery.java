package com.xianzaishi.purchasecenter.client.purchasereturn.query;

import java.io.Serializable;

import com.google.common.base.Preconditions;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO.PurchaseSubOrderStatusConstants;

/**
 * 退货单单查询参数对象
 * @author zhancang
 */
public class PurchaseReturnQuery implements Serializable{

  private static final long serialVersionUID = -4513564975251036607L;

  /**
   * skuId
   */
  private Long skuId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 采购员id
   */
  private Integer purchasingAgent;
  
  /**
   * 采购单状态
   */
  private Short status;
  
  /**
   * 创建人
   */
  private Integer creator;
  
  /**
   * 默认查询都查20条记录
   */
  private Integer pageSize = 20;
  
  /**
   * 默认查询从0页开始
   */
  private Integer pageNum = 0;

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    Preconditions.checkNotNull(skuId, "skuId == null");
    Preconditions.checkArgument(skuId > 0, "skuId <= 0");
    this.skuId = skuId;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    Preconditions.checkNotNull(supplierId, "supplierId == null");
    Preconditions.checkArgument(supplierId > 0, "supplierId <= 0");
    this.supplierId = supplierId;
  }

  public Integer getPurchasingAgent() {
    return purchasingAgent;
  }

  public void setPurchasingAgent(Integer purchasingAgent) {
    Preconditions.checkNotNull(purchasingAgent, "purchasingAgent == null");
    Preconditions.checkArgument(purchasingAgent > 0, "purchasingAgent <= 0");
    this.purchasingAgent = purchasingAgent;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument(PurchaseSubOrderStatusConstants.isCorrectParameter(status),
        "status not in(0,1,2,3)");
    this.status = status;
  }

  public Integer getCreator() {
    return creator;
  }

  public void setCreator(Integer creator) {
    Preconditions.checkNotNull(creator, "creator == null");
    Preconditions.checkArgument(creator > 0, "creator <= 0");
    this.creator = creator;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    Preconditions.checkNotNull(pageSize, "pageSize == null");
    Preconditions.checkArgument(pageSize > 0, "pageSize <= 0");
    this.pageSize = pageSize;
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    Preconditions.checkNotNull(pageNum, "pageNum == null");
    Preconditions.checkArgument(pageNum >= 0, "pageNum < 0");
    this.pageNum = pageNum;
  }
  
}
