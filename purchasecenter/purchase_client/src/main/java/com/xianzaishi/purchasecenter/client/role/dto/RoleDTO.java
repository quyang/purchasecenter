package com.xianzaishi.purchasecenter.client.role.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 角色 表字段
 * 
 * @author dongpo
 * 
 */
public class RoleDTO implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -1600001606962634287L;

  /**
   * 角色id
   */
  private Integer roleId;

  /**
   * 角色名
   */
  private String roleName;

  /**
   * 角色介绍
   */
  private String introduction;

  /**
   * 角色创建时间
   */
  private Date gmtCreate;

  /**
   * 角色修改时间
   */
  private Date gmtModified;


  public Integer getRoleId() {
    return roleId == null ? 0 : roleId;
  }


  public void setRoleId(Integer roleId) {
    Preconditions.checkNotNull(roleId, "roleId is null !");
    Preconditions.checkArgument(roleId>0, "roleId must be greater than 0");
    this.roleId = roleId;
  }


  public String getRoleName() {
    return roleName;
  }


  public void setRoleName(String roleName) {
    this.roleName = roleName == null ? null : roleName.trim();
  }


  public String getIntroduction() {
    return introduction;
  }


  public void setIntroduction(String introduction) {
    this.introduction = introduction == null ? null : introduction.trim();
  }


  public Date getGmtCreate() {
    return gmtCreate;
  }


  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }


  public Date getGmtModified() {
    return gmtModified;
  }


  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

}
