package com.xianzaishi.purchasecenter.client.user;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.query.BackGroundUserQuery;

/**
 * 基础用户表
 * 
 * @author zhancang
 * 
 */
public interface BackGroundUserService {

  /**
   * 更新员工信息
   * 
   * @param employeeDto
   * @return
   */
  Result<Boolean> updateUserDTO(BackGroundUserDTO userDTO);
  
  /**
   * 根据token查询当前用户
   * @param token
   * @return
   */
  Result<BackGroundUserDTO> queryUserDTOByToken(String token);
  
  /**
   * 根据登录用户名查询当前用户
   * @param token
   * @return
   */
  Result<BackGroundUserDTO> queryUserDTOByName(String userName);
  
  /**
   * 根据当前用户id查询当前用户
   * @param token
   * @return
   */
  Result<BackGroundUserDTO> queryUserDTOById(Integer userId);
  
  /**
   * 根据当前用户id查询当前用户
   * @param token
   * @return
   */
  Result<List<BackGroundUserDTO>> queryUserDTOByIdList(List<Integer> userIdList);
  
  /**
   * 根据查询条件查询当前用户
   * @param token
   * @return
   */
  PagedResult<List<BackGroundUserDTO>> queryUserDTOByQuery(BackGroundUserQuery query);
  
  /**
   * 根据查询条件查询当前用户数量
   * @param token
   * @return
   */
  Result<Integer> queryUserDTOCountByQuery(BackGroundUserQuery query);
  
  /**
   * 根据当前用户id查询当前用户
   * @param token
   * @return
   */
  Result<BackGroundUserDTO> queryUserDTOByPhone(Long phone);
  
}
