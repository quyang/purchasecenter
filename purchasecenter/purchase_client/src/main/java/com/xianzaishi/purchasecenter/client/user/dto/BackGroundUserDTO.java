package com.xianzaishi.purchasecenter.client.user.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.google.common.collect.Lists;

/**
 * 用户表
 * 
 * @author zhancang
 * 
 */
public class BackGroundUserDTO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -8752877269552340855L;

  public static final String ROLE_SPLIT = ";";// 角色字段底层分隔符
  /**
   * 用户id
   */
  protected Integer userId;

  /**
   * 用户名称
   */
  protected String name;

  /**
   * 雇员登陆token
   */
  protected String token;

  /**
   * 用户类型
   */
  protected Short userType;

  /**
   * 创建时间
   */
  protected Date gmtCreate;

  /**
   * 修改时间
   */
  protected Date gmtModified;

  /**
   * token过期时间
   */
  protected Date expireTime;

  /**
   * 硬件编码
   */
  protected String hardwareCode;

  /**
   * 用户角色
   */
  protected String role;

  /**
   * 密码数据
   */
  protected String pwd;

  /**
   * 电话号码
   */
  protected Long phone;

  /**
   * 激活码、创建时间混合字段 code***;timelong****
   */
  protected String activeCode;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token == null ? null : token.trim();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getUserId() {
    return userId == null ? 0 : userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getExpireTime() {
    return expireTime;
  }

  public void setExpireTime(Date expireTime) {
    this.expireTime = expireTime;
  }

  public String getHardwareCode() {
    return hardwareCode;
  }

  public void setHardwareCode(String hardwareCode) {
    this.hardwareCode = hardwareCode == null ? null : hardwareCode.trim();
  }

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short userType) {
    this.userType = userType;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  /**
   * 取list结构的角色列表
   * 
   * @return
   */
  public List<Integer> queryRoleList() {
    List<Integer> result = Lists.newArrayList();
    if (StringUtils.isNotEmpty(role)) {
      String roleArray[] = role.split(ROLE_SPLIT);
      for (String tmp : roleArray) {
        if (StringUtils.isNotEmpty(tmp) && StringUtils.isNumeric(tmp) ) {
          result.add(Integer.valueOf(tmp));
        }
      }
    }
    return result;
  }

  /**
   * 增加一个角色
   * 
   * @param role
   */
  public void addRole(Integer roleId) {
    List<Integer> roleList = this.queryRoleList();
    if (!roleList.contains(roleId)) {
      if (StringUtils.isEmpty(this.role)) {
        this.role = roleId + ROLE_SPLIT;
      } else {
        this.role = this.role + roleId + ROLE_SPLIT;
      }
    }
  }

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }

  public String getActiveCode() {
    return activeCode;
  }

  public void setActiveCode(String activeCode) {
    this.activeCode = activeCode;
  }

  /**
   * 检查是否token是正确token
   * @return
   */
  public boolean checkIsRightToken(String inputToken){
    if(StringUtils.isEmpty(inputToken) || StringUtils.isEmpty(this.getToken())){
      return false;
    }
    
    if(inputToken.toLowerCase().equals(this.getToken().toLowerCase())){
      return true;
    }
    
    return false;
  }
  /**
   * 用户类型
   * 
   * @author zhancang
   *
   */
  public static class UserTypeConstants implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 员工
     */
    public static final Short USER_EMPLOYEE = 0;

    /**
     * 供应商
     */
    public static final Short USER_SUPPLIER = 1;
  }

  /**
   * 员工退款权限类型
   *
   * @author quyang
   *
   */
  public static class EmployeeTypeConstants implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 客服
     */
    public static final Short USER_SERVICER = 0;

    /**
     * 财务
     */
    public static final Short USER_FINANCE = 1;

    /**
     * 管理
     */
    public static final Short USER_ADMIN = 2;


  }
}
