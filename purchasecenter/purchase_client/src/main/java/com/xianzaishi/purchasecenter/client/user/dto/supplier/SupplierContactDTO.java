package com.xianzaishi.purchasecenter.client.user.dto.supplier;

import java.io.Serializable;

/**
 * 供应商联系人信息
 * 
 * @author zhancang
 */
public class SupplierContactDTO implements Serializable {

  private static final long serialVersionUID = 3846450725580519286L;

  private Integer supplierId;

  /**
   * 联系人名称
   */
  private String contactName;

  /**
   * 联系电话后备号码
   */
  private Long contactPhoneBk;

  /**
   * 联系人邮件
   */
  private String contactMail;

  /**
   * 联系人qq
   */
  private Long contactQq;

  /**
   * 联系人上司名称
   */
  private String managerName;

  /**
   * 联系人上司手机
   */
  private Long managerPhone;

  /**
   * 联系人上司手机备份手机
   */
  private Long managerPhoneBk;

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }
  
  public String getContactName() {
    return contactName;
  }

  public void setContactName(String contactName) {
    this.contactName = contactName == null ? null : contactName.trim();
  }

  public Long getContactPhoneBk() {
    return contactPhoneBk;
  }

  public void setContactPhoneBk(Long contactPhoneBk) {
    this.contactPhoneBk = contactPhoneBk;
  }

  public String getContactMail() {
    return contactMail;
  }

  public void setContactMail(String contactMail) {
    this.contactMail = contactMail == null ? null : contactMail.trim();
  }

  public Long getContactQq() {
    return contactQq;
  }

  public void setContactQq(Long contactQq) {
    this.contactQq = contactQq;
  }

  public String getManagerName() {
    return managerName;
  }

  public void setManagerName(String managerName) {
    this.managerName = managerName == null ? null : managerName.trim();
  }

  public Long getManagerPhone() {
    return managerPhone;
  }

  public void setManagerPhone(Long managerPhone) {
    this.managerPhone = managerPhone;
  }

  public Long getManagerPhoneBk() {
    return managerPhoneBk;
  }

  public void setManagerPhoneBk(Long managerPhoneBk) {
    this.managerPhoneBk = managerPhoneBk;
  }
}
