package com.xianzaishi.purchasecenter.client.user.dto.supplier;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import com.google.common.base.Preconditions;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;

/**
 * 供应商信息
 * 
 * @author zhancang
 */
public class SupplierDTO extends BackGroundUserDTO implements Serializable {

  private static final long serialVersionUID = 4722454302015324058L;

  /**
   * 供应商公司名称
   */
  private String companyName;

  /**
   * 供应商当前状态，参考SUPPLIER_STATUS类
   */
  private Short status = SupplierStatusConstants.SUPPLIER_STATUS_WAITCHECK;

  /**
   * 供应商对应采购
   */
  private Integer purchasingAgent;

  /**
   * 审核失败原因
   */
  private String checkedFailedReason;

  /**
   * 审核人列表
   */
  private List<Integer> checkerList;

  /**
   * 供应商备注
   */
  private String note;

  /**
   * 供应商联系人信息
   */
  private SupplierContactDTO supplierContactDTO;

  /**
   * 供应商公司信息
   */
  private SupplierCompanyDTO supplierCompanyDTO;

  /**
   * 供应商财务信息
   */
  private SupplierFinanceDTO supplierFinanceDTO;

  /**
   * 供应商资质信息
   */
  private List<SupplierLicensesDTO> supplierLicensesDTOList;

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName == null ? null : companyName.trim();
  }

  public Short getStatus() {
    return status == null ? SupplierStatusConstants.SUPPLIER_STATUS_WAITCHECK : status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument(SupplierStatusConstants.isCorrectParameter(status),
        "status not in(0,1,2,3,4)");
    this.status = status;
  }

  public Integer getPurchasingAgent() {
    return purchasingAgent == null ? 0 : purchasingAgent;
  }

  public void setPurchasingAgent(Integer purchasingAgent) {
    Preconditions.checkNotNull(purchasingAgent, "purchasingAgent == null");
    Preconditions.checkArgument(purchasingAgent > 0, "purchasingAgent <= 0");
    this.purchasingAgent = purchasingAgent;
  }

  public String getNote() {
    return note;
  }

  public void setNote(String note) {
    this.note = note == null ? null : note.trim();
  }

  public SupplierContactDTO getSupplierContactDTO() {
    return supplierContactDTO;
  }

  public void setSupplierContactDTO(SupplierContactDTO supplierContactDTO) {
    this.supplierContactDTO = supplierContactDTO;
  }

  public SupplierCompanyDTO getSupplierCompanyDTO() {
    return supplierCompanyDTO;
  }

  public void setSupplierCompanyDTO(SupplierCompanyDTO supplierCompanyDTO) {
    this.supplierCompanyDTO = supplierCompanyDTO;
  }

  public SupplierFinanceDTO getSupplierFinanceDTO() {
    return supplierFinanceDTO;
  }

  public void setSupplierFinanceDTO(SupplierFinanceDTO supplierFinanceDTO) {
    this.supplierFinanceDTO = supplierFinanceDTO;
  }

  public List<SupplierLicensesDTO> getSupplierLicensesDTOList() {
    return supplierLicensesDTOList == null ? Collections.<SupplierLicensesDTO>emptyList()
        : supplierLicensesDTOList;
  }

  public void setSupplierLicensesDTOList(List<SupplierLicensesDTO> supplierLicensesDTOList) {
    this.supplierLicensesDTOList = supplierLicensesDTOList;
  }

  public String getCheckedFailedReason() {
    return checkedFailedReason;
  }

  public void setCheckedFailedReason(String checkedFailedReason) {
    this.checkedFailedReason = checkedFailedReason == null ? null : checkedFailedReason.trim();
  }

  public List<Integer> getCheckerList() {
    return checkerList == null ? Collections.<Integer>emptyList() : checkerList;
  }

  public void setCheckerList(List<Integer> checkerList) {
    this.checkerList = checkerList;
  }

  /**
   * 供应商状态类
   * 
   * @author zhancang
   */
  public static class SupplierStatusConstants implements Serializable {

    private static final long serialVersionUID = -3467189058053114453L;

    /**
     * 正在待审核状态
     */
    public static final Short SUPPLIER_STATUS_WAITCHECK = 0;

    /**
     * 审核通过状态
     */
    public static final Short SUPPLIER_STATUS_PASS = 1;

    /**
     * 审核不通过状态
     */
    public static final Short SUPPLIER_STATUS_CHECKED_NOTPASS = 2;

    /**
     * 正在审核状态
     */
    public static final Short SUPPLIER_STATUS_CHECKING = 3;

    /**
     * 被清退供应商
     */
    public static final Short SUPPLIER_STATUS_REJECTED = 4;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      if(null == parameter){
        return false;
      }
      return parameter.shortValue() == SUPPLIER_STATUS_WAITCHECK.shortValue() || parameter.shortValue() == SUPPLIER_STATUS_PASS.shortValue()
          || parameter.shortValue() == SUPPLIER_STATUS_CHECKED_NOTPASS.shortValue() || parameter.shortValue() == SUPPLIER_STATUS_CHECKING.shortValue()
          || parameter.shortValue() == SUPPLIER_STATUS_REJECTED.shortValue();
    }
  }
}
