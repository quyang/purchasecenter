package com.xianzaishi.purchasecenter.client.user.dto.supplier;

import java.io.Serializable;

/**
 * 供应商财务信息
 * 
 * @author zhancang
 */
public class SupplierFinanceDTO implements Serializable {

  private static final long serialVersionUID = 3990288675973964351L;

  private Integer supplierId;

  /**
   * 默认帐期时间15天
   */
  public static final int DEFAULT_PAYMENT_PERIOD_DAY = 15;
  
  /**
   * 开户行描述
   */
  private String bankInfo;

  /**
   * 开户名称
   */
  private String bankName;

  /**
   * 银行账号
   */
  private String bankAccount;

  /**
   * 发票抬头
   */
  private String invoice;

  /**
   * 支付宝账号
   */
  private String alipayAccount;

  /**
   * 支付宝名称
   */
  private String alipayName;

  /**
   * 帐期时间，默认15天
   */
  private Integer paymentPeriod = DEFAULT_PAYMENT_PERIOD_DAY;

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }
  
  public String getBankInfo() {
    return bankInfo;
  }

  public void setBankInfo(String bankInfo) {
    this.bankInfo = bankInfo == null ? null : bankInfo.trim();
  }

  public String getBankName() {
    return bankName;
  }

  public void setBankName(String bankName) {
    this.bankName = bankName == null ? null : bankName.trim();
  }

  public String getBankAccount() {
    return bankAccount;
  }

  public void setBankAccount(String bankAccount) {
    this.bankAccount = bankAccount == null ? null : bankAccount.trim();
  }

  public String getInvoice() {
    return invoice;
  }

  public void setInvoice(String invoice) {
    this.invoice = invoice == null ? null : invoice.trim();
  }

  public String getAlipayAccount() {
    return alipayAccount;
  }

  public void setAlipayAccount(String alipayAccount) {
    this.alipayAccount = alipayAccount == null ? null : alipayAccount.trim();
  }

  public String getAlipayName() {
    return alipayName;
  }

  public void setAlipayName(String alipayName) {
    this.alipayName = alipayName == null ? null : alipayName.trim();
  }

  public Integer getPaymentPeriod() {
    return paymentPeriod;
  }

  public void setPaymentPeriod(Integer paymentPeriod) {
    this.paymentPeriod = paymentPeriod;
  }
}
