package com.xianzaishi.purchasecenter.client.user.dto.supplier;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * 供应商各种执照信息信息
 * 
 * @author zhancang
 */
public class SupplierLicensesDTO implements Serializable {

  private static final long serialVersionUID = 731702481215943302L;

  /**
   * 主键id
   */
  private Integer licenseId;
  
  /**
   * 供应商id
   */
  private Integer supplierId;
  
  /**
   * 证件类型
   */
  private Short licensesType = LicensesTypeConstants.OTHER;

  /**
   * 证件代码
   */
  private String licensesCode;

  /**
   * 生效开始时间
   */
  private Date licensesBegin;

  /**
   * 生效结束时间
   */
  private Date licensesEnd;

  /**
   * 证件照片
   */
  private List<String> licensesPicList;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 修改时间
   */
  private Date gmtModified;

  /**
   * 是否做了三证合一
   */
  private Boolean isBusinessOrganizationTaxTogether;

  public Integer getLicenseId() {
    return licenseId;
  }

  public void setLicenseId(Integer licenseId) {
    this.licenseId = licenseId;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }
  
  public Short getLicensesType() {
    return licensesType == null ? LicensesTypeConstants.OTHER : licensesType;
  }

  public void setLicensesType(Short licensesType) {
    Preconditions.checkNotNull(licensesType, "licensesType == null");
    Preconditions.checkArgument(LicensesTypeConstants.isCorrectParameter(licensesType),
        "licensesType not in(1,2,3,4,5,6,7,8)");
    this.licensesType = licensesType;
  }

  public String getLicensesCode() {
    return licensesCode;
  }

  public void setLicensesCode(String licensesCode) {
    this.licensesCode = licensesCode == null ? null : licensesCode.trim();
  }

  public Date getLicensesBegin() {
    return licensesBegin;
  }

  public void setLicensesBegin(Date licensesBegin) {
    this.licensesBegin = licensesBegin;
  }

  public Date getLicensesEnd() {
    return licensesEnd;
  }

  public void setLicensesEnd(Date licensesEnd) {
    this.licensesEnd = licensesEnd;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Boolean getIsBusinessOrganizationTaxTogether() {
    return isBusinessOrganizationTaxTogether;
  }

  public void setIsBusinessOrganizationTaxTogether(Boolean isBusinessOrganizationTaxTogether) {
    this.isBusinessOrganizationTaxTogether = isBusinessOrganizationTaxTogether;
  }

  public List<String> getLicensesPicList() {
    return licensesPicList == null ? Collections.<String>emptyList():licensesPicList;
  }

  public void setLicensesPicList(List<String> licensesPicList) {
    this.licensesPicList = licensesPicList;
  }

  /**
   * 供应商状态类
   * 
   * @author zhancang
   */
  public static class LicensesTypeConstants implements Serializable {

    private static final long serialVersionUID = -3467189058053113453L;

    /**
     * 营业执照证照号
     */
    public static final Short BUSINESS_LICENSES = 1;

    /**
     * 组织机构代码证号
     */
    public static final Short ORGANIZATION_CODE = 2;

    /**
     * 税务登记证号
     */
    public static final Short TAX_REGISTRATION = 3;

    /**
     * 开户行许可证号
     */
    public static final Short ACCOUNT_PERMIT = 4;

    /**
     * 生产许可证号（QS）
     */
    public static final Short QS = 5;

    /**
     * 国际条码证
     */
    public static final Short BAR_CODE = 6;

    /**
     * 食品流通许可证
     */
    public static final Short FOOD_PERMIT = 7;

    /**
     * 其它许可证
     */
    public static final Short OTHER = 8;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      if(null == parameter){
        return false;
      }
      return parameter.shortValue() == BUSINESS_LICENSES.shortValue() || parameter == ORGANIZATION_CODE.shortValue()
          || parameter.shortValue() == TAX_REGISTRATION.shortValue() || parameter == ACCOUNT_PERMIT.shortValue() || parameter.shortValue() == QS.shortValue()
          || parameter.shortValue() == BAR_CODE.shortValue() || parameter == FOOD_PERMIT.shortValue() || parameter.shortValue() == OTHER.shortValue();
    }
  }
}
