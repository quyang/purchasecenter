package com.xianzaishi.purchasecenter.client.user.query;

import java.io.Serializable;
import java.util.List;

import com.google.common.base.Preconditions;
import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 供应商查询参数对象
 * @author zhancang
 */
public class SupplierQuery extends BaseQuery implements Serializable{

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -4102037689318633341L;
  
  /**
   * 按照采购人员查询
   */
  private Integer purchasingAgent;
  
  /**
   * 按照供应商名字查询
   */
  private String name;
  
  /**
   * 根据供应商id查询
   */
  private List<Integer> supplierIds;
  
  /**
   * 根据供应商查询
   */
  private Short supplierType;
  
  /**
   * 是否要返回联系人信息
   */
  private boolean includeContactInfo;

  /**
   * 是否要返回公司信息
   */
  private boolean includeCompanyInfo;

  /**
   * 是否要返回财务信息
   */
  private boolean includeFinanceInfo;

  /**
   * 是否要返回各类执照信息信息
   */
  private boolean includeLicensesInfo;

  
  public List<Integer> getSupplierIds() {
    return supplierIds;
  }

  public void setSupplierIds(List<Integer> supplierIds) {
    this.supplierIds = supplierIds;
  }

  public Integer getPurchasingAgent() {
    return purchasingAgent;
  }

  public void setPurchasingAgent(Integer purchasingAgent) {
    Preconditions.checkNotNull(purchasingAgent, "purchasingAgent == null");
    Preconditions.checkArgument(purchasingAgent > 0, "purchasingAgent <= 0");
    this.purchasingAgent = purchasingAgent;
  }

  public boolean getIncludeContactInfo() {
    return includeContactInfo;
  }

  public void setIncludeContactInfo(boolean includeContactInfo) {
    Preconditions.checkNotNull(includeContactInfo, "includeContactInfo == null");
    this.includeContactInfo = includeContactInfo;
  }

  public boolean getIncludeCompanyInfo() {
    return includeCompanyInfo;
  }

  public void setIncludeCompanyInfo(boolean includeCompanyInfo) {
    Preconditions.checkNotNull(includeCompanyInfo, "includeCompanyInfo == null");
    this.includeCompanyInfo = includeCompanyInfo;
  }

  public boolean getIncludeFinanceInfo() {
    return includeFinanceInfo;
  }

  public void setIncludeFinanceInfo(boolean includeFinanceInfo) {
    Preconditions.checkNotNull(includeFinanceInfo, "includeFinanceInfo == null");
    this.includeFinanceInfo = includeFinanceInfo;
  }

  public boolean getIncludeLicensesInfo() {
    return includeLicensesInfo;
  }

  public void setIncludeLicensesInfo(boolean includeLicensesInfo) {
    Preconditions.checkNotNull(includeLicensesInfo, "includeLicensesInfo == null");
    this.includeLicensesInfo = includeLicensesInfo;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Short getSupplierType() {
    return supplierType;
  }

  public void setSupplierType(Short supplierType) {
    this.supplierType = supplierType;
  }
  
}
