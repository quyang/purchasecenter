package com.xianzaishi.settlement.client.finance.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 财务流水记录
 * 
 * @author zhancang
 *
 */
public class FinancialOrderDTO implements Serializable {

  private static final long serialVersionUID = -1939925330519349249L;

  private Date now = new Date();
  /**
   * 流水id
   */
  private Integer financialId;

  /**
   * 对应采购、退货等订单id
   */
  private Integer orderId;

  /**
   * 财务类型
   */
  private Short type = FinancialOrderTypeConstants.ORDER_TYPE_RECEIVE;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 标题
   */
  private String title;

  /**
   * 初始金额
   */
  private Integer price;

  /**
   * 当前金额
   */
  private Integer currentPrice;

  /**
   * 付款开始日期
   */
  private Date payStart;

  /**
   * 付款截止日期
   */
  private Date payEnd;

  /**
   * 操作人
   */
  private Integer payOperator;

  /**
   * 
   */
  private Date gmtCreate = now;

  /**
   * 
   */
  private Date gmtModified = now;

  /**
   * 财务当前记录状态
   */
  private Short status = FinancialOrderStatusConstants.ORDER_STATUS_NOT_START;

  public Integer getFinancialId() {
    return financialId == null ? 0 : financialId;
  }

  public void setFinancialId(Integer financialId) {
    Preconditions.checkNotNull(financialId, "setFinancialId == null");
    Preconditions.checkArgument(financialId > 0, "setFinancialId <= 0");
    this.financialId = financialId;
  }

  public Integer getOrderId() {
    return orderId == null ? 0 : orderId;
  }

  public void setOrderId(Integer orderId) {
    Preconditions.checkNotNull(orderId, "orderId == null");
    Preconditions.checkArgument(orderId > 0, "orderId <= 0");
    this.orderId = orderId;
  }

  public Short getType() {
    return type == null ? FinancialOrderTypeConstants.ORDER_TYPE_RECEIVE : type;
  }

  public void setType(Short type) {
    Preconditions.checkNotNull(type, "type == null");
    Preconditions.checkArgument(FinancialOrderTypeConstants.isCorrectParameter(type),
        "type not in(1,2)");
    this.type = type;
  }

  public Integer getSupplierId() {
    return supplierId == null ? 0 : supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    Preconditions.checkNotNull(supplierId, "supplierId == null");
    Preconditions.checkArgument(supplierId > 0, "supplierId <= 0");
    this.supplierId = supplierId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public Integer getPrice() {
    return price == null ? 0 : price;
  }

  public void setPrice(Integer price) {
    Preconditions.checkNotNull(price, "price == null");
    Preconditions.checkArgument(price >= 0, "price < 0");
    this.price = price;
  }

  public Integer getCurrentPrice() {
    return currentPrice == null ? 0 : currentPrice;
  }

  public void setCurrentPrice(Integer currentPrice) {
    Preconditions.checkNotNull(currentPrice, "currentPrice == null");
    Preconditions.checkArgument(currentPrice >= 0, "currentPrice < 0");
    this.currentPrice = currentPrice;
  }

  public Date getPayStart() {
    return payStart;
  }

  public void setPayStart(Date payStart) {
    Preconditions.checkNotNull(payStart, "payStart == null");
    this.payStart = payStart;
  }

  public Date getPayEnd() {
    return payEnd;
  }

  public void setPayEnd(Date payEnd) {
    Preconditions.checkNotNull(payEnd, "payEnd == null");
    this.payEnd = payEnd;
  }

  public Integer getPayOperator() {
    return payOperator == null ? 0 : payOperator;
  }

  public void setPayOperator(Integer payOperator) {
    Preconditions.checkNotNull(payOperator, "payOperator == null");
    Preconditions.checkArgument(payOperator >= 0, "payOperator < 0");
    this.payOperator = payOperator;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    Preconditions.checkNotNull(gmtCreate, "gmtCreate == null");
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    Preconditions.checkNotNull(gmtModified, "gmtModified == null");
    this.gmtModified = gmtModified;
  }

  public Short getStatus() {
    return status == null ? FinancialOrderStatusConstants.ORDER_STATUS_NOT_START : status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument(FinancialOrderStatusConstants.isCorrectParameter(status),
        "status not in(0,2,4)");
    this.status = status;
  }

  /**
   * 财务流水记录类型
   * 
   * @author zhancang
   */
  public static class FinancialOrderTypeConstants implements Serializable {

    private static final long serialVersionUID = -3464189054053122453L;

    /**
     * 付款类型
     */
    public static final Short ORDER_TYPE_PAYMENT = 1;

    /**
     * 收款类型
     */
    public static final Short ORDER_TYPE_RECEIVE = 2;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == ORDER_TYPE_PAYMENT || parameter == ORDER_TYPE_RECEIVE;
    }
  }

  /**
   * 财务流水记录类型
   * 
   * @author zhancang
   */
  public static class FinancialOrderStatusConstants implements Serializable {

    private static final long serialVersionUID = -3464189054453122453L;

    /**
     * 未开始处理
     */
    public static final Short ORDER_STATUS_NOT_START = 0;

    /**
     * 处理中
     */
    public static final Short ORDER_STATUS_PROCESSING = 2;

    /**
     * 处理完成
     */
    public static final Short ORDER_STATUS_END = 4;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == ORDER_STATUS_NOT_START || parameter == ORDER_STATUS_PROCESSING
          || parameter == ORDER_STATUS_END;
    }
  }
}
