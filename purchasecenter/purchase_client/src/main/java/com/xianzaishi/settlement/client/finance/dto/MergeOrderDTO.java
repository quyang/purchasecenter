package com.xianzaishi.settlement.client.finance.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 财务抵扣合并记录
 * 
 * @author zhancang
 */
public class MergeOrderDTO implements Serializable {

  private static final long serialVersionUID = 7153962474719441460L;

  /**
   * 合并操作id
   */
  private Integer mergeId;

  /**
   * 针对源财务流水id
   */
  private Integer srcFinancialId;

  /**
   * 影响到目标财务流水记录
   */
  private Integer targetFinancialId;

  /**
   * 操作人
   */
  private Integer margeOperator;

  /**
   * 本次操作影响金额
   */
  private Integer price;

  /**
   * 操作记录时间戳
   */
  private Date gmtCreate = new Date();
  
  /**
   * 源财务流水处理前的金额快照
   */
  private Integer srcFinancialPrice;
  
  /**
   * 目标财务处理前的金额快照
   */
  private Integer targetFinancialPrice;
  
  public Integer getMergeId() {
    return mergeId == null ? 0 : mergeId;
  }

  public void setMergeId(Integer mergeId) {
    Preconditions.checkNotNull(mergeId, "mergeId == null");
    Preconditions.checkArgument(mergeId > 0, "mergeId <= 0");
    this.mergeId = mergeId;
  }

  public Integer getSrcFinancialId() {
    return srcFinancialId == null ? 0 : srcFinancialId;
  }

  public void setSrcFinancialId(Integer srcFinancialId) {
    Preconditions.checkNotNull(srcFinancialId, "srcFinancialId == null");
    Preconditions.checkArgument(srcFinancialId > 0, "srcFinancialId <= 0");
    this.srcFinancialId = srcFinancialId;
  }

  public Integer getTargetFinancialId() {
    return targetFinancialId == null ? 0 : targetFinancialId;
  }

  public void setTargetFinancialId(Integer targetFinancialId) {
    Preconditions.checkNotNull(targetFinancialId, "targetFinancialId == null");
    Preconditions.checkArgument(targetFinancialId > 0, "targetFinancialId <= 0");
    this.targetFinancialId = targetFinancialId;
  }

  public Integer getMargeOperator() {
    return margeOperator == null ? 0 : margeOperator;
  }

  public void setMargeOperator(Integer margeOperator) {
    Preconditions.checkNotNull(margeOperator, "margeOperator == null");
    Preconditions.checkArgument(margeOperator > 0, "margeOperator <= 0");
    this.margeOperator = margeOperator;
  }

  public Integer getPrice() {
    return price == null ? 0 : price;
  }

  public void setPrice(Integer price) {
    Preconditions.checkNotNull(price, "price == null");
    Preconditions.checkArgument(price > 0, "price <= 0");
    this.price = price;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    Preconditions.checkNotNull(gmtCreate, "gmtCreate == null");
    this.gmtCreate = gmtCreate;
  }

  public Integer getSrcFinancialPrice() {
    return srcFinancialPrice  == null ? 0 : srcFinancialPrice;
  }

  public void setSrcFinancialPrice(Integer srcFinancialPrice) {
    Preconditions.checkNotNull(srcFinancialPrice, "srcFinancialPrice == null");
    Preconditions.checkArgument(srcFinancialPrice > 0, "srcFinancialPrice <= 0");
    this.srcFinancialPrice = srcFinancialPrice;
  }

  public Integer getTargetFinancialPrice() {
    return targetFinancialPrice  == null ? 0 : targetFinancialPrice;
  }

  public void setTargetFinancialPrice(Integer targetFinancialPrice) {
    Preconditions.checkNotNull(targetFinancialPrice, "targetFinancialPrice == null");
    Preconditions.checkArgument(targetFinancialPrice > 0, "targetFinancialPrice <= 0");
    this.targetFinancialPrice = targetFinancialPrice;
  }
}
