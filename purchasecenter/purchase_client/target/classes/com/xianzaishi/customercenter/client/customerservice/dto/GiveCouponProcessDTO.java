package com.xianzaishi.customercenter.client.customerservice.dto;

/**
 * 赠送优惠券处理
 * 
 * @author zhancang
 */
public class GiveCouponProcessDTO extends ProcessDTO {

  private static final long serialVersionUID = 4618100587475167146L;

  /**
   * 优惠券类型
   */
  private int couponType;

  /**
   * 优惠券数量
   */
  private int count = 1;

  public int getCouponType() {
    return couponType;
  }

  public void setCouponType(int couponType) {
    this.couponType = couponType;
  }

  public int getCount() {
    return count;
  }

  public void setCount(int count) {
    this.count = count;
  }
}
