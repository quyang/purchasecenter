package com.xianzaishi.customercenter.client.customerservice.dto;

import java.io.Serializable;

/**
 * 处理对象
 * 
 * @author zhancang
 */
public abstract class ProcessDTO implements Serializable {

  private static final long serialVersionUID = 3132304215225858735L;

  /**
   * 处理id，比如退款流水id、赠送优惠券流水id
   */
  private String processId;

  public String getProcessId() {
    return processId;
  }

  public void setProcessId(String processId) {
    this.processId = processId;
  }

  /**
   * 处理类型枚举
   * 
   * @author zhancang
   */
  public static class ProcessTypeConstants implements Serializable {

    private static final long serialVersionUID = -346413454453122453L;

    /**
     * 退款方式处理
     */
    public static final Short PROCESS_REFUND = 0;

    /**
     * 赠送优惠券方式处理
     */
    public static final Short PROCESS_GIVE_COUPON = 1;
    
    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return PROCESS_REFUND.equals(parameter) || PROCESS_GIVE_COUPON.equals(parameter);
    }
  }

}
