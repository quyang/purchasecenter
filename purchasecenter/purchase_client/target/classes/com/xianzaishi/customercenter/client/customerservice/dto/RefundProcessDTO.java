package com.xianzaishi.customercenter.client.customerservice.dto;

/**
 * 退款处理
 * @author zhancang
 */
public class RefundProcessDTO extends ProcessDTO{

  private static final long serialVersionUID = -3497599481973006175L;

  /**
   * 对应订单id
   */
  private Long orderId;
  
  /**
   * skuId
   */
  private Long skuId;
  
  /**
   * 对应库存数量
   */
  private Integer count;
  
  /**
   * 产生费用
   */
  private Integer price;

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }
}
