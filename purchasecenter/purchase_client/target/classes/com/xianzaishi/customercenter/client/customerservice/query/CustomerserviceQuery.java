package com.xianzaishi.customercenter.client.customerservice.query;

import java.io.Serializable;
import java.util.Date;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

public class CustomerserviceQuery extends BaseQuery implements Serializable{

  private static final long serialVersionUID = 7692155662086517112L;

  /**
   * 按照任务状态查询
   */
  private Short status;
  /**
   * 任务类型
   */
  private Short type;

  /**
   * 开始时间范围
   */
  private Date begin;

  /**
   * 结束时间范围
   */
  private Date end;

  /**
   * 订单号
   */
  private String bizId;

  public String getBizId() {
    return bizId;
  }

  public void setBizId(String bizId) {
    this.bizId = bizId;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Date getBegin() {
    return begin;
  }

  public void setBegin(Date begin) {
    this.begin = begin;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }
  
}
