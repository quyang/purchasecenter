package com.xianzaishi.customercenter.client.sessionexpireservice;

import com.xianzaishi.itemcenter.common.result.Result;

/**
 * session是否过期服务
 * @author dongpo
 *
 */
public interface SessionExpireService {
  
  /**
   * 判断session是否过期
   * @return
   */
  public Result<Boolean> sessionExpire(String sessionType);

}
