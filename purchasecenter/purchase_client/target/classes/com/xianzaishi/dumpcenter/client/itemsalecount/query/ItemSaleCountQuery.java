package com.xianzaishi.dumpcenter.client.itemsalecount.query;

import java.util.Date;
import java.util.List;

public class ItemSaleCountQuery {
  
  /**
   * sku id列表
   */
  private List<Long> skuIds;
  
  /**
   * 销售日期
   */
  private List<Date> saleDays;
  
  /**
   * 销售渠道，1表示线上，2表示线下
   */
  private Short channelType;

  public List<Long> getSkuIds() {
    return skuIds;
  }

  public void setSkuIds(List<Long> skuIds) {
    this.skuIds = skuIds;
  }

  public List<Date> getSaleDays() {
    return saleDays;
  }

  public void setSaleDays(List<Date> saleDays) {
    this.saleDays = saleDays;
  }

  public Short getChannelType() {
    return channelType;
  }

  public void setChannelType(Short channelType) {
    this.channelType = channelType;
  }

  
}
