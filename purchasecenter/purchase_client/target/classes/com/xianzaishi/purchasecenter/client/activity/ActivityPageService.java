package com.xianzaishi.purchasecenter.client.activity;

import java.util.List;

import com.xianzaishi.itemcenter.common.query.BaseQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityPageDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepDTO;

public interface ActivityPageService {

  /**
   * 插入活动楼层数据
   * 
   * @param activityPageDTO
   * @return
   */
  public Result<Integer> insertPageStepInfo(ActivityPageDTO activityPageDTO);
  
  /**
   * 活动添加楼层数据
   * 
   * @param activityPageDTO
   * @return
   */
  public Result<Integer> addPageStepInfo(ActivityStepDTO activityStepDTO, int pageId);
  
  /**
   * 根据页面id+楼层id查询数据
   */
  public Result<ActivityPageDTO> getPage(int pageId,List<Integer> stepId);
  
  /**
   * 查询所有数据
   */
  public Result<List<ActivityPageDTO>> getPageAll();
  
  /**
   * 根据分页查询数据
   * @return
   */
  public Result<List<ActivityPageDTO>> getPageByPaging(BaseQuery query);
  
  /**
   * 查询数量
   * @return
   */
  public Result<Integer> getPageCount();
  
  /**
   * 更新楼层数据
   * @param activityStepDTO
   * @return
   */
  public Result<Boolean> updateStep(ActivityStepDTO activityStepDTO, int pageId);
  
  /**
   * 重置回滚功能
   * @param activityStepDTO
   * @return
   */
  public Result<Boolean> resetStep(int pageId, int stepId);
  
  /**
   * 删除楼层数据
   * @param stepId
   * @return
   */
  public Result<Boolean> deletePageStepInfo(int stepId);
}
