package com.xianzaishi.purchasecenter.client.activity.dto;

import java.io.Serializable;

/**
 * 文案格式信息
 * 
 * @author zhancang
 */
public class ActivityStepContentDTO implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -3471363791694106763L;

  /**
   * 文字楼层的文字内容
   */
  private String content;

  /**
   * 楼层图片的跳转目标地址，包含跳转h5活动页面或者商品详情页面或者原生活动页面
   */
  private String targetId;

  /**
   * 文字楼层的点击动作效果
   */
  private Short targetType;

  /**
   * 图片标题
   */
  private String titleInfo;
  
  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getTargetId() {
    return targetId;
  }

  public void setTargetId(String targetId) {
    this.targetId = targetId;
  }

  public Short getTargetType() {
    return targetType;
  }

  public void setTargetType(Short targetType) {
    this.targetType = targetType;
  }

  public String getTitleInfo() {
    return titleInfo;
  }

  public void setTitleInfo(String titleInfo) {
    this.titleInfo = titleInfo;
  }
}
