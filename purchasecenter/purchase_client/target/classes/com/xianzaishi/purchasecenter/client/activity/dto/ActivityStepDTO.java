package com.xianzaishi.purchasecenter.client.activity.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 楼层对象
 * 
 * @author zhancang
 */
public class ActivityStepDTO implements Serializable {

  private static final long serialVersionUID = 7907407641993494509L;

  /**
   * 楼层中的图片列表
   */
  private List<ActivityStepPicDTO> picList;
  
  /**
   * 楼层中的文案列表
   */
  private List<ActivityStepContentDTO> contentList;

  /**
   * 楼层的标题
   */
  private String title;

  /**
   * 楼层中的商品列表
   */
  private List<ActivityStepItemDTO> itemList;

  /**
   * 楼层中的类目列表
   */
  private List<ActivityStepCatDTO> catList;
  
  /**
   * 楼层类型
   */
  private Integer stepType;
  
  /**
   * 楼层id
   */
  private Integer stepId;
  
  /**
   * 开始时间
   */
  private Long begin;
  
  /**
   * 结束时间
   */
  private Long end;
  
  /**
   * 排序id字段，该字段不用做json格式化到服务中
   */
  private Integer sortId;
  
  /**
   * 是否有顶部间隔
   */
  private Boolean headSpace = false;
  
  /**
   * 楼层内部楼层列表
   */
  private List<ActivityStepDTO> innerStepList;

  public List<ActivityStepPicDTO> getPicList() {
    return picList;
  }

  public void setPicList(List<ActivityStepPicDTO> picList) {
    this.picList = picList;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<ActivityStepItemDTO> getItemList() {
    return itemList;
  }

  public void setItemList(List<ActivityStepItemDTO> itemList) {
    this.itemList = itemList;
  }

  public Integer getStepType() {
    return stepType;
  }

  public void setStepType(Integer stepType) {
    this.stepType = stepType;
  }
  
  public Integer getStepId() {
    return stepId;
  }

  public void setStepId(Integer stepId) {
    this.stepId = stepId;
  }
  
  public Integer getSortId() {
    return sortId;
  }

  public void setSortId(Integer sortId) {
    this.sortId = sortId;
  }

  public List<ActivityStepCatDTO> getCatList() {
    return catList;
  }

  public void setCatList(List<ActivityStepCatDTO> catList) {
    this.catList = catList;
  }

  public List<ActivityStepDTO> getInnerStepList() {
    return innerStepList;
  }

  public void setInnerStepList(List<ActivityStepDTO> innerStepList) {
    this.innerStepList = innerStepList;
  }

  public Long getBegin() {
    return begin;
  }

  public void setBegin(Long begin) {
    this.begin = begin;
  }

  public Long getEnd() {
    return end;
  }

  public void setEnd(Long end) {
    this.end = end;
  }

  public List<ActivityStepContentDTO> getContentList() {
    return contentList;
  }

  public void setContentList(List<ActivityStepContentDTO> contentList) {
    this.contentList = contentList;
  }
  
  public Boolean getHeadSpace() {
    return headSpace;
  }

  public void setHeadSpace(Boolean headSpace) {
    this.headSpace = headSpace;
  }

  public static class StepTypeConstants implements Serializable {

    private static final long serialVersionUID = -3464189054053122453L;

    /**
     * 多图片轮播列表
     */
    public static final Integer PIC_STEP = 0;
    
    /**
     * 单图
     */
    public static final Integer ONE_PIC_STEP = 4;
    
    /**
     * 图片+商品楼层
     */
    public static final Integer PIC_ITEM_STEP = 1;

    /**
     * 无图片商品楼层
     */
    public static final Integer ITEM_STEP = 2;

    /**
     * 文字+商品列表
     */
    public static final Integer TITLE_ITEM_STEP = 3;

    /**
     * 图片列表
     */
    public static final Integer PIC_LIST_STEP = 5;
    
    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Integer parameter) {
      return PIC_STEP.equals(parameter) || PIC_ITEM_STEP.equals(parameter)
          || ITEM_STEP.equals(parameter) || TITLE_ITEM_STEP.equals(parameter)
          || ONE_PIC_STEP.equals(parameter) || PIC_LIST_STEP.equals(parameter)
          ;
    }
  }
  
//  public static void main(String args[]){
//    ActivityStepDTO step = new ActivityStepDTO();
//    ActivityStepPicDTO dto = new ActivityStepPicDTO();
//    dto.setPicUrl("img.alicdn.com/imgextra/i3/496514980/T29RlkXE8XXXXXXXXX_!!496514980.jpg");
//    dto.setTargetId(1234);
//    dto.setTargetType(ActivityPicTypeConstants.PIC_DEFAULT);
//    step.setPicList(Arrays.asList(dto));
//    
//    ActivityStepItemDTO item1 = new ActivityStepItemDTO();
//    item1.setPicUrl("img.alicdn.com/imgextra/i3/496514980/T29RlkXE8XXXXXXXXX_!!496514980.jpg");
//    item1.setSkuId(1234445454655465L);
//    
//    List<ActivityStepItemDTO> itemList = Lists.newArrayList();
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    itemList.add(item1);
//    step.setItemList(itemList);
//    
//    step.setStepType((short) 1);
//    
//    step.setTitle("cececececececececececece");
//    
//    System.out.println(JackSonUtil.getJson(step));
//    
//  }

}
