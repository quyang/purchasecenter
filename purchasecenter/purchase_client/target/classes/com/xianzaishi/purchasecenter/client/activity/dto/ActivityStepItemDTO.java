package com.xianzaishi.purchasecenter.client.activity.dto;

import java.io.Serializable;

/**
 * 楼层对象中的某个商品对象
 * @author zhancang
 */
public class ActivityStepItemDTO implements Serializable{

  private static final long serialVersionUID = 4945265156484310064L;

  /**
   * 商品的图片
   */
  private String picUrl;
  
  /**
   * 商品的skuId
   */
  private Long itemId;

  public String getPicUrl() {
    return picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl;
  }

  public Long getItemId() {
    return itemId;
  }

  public void setItemId(Long itemId) {
    this.itemId = itemId;
  }
}
