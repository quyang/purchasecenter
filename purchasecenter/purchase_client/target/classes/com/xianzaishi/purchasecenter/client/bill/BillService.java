package com.xianzaishi.purchasecenter.client.bill;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.bill.dto.BillInformationDTO;
import com.xianzaishi.purchasecenter.client.bill.dto.BillInsertDTO;
import com.xianzaishi.purchasecenter.client.bill.dto.BillUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.UserBillDTO;
import java.util.List;

/**
 * 
 * @author quyang 2017-2-13 15:01:30
 *
 */
public interface BillService {
  
  /**
   * 插入用户发票信息
   * @param dto
   * @return
   */
   Result<Boolean> insertBillInfo(UserBillDTO dto);
  
  /**
   * 修改用户发票信息
   * @param dto 
   * @return
   */
  Result<Boolean> updateBillInfo(BillInformationDTO dto);

  /**
   * 查询发票信息 通过oid
   * @param oid
   * @return
   */
  Result<BillInformationDTO> selectBillInfoByOrderId(Long oid);


  /**
   * 更具uid查询用户发票信息
   * @param uid
   * @return
   */
  Result<BillInformationDTO> selectBillInfoByUid(Long uid);


  /**
   * 根据发票id查询用户发票信息
   * @param billNumber
   * @return
   */
  Result<BillInformationDTO> selectBillInfoByBillNum(Long billNumber);

  /**
   * 查询订单是否开发票
   * @param orderId
   * @return
   */
  Result<Boolean> hasBillByOrderId(Long orderId);

  /**
   *
   * @param vo
   * @return
   */
  Result<Boolean> bactchBillVO(BillInsertDTO vo);

  /**
   * 查询用户发票列表
   * @param userId
   * @return
   */
  Result<List<BillUserDTO>> getBillList(Integer userId);
}
