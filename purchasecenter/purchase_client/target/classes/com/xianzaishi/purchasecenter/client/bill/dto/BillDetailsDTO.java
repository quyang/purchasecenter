package com.xianzaishi.purchasecenter.client.bill.dto;

import java.io.Serializable;

/**
 * Created by quyang on 2017/2/17.
 */
public class BillDetailsDTO implements Serializable {


  /**
   * 
   */
  private static final long serialVersionUID = 4804783876573792952L;

  /**
   * 订单id
   */
  private Long orderId ;

  /**
   * 用户id
   */
  private Integer userId ;

  /**
   * 订单金额
   */
  private String money ;

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getMoney() {
    return money;
  }

  public void setMoney(String money) {
    this.money = money;
  }
}
