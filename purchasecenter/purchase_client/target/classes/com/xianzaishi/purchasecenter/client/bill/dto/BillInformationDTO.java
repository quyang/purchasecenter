package com.xianzaishi.purchasecenter.client.bill.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Administrator on 2017/2/15.
 */
public class BillInformationDTO implements Serializable {


  /**
   * 
   */
  private static final long serialVersionUID = -5031005260540785611L;

  /**
   * 订单id
   */
  private Long orderId;

  /**
   * 用户id
   */
  private Integer userId;
  /**
   * 发票号
   */
  private Long billNumber;
  /**
   * 订单金额
   */
  private Long money;

  /**
   * 创建时间
   */
  protected Date gmtCreate;

  /**
   * 修改时间
   */
  protected Date gmtModified;
  /**
   * 发票描述
   */
  private String contribution;

  /**
   * 发票金额
   */
  private String billMoney;

  /**
   * 用户手机号
   */
  private Long phoneNumber;


  public String getBillMoney() {
    return billMoney;
  }

  public void setBillMoney(String billMoney) {
    this.billMoney = billMoney;
  }

  public Long getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(Long phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Long getBillNumber() {
    return billNumber;
  }

  public void setBillNumber(Long billNumber) {
    this.billNumber = billNumber;
  }

  public Long getMoney() {
    return money;
  }

  public void setMoney(Long money) {
    this.money = money;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getContribution() {
    return contribution;
  }

  public void setContribution(String contribution) {
    this.contribution = contribution;
  }
}
