package com.xianzaishi.purchasecenter.client.bill.dto;

import java.io.Serializable;
import java.util.List;

/**
 * Created by quyang on 2017/2/17.
 */
public class BillInsertDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = -8169362648705744552L;


  /**
   * 订单信息
   */
  private List<BillDetailsDTO> orders ;

  /**
   * 发票号
   */
  private Long billNumber ;

  /**
   * 发票描述
   */
  private String contribution ;

  /**
   * 操作员
   */
  private String operator;

  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public List<BillDetailsDTO> getOrders() {
    return orders;
  }

  public void setOrders(
      List<BillDetailsDTO> orders) {
    this.orders = orders;
  }

  public Long getBillNumber() {
    return billNumber;
  }

  public void setBillNumber(Long billNumber) {
    this.billNumber = billNumber;
  }

  public String getContribution() {
    return contribution;
  }

  public void setContribution(String contribution) {
    this.contribution = contribution;
  }
}
