package com.xianzaishi.purchasecenter.client.bill.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by quyang on 2017/2/17.
 */
public class BillUserDTO implements Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 5151139309545367303L;


  /**
   * 发票号
   */
  private Long billNumber;

  /**
   * 发票描述
   */
  private String contribution;


  /**
   * 订单id
   */
  private Long orderId;

  /**
   * 用户id
   */
  private Integer userId;

  /**
   * 订单金额
   */
  private Long money;

  /**
   * 发票金额
   */
  private Long billMoney;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 修改时间
   */
  private Date gmtModified;

  /**
   * 标记
   */
  private Integer tag;

  /**
   * 订单数量
   */
  private Integer orderCount;

  public Long getBillNumber() {
    return billNumber;
  }

  public void setBillNumber(Long billNumber) {
    this.billNumber = billNumber;
  }

  public String getContribution() {
    return contribution;
  }

  public void setContribution(String contribution) {
    this.contribution = contribution;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public Long getMoney() {
    return money;
  }

  public void setMoney(Long money) {
    this.money = money;
  }

  public Long getBillMoney() {
    return billMoney;
  }

  public void setBillMoney(Long billMoney) {
    this.billMoney = billMoney;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Integer getTag() {
    return tag;
  }

  public void setTag(Integer tag) {
    this.tag = tag;
  }

  public Integer getOrderCount() {
    return orderCount;
  }

  public void setOrderCount(Integer orderCount) {
    this.orderCount = orderCount;
  }
}
