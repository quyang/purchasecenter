package com.xianzaishi.purchasecenter.client.contract.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 合同表字段
 * 
 * @author dongpo
 * 
 */
public class ContractDTO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = 488578028454897788L;

  /**
   * 合同id
   */
  private Integer contractId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 合同状态(0：待审核,1：审核通过,2：审核不通过,3：审核中,4：作废)
   */
  private Short status = ContractStatusConstants.CONTRACT_STATUS_PASS;

  /**
   * 合同创建时间
   */
  private Date gmtCreate;

  /**
   * 合同修改时间
   */
  private Date gmtModified;

  /**
   * 多个合同照片地址合集
   */
  private String pic;

  /**
   * 合同内容
   */
  private String content;

  /**
   * 合同生效日期
   */
  private Date begin;

  /**
   * 合同有效结束日期
   */
  private Date end;

  /**
   * 混合数据结构，审核原因+审核人列表
   */
  private String checkinfo;

  public Integer getContractId() {
    return contractId == null ? 0 : contractId;
  }

  public void setContractId(Integer contractId) {
    Preconditions.checkNotNull(contractId, "contractId is null !");
    Preconditions.checkArgument(contractId > 0, "contractId must be greater than 0");
    this.contractId = contractId;
  }

  public Integer getSupplierId() {
    return supplierId == null ? 0 : supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    Preconditions.checkNotNull(supplierId, "supplierId is null !");
    Preconditions.checkArgument(supplierId > 0, "supplierId must be greater than 0");
    this.supplierId = supplierId;
  }

  public Short getStatus() {
    return status == null ? ContractStatusConstants.CONTRACT_STATUS_PASS : status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status is null !");
    Preconditions.checkArgument(ContractStatusConstants.isCorrectParameter(status),
        "status not in(0,1,2,4)");
    this.status = status;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic == null ? null : pic.trim();
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content == null ? null : content.trim();
  }

  public Date getBegin() {
    return begin;
  }

  public void setBegin(Date begin) {
    Preconditions.checkNotNull(begin, "begin is null !");
    this.begin = begin;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    Preconditions.checkNotNull(end, "end is null !");
    this.end = end;
  }

  public String getCheckinfo() {
    return checkinfo;
  }

  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo == null ? null : checkinfo.trim();
  }

  public static class ContractStatusConstants implements Serializable {

    private static final long serialVersionUID = -3464189054053122453L;

    /**
     * 正在待审核状态
     */
    public static final Short CONTRACT_STATUS_WAITCHECK = 0;

    /**
     * 审核通过状态
     */
    public static final Short CONTRACT_STATUS_PASS = 1;

    /**
     * 审核不通过状态
     */
    public static final Short CONTRACT_STATUS_CHECKED_NOTPASS = 2;
    
    /**
     * 审核中
     */
    public static final Short CONTRACT_STATUS_CHECKING = 3;

    /**
     * 草稿版本
     */
    public static final Short CONTRACT_STATUS_DRAFT = 4;


    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return CONTRACT_STATUS_WAITCHECK.equals(parameter) || CONTRACT_STATUS_PASS.equals(parameter)
          || CONTRACT_STATUS_CHECKED_NOTPASS.equals(parameter) || CONTRACT_STATUS_CHECKING.equals(parameter)
          || CONTRACT_STATUS_DRAFT.equals(parameter);
    }
  }
}
