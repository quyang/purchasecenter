package com.xianzaishi.purchasecenter.client.equipment;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.equipment.dto.EmployeeEquipmentDTO;

/**
 * 员工设备关联接口，包括查询关联信息，插入关联信息和更新关联信息
 * 
 * @author dongpo
 * 
 */
public interface EmployeeEquipmentService {

  /**
   * 根据员工号查询设备信息
   * 
   * @param equipmentId
   * @return
   */
  public Result<List<EmployeeEquipmentDTO>> queryEquipmentByUserId(Integer equipmentId);

  /**
   * 插入人员设备关联信息
   * 
   * @param emEquipmentDto
   * @return
   */
  public Result<Integer> insertEquipment(EmployeeEquipmentDTO emEquipmentDto);

  /**
   * 更新人员设备关联信息
   * 
   * @param emEquipmentDto
   * @return
   */
  public Result<Boolean> updateEquipment(EmployeeEquipmentDTO emEquipmentDto);
}
