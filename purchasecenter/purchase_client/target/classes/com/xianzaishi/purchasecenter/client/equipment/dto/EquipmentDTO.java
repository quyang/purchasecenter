package com.xianzaishi.purchasecenter.client.equipment.dto;

import java.io.Serializable;
import java.util.Date;

import com.google.common.base.Preconditions;

/**
 * 设备表字段
 * 
 * @author dongpo
 * 
 */
public class EquipmentDTO implements Serializable {
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -1600215680125146370L;

  /**
   * 设备号
   */
  private Integer equipmentId;

  /**
   * 设备名
   */
  private String name;

  /**
   * 状态(0：设备未激活,1：设备正常,4：设备损坏)
   */
  private Short status = EquipmentStatusConstants.EQUIPMENT_STATUS_NORMAL;

  /**
   * 介绍
   */
  private String introduction;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 修改时间
   */
  private Date gmtModified;

  /**
   * 设备耗费
   */
  private Integer cost;

  /**
   * 设备码
   */
  private String equipmentCode;

  public Integer getEquipmentId() {
    return equipmentId == null ? 0 : equipmentId;
  }

  public void setEquipmentId(Integer equipmentId) {
    Preconditions.checkNotNull(equipmentId, "equipmentId is null !");
    Preconditions.checkArgument(equipmentId>0, "equipmentId must be greater than 0");
    this.equipmentId = equipmentId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name == null ? null : name.trim();
  }

  public Short getStatus() {
    return status == null ? EquipmentStatusConstants.EQUIPMENT_STATUS_NORMAL : status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    this.status = status;
  }

  public String getIntroduction() {
    return introduction;
  }

  public void setIntroduction(String introduction) {
    this.introduction = introduction == null ? null : introduction.trim();
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Integer getCost() {
    return cost == null ? 0 : cost;
  }

  public void setCost(Integer cost) {
    Preconditions.checkNotNull(cost, "cost is null !");
    Preconditions.checkArgument(cost>0, "cost must be greater than 0");
    this.cost = cost;
  }

  public String getEquipmentCode() {
    return equipmentCode;
  }

  public void setEquipmentCode(String equipmentCode) {
    this.equipmentCode = equipmentCode == null ? null : equipmentCode.trim();
  }

  public static class EquipmentStatusConstants implements Serializable {

    private static final long serialVersionUID = -3464189054053122453L;

    /**
     * 设备未激活
     */
    public static final Short EQUIPMENT_STATUS_NOTACTIVE = 0;

    /**
     * 正常使用
     */
    public static final Short EQUIPMENT_STATUS_NORMAL = 1;
    
    /**
     * 设备损坏
     */
    public static final Short EQUIPMENT_STATUS_BREAKDOWN = 4;


    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == EQUIPMENT_STATUS_NOTACTIVE || parameter == EQUIPMENT_STATUS_NORMAL 
          || parameter == EQUIPMENT_STATUS_BREAKDOWN;
    }
  }
}
