package com.xianzaishi.purchasecenter.client.logmonitor;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.logmonitor.dto.LogMonitorDTO;
import com.xianzaishi.purchasecenter.client.logmonitor.query.LogMonitorQuery;
import java.util.List;

/**
 * 监控服务
 * @author dongpo
 *
 */
public interface LogMonitorService {
  /**
   * 插入日志数据
   * @param logMonitorDTO
   * @return
   */
  Result<Integer> insertLog(LogMonitorDTO logMonitorDTO);
  
  /**
   * 更新日志数据  更新时间需自己设置  通过主键更新
   * @param logMonitorDTO
   * @return
   */
  Result<Boolean> updateLog(LogMonitorDTO logMonitorDTO);
  
  /**
   * 根据查询条件查询日志
   * @param query
   * @return
   */
  Result<List<LogMonitorDTO>> queryLog(LogMonitorQuery query);


  /**
   * 插入或更新监控数据
   * @param type
   * @param  isOnlyUpdate true 表示如果监控信息存在就只更新监控数据修改时间；  false表示如果已有监控数据，就修改原有数据的修改时间，并插入新的监控数据，如果没有监控数据，则先插入数据
   * @return
   */
  Result<Boolean> insertOrUpdate(Short type, Boolean isOnlyUpdate);

}
