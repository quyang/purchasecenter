package com.xianzaishi.purchasecenter.client.logmonitor.query;

import java.io.Serializable;
import java.util.Date;

public class LogMonitorQuery implements Serializable{


  private static final long serialVersionUID = -3737610880567499920L;
  /**
   * 监控对象名称
   */
  private String name;

  /**
   * 日志信息
   */
  private String info;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 修改时间
   */
  private Date gmtModified;

  /**
   * 监控事件类型
   */
  private Short type;

  /**
   * 数据状态
   */
  private Short status;


  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }
}
