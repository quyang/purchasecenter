package com.xianzaishi.purchasecenter.client.logmonitorconfig.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * quyang
 */
public class LogMonitorConfigDTO implements Serializable {


  private static final long serialVersionUID = -2503779820052725961L;

  /**
   * 主键id
   */
  private Integer id;

  /**
   * 监听事件类型
   */
  private Short type;

  /**
   * 时间范围
   */
  private Integer timeScope;
  /**
   * 目标手机号码
   */
  private String phone;

  private Date gmtCreate;

  private Date gmtModified;

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Integer getTimeScope() {
    return timeScope;
  }

  public void setTimeScope(Integer timeScope) {
    this.timeScope = timeScope;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }
}
