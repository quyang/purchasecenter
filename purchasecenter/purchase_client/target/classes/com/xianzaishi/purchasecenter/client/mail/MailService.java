package com.xianzaishi.purchasecenter.client.mail;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.mail.dto.SheetDTO;

public interface MailService {
  
  /**
   * 发送邮件，发送人直接在配置文件里面配置
   * @param to 收件人
   * @param copyto 抄送人
   * @param subject 邮件标题
   * @param content 邮件内容
   * @param fileNames 附件地址
   * @return
   */
  Result<Boolean> sendMail(String to, String copyto, String subject, String content,
      List<String> fileNames);
  
  /**
   * 生成xlsx附件
   * @param dataList 数据内容
   * @return
   */
  Result<Boolean> createAttachment(List<SheetDTO> dataList);

}
