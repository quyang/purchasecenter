package com.xianzaishi.purchasecenter.client.mail.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class SheetDTO implements Serializable{
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = 4525902967119215763L;
  /**
   * 表格名称
   */
  private String sheetName;
  
  /**
   * 标题
   */
  private String titleActive;
  
  /**
   * 日期模式
   */
  private String datePattern;
  
  /**
   * 采购日期
   */
  private String purchaseDate;
  
  /**
   * 送货地址
   */
  private String address;
  
  /**
   * 采购单号
   */
  private Integer orderId;
  
  /**
   * 列名
   */
  private List<String> columnNames;
  
  /**
   * 行数据
   */
  private List<Map<String,Object>> rows;
  
  /**
   * 供应商名称
   */
  private String supplierName;
  
  /**
   * 总价格
   */
  private String totalPrice;
  
  /**
   * 采购总数量
   */
  private String totalCount;

  public SheetDTO() {
    
  }  

  public SheetDTO(List<String> columnNames, List<Map<String,Object>> rows) {  
      this.columnNames = columnNames;  
      this.rows = rows;  
  }  

  public String getSheetName() {  
      return sheetName;  
  }  

  public void setSheetName(String sheetName) {  
      this.sheetName = sheetName;  
  }  

  public String getTitleActive() {  
      return titleActive;  
  }  

  public void setTitleActive(String titleActive) {  
      this.titleActive = titleActive;  
  }  

  public List<String> getColumnNames() {  
      return columnNames;  
  }  

  public void setColumnNames(List<String> columnNames) {  
      this.columnNames = columnNames;  
  }  

  public List<Map<String,Object>> getRows() {  
      return rows;  
  }  

  public void setRows(List<Map<String,Object>> rows) {  
      this.rows = rows;  
  }  

  public String getDatePattern() {  
      return datePattern;  
  }  

  public void setDatePattern(String datePattern) {  
      this.datePattern = datePattern;  
  }

  public String getPurchaseDate() {
    return purchaseDate;
  }

  public void setPurchaseDate(String purchaseDate) {
    this.purchaseDate = purchaseDate;
  }

  public Integer getOrderId() {
    return orderId;
  }

  public void setOrderId(Integer orderId) {
    this.orderId = orderId;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getSupplierName() {
    return supplierName;
  }

  public void setSupplierName(String supplierName) {
    this.supplierName = supplierName;
  }

  public String getTotalPrice() {
    return totalPrice;
  }

  public void setTotalPrice(String totalPrice) {
    this.totalPrice = totalPrice;
  }

  public String getTotalCount() {
    return totalCount;
  }

  public void setTotalCount(String totalCount) {
    this.totalCount = totalCount;
  }  

}
