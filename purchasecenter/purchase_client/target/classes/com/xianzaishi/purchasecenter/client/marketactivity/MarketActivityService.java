package com.xianzaishi.purchasecenter.client.marketactivity;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.marketactivity.dto.MarketActivityDTO;
import com.xianzaishi.purchasecenter.client.marketactivity.query.MarketActivityQuery;
import java.util.List;

/**
 * 营销活动接口
 * Created by quyang on 2017/4/14.
 */
public interface MarketActivityService {

  /**
   * 查询活动接口
   * @param marketActivityQuery
   * @return
   */
  Result<List<MarketActivityDTO>> query(MarketActivityQuery marketActivityQuery);


  /**
   * 插入活动接口
   * @param marketActivityDTO
   * @return
   */
  Result<Boolean> insert(MarketActivityDTO marketActivityDTO);



  /**
   * 修改活动接口
   * @param marketActivityDTO
   * @return
   */
  Result<Boolean> update(MarketActivityDTO marketActivityDTO);



  /**
   * 删除活动接口,根据id
   * @return
   */
  Result<Boolean> delete(Integer id);


  /**
   * 获取符合条件的活动数量
   * @param marketActivityQuery
   * @return
   */
  Result<Integer> queryCount(MarketActivityQuery marketActivityQuery);

  /**
   * 定时轮询活动
   * @return
   */
  Result<Boolean> time();
}
