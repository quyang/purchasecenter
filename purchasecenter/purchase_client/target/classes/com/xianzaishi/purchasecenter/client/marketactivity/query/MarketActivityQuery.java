package com.xianzaishi.purchasecenter.client.marketactivity.query;

import com.xianzaishi.itemcenter.common.query.BaseQuery;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by quyang on 2017/5/6.
 */
public class MarketActivityQuery extends BaseQuery implements Serializable{


  private static final long serialVersionUID = 9018471454248533345L;



  /**
   * 活动id
   */
  private Integer id;

  /**
   * 活动名称
   */
  private String name;
  /**
   * 活动状态 0 有效  1 无效
   */
  private Short status;

  /**
   * 优惠内容
   */
  private String couponInfo;

  /**
   * 活动类型
   */
  private Short type;
  /**
   * 开始时间
   */
  private Date gmtStart;

  /**
   * 结束时间
   */
  private Date gmtEnd;

  private Date gmtCreate;

  private Date gmtModified;


  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public String getCouponInfo() {
    return couponInfo;
  }

  public void setCouponInfo(String couponInfo) {
    this.couponInfo = couponInfo;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Date getGmtStart() {
    return gmtStart;
  }

  public void setGmtStart(Date gmtStart) {
    this.gmtStart = gmtStart;
  }

  public Date getGmtEnd() {
    return gmtEnd;
  }

  public void setGmtEnd(Date gmtEnd) {
    this.gmtEnd = gmtEnd;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }


  public static MarketActivityQuery queryById(Integer id) {
    MarketActivityQuery query = new MarketActivityQuery();
    query.setId(id);
    return query;
  }



  public static MarketActivityQuery queryByName(String name) {
    MarketActivityQuery query = new MarketActivityQuery();
    query.setName(name);
    return query;
  }

  public static MarketActivityQuery queryByStatus(Short status) {
    MarketActivityQuery query = new MarketActivityQuery();
    query.setStatus(status);
    return query;
  }

  public static MarketActivityQuery queryByType(Short type) {
    MarketActivityQuery query = new MarketActivityQuery();
    query.setType(type);
    return query;
  }


  public static MarketActivityQuery queryByStartAndEndTime(Date start,Date end) {
    MarketActivityQuery query = new MarketActivityQuery();
    query.setGmtStart(start);
    query.setGmtEnd(end);
    return query;
  }


  public static MarketActivityQuery queryByIdAndName(Integer id, String name) {
    MarketActivityQuery query = new MarketActivityQuery();
    query.setId(id);
    query.setName(name);
    return query;
  }
}
