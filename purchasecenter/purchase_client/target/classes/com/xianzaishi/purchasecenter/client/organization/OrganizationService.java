package com.xianzaishi.purchasecenter.client.organization;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.organization.dto.OrganizationDTO;

/**
 * 门店信息（机构）接口，包括查询机构、插入机构信息以及更新机构信息
 */
public interface OrganizationService {
  /**
   * 通过机构id来查询机构信息
   * 
   * @param organizationId
   * @return
   */
  public Result<OrganizationDTO> queryOrganization(Integer organizationId);
  
  /**
   * 通过当前雇员id查询对应机构
   * 
   * @param organizationId
   * @return
   */
  public Result<OrganizationDTO> queryOrganizationByEmployId(int userId);
  
  /**
   * 通过当前雇员token查询对应机构
   * 
   * @param organizationId
   * @return
   */
  public Result<OrganizationDTO> queryOrganizationByEmployToken(String token);
  
  /**
   * 查询默认机构
   * 
   * @param organizationId
   * @return
   */
  public Result<OrganizationDTO> queryDefaultOrganization();

}
