package com.xianzaishi.purchasecenter.client.organization.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * 门店信息（机构）表字段
 * 
 * @author dongpo
 * 
 */
public class OrganizationDTO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -3592723319514491771L;

  /**
   * 机构id
   */
  private Integer organizationId;

  /**
   * 店长id
   */
  private Integer ownerId;

  /**
   * 机构状态（1：正常状态，0：不可用状态）
   */
  private Short status = OrganizationStatusConstants.ORGANIZATION_STATUS_NORMAL;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 修改时间
   */
  private Date gmtModified;

  /**
   * 机构名
   */
  private String name;

  /**
   * 机构地址
   */
  private String address;

  /**
   * 机构区域，默认记录区域中文
   */
  private String area;

  /**
   * 经度信息，支持最大到小数点后16位。
   */
  private String poiX;

  /**
   * 纬度信息，支持最大到小数点后16位。
   */
  private String poiY;

  /**
   * 机构图片列表
   */
  private String picUrl;

  /**
   * 开始营运时间
   */
  private Date begin;

  /**
   * 结束运营时间
   */
  private Date end;

  /**
   * 扩展属性，暂时不用
   */
  private String feature;

  /**
   * 门店编号
   */
  private Long organizationNum;

  /**
   * 门店秘钥
   */
  private String orgKey;

  public Integer getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(Integer organizationId) {
    this.organizationId = organizationId;
  }

  public Integer getOwnerId() {
    return ownerId;
  }

  public void setOwnerId(Integer ownerId) {
    this.ownerId = ownerId;
  }

  public Short getStatus() {
    return status == null ? OrganizationStatusConstants.ORGANIZATION_STATUS_NORMAL : status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }


  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name == null ? null : name.trim();
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address == null ? null : address.trim();
  }

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area == null ? null : area.trim();
  }

  public String getPoiX() {
    return poiX;
  }

  public void setPoiX(String poiX) {
    this.poiX = poiX == null ? null : poiX.trim();
  }

  public String getPoiY() {
    return poiY;
  }

  public void setPoiY(String poiY) {
    this.poiY = poiY == null ? null : poiY.trim();
  }

  public String getPicUrl() {
    return picUrl;
  }

  public void setPicUrl(String picUrl) {
    this.picUrl = picUrl == null ? null : picUrl.trim();
  }

  public Date getBegin() {
    return begin;
  }

  public void setBegin(Date begin) {
    this.begin = begin;
  }

  public Date getEnd() {
    return end;
  }

  public void setEnd(Date end) {
    this.end = end;
  }

  public String getFeature() {
    return feature;
  }

  public void setFeature(String feature) {
    this.feature = feature == null ? null : feature.trim();
  }

  public Long getOrganizationNum() {
    return organizationNum;
  }

  public void setOrganizationNum(Long organizationNum) {
    this.organizationNum = organizationNum;
  }

  public String getOrgKey() {
    return orgKey;
  }

  public void setOrgKey(String orgKey) {
    this.orgKey = orgKey;
  }


  public static class OrganizationStatusConstants implements Serializable {

    private static final long serialVersionUID = -3464189054053122453L;

    /**
     * 正常状态
     */
    public static final Short ORGANIZATION_STATUS_NORMAL = 1;

    /**
     * 不可用状态
     */
    public static final Short ORGANIZATION_STATUS_DISABLE = 0;


    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return ORGANIZATION_STATUS_NORMAL.equals(parameter) || ORGANIZATION_STATUS_DISABLE.equals(parameter);
    }
  }

}
