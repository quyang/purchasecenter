package com.xianzaishi.purchasecenter.client.purchase;

import java.util.List;
import java.util.Map;

import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseQualityDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.StorageOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.query.PurchaseQuery;

public interface PurchaseService {
  
  /**
   * 查询出来的子采购单map key
   */
  String SUB_PURCHASE_LIST_KEY = "subPurchaseList";
  
  /**
   * 查询出来的子采购单map数量
   */
  String SUB_PURCHASE_COUNT_KEY = "count";

  /**
   * 创建一笔采购单
   * @param purchaseOrderDTO
   * @return
   */
  Result<Integer> insertPurchase(PurchaseOrderDTO purchaseOrderDTO);
  
  /**
   * 查询总采购单信息
   * @param query 查询条件
   * @return
   */
  PagedResult<List<PurchaseOrderDTO>> queryPurchase(PurchaseQuery query);
  
  
  /**
   * 更新一笔子采购单，只更新
   * @param purchaseOrderDTO
   * @return
   */
  Result<Boolean> updatePurchaseSubOrder(PurchaseSubOrderDTO purchaseSubOrderDTO);
  
  /**
   * 创建一个入库记录
   * @param purchaseOrderDTO
   * @return
   */
  Result<Integer> insertStorageOrder(StorageOrderDTO storageOrderDTO);
  
  /**
   * 更新一个入库记录
   * @param purchaseOrderDTO
   * @return
   */
  Result<Boolean> updateStorageOrder(StorageOrderDTO storageOrderDTO);
  
  /**
   * 创建一个质检单
   * @param purchaseOrderDTO
   * @return
   */
  Result<Integer> insertPurchaseQuality(PurchaseQualityDTO purchaseQualityDTO);
  
  /**
   * 更新一个质检单
   * @param purchaseOrderDTO
   * @return
   */
  Result<Boolean> updatePurchaseQuality(PurchaseQualityDTO purchaseQualityDTO);
  
  /**
   * 查询符合条件的采购单列表
   * 
   * @param query
   * @return
   */
  Result<Map<String, Object>> querySubPurchaseList(PurchaseQuery query);

  
  /**
   * 查询符合条件的采购单数量
   * @param query
   * @return
   */
  Result<Integer> querySubPurchaseCount(PurchaseQuery query);
  
  /**
   * 更新采购单
   * @param purchaseOrderDTO
   * @return
   */
  Result<Boolean> updatePurchaseOrder(PurchaseOrderDTO purchaseOrderDTO);
  
  /**
   * 查询一条采购单详细
   * 
   * @param query
   * @return
   */
  Result<PurchaseSubOrderDTO> querySubPurchase(Integer purchaseSubOrderId);
  
  /**
   * 查询一条采购单详细入库记录
   * 
   * @param query
   * @return
   */
  Result<List<StorageOrderDTO>> queryStorageOrderByPurchaseSubOrder(Integer purchaseSubOrderId);
  
  /**
   * 查询一条采购单详细质检记录
   * 
   * @param query
   * @return
   */
  public Result<List<PurchaseQualityDTO>> queryQualityByPurchaseSubOrder(Integer purchaseSubOrderId);
/**
 * 根据采购单ID获取子采购单详情
 * @param purchaseId
 * @return
 */
  public Result<List<PurchaseSubOrderDTO>> querySubPurchaseListByPurId(int purchaseId);
  
  /**
   * 插入子采购单
   * @param purchaseSubOrderDTO
   * @return
   */
  Result<Boolean> insertSubPurchase(PurchaseSubOrderDTO purchaseSubOrderDTO);
  
  /**
   * 插入多条子采购单
   * @param purchaseSubOrderDTO
   * @return
   */
  Result<Boolean> batchInsertSubPurchase(List<PurchaseSubOrderDTO> purchaseSubOrderDTOs);
}
