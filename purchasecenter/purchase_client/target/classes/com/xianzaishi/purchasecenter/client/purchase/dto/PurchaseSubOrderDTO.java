package com.xianzaishi.purchasecenter.client.purchase.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 单笔采购单详细
 * 
 * @author zhancang
 */
public class PurchaseSubOrderDTO implements Serializable {

  /**
   * 
   */
  private static final long serialVersionUID = 4073692057789999061L;

  private Date now = new Date();
  
  /**
   * 创建人
   */
  private String createName;
  
  /**
   * 供应商
   */
  private String supplierName;

  /**
   * 具体单笔sku采购详细id
   */
  private Integer purchaseSubId;

  /**
   * 采购单状态
   */
  private Short status = PurchaseSubOrderStatusConstants.SUBORDER_STATUS_WAITCHECK;

  /**
   * 采购单流转状态
   */
  private Short flowStatus = PurchaseSubOrderFlowStatusConstants.FLOW_STATUS_NOT_START;

  /**
   * 采购单类型
   */
  private Short type = PurchaseSubOrderTypeConstants.PURCHASE_TYPE_NEW;

  /**
   * skuId
   */
  private Long skuId;

  /**
   * 对应总采购单id
   */
  private Integer purchaseId;

  /**
   * 供应商id
   */
  private Integer supplierId;
  
  /**
   * 入库编号
   */
  private Long storageId;

  /**
   * 采购员id
   */
  private Integer purchasingAgent;

  /**
   * 对应合同id
   */
  private Integer contractId;

  /**
   * 标题
   */
  private String title;

  /**
   * 采购生效时间
   */
  private Date purchaseDate;

  /**
   * 期望到货时间
   */
  private Date expectArriveDate;
  
  /**
   * 仓库号
   */
  private Integer wareHouse;

  /**
   * 采购数量
   */
  private Integer count;

  /**
   * 单箱成本
   */
  private Integer unitCost;

  /**
   * 真实结算金额
   */
  private Integer settlementPrice;

  /**
   * 创建人
   */
  private Integer creator;

  /**
   * 
   */
  private Date gmtCreate = now;

  /**
   * 
   */
  private Date gmtModified = now;

  /**
   * 审核失败原因
   */
  private String checkedFailedReason;

  /**
   * 审核人列表
   */
  private List<Integer> checkerList;
  
  /**
   * 审核信息
   */
  private String checkinfo;

  /**
   * 实际采购时间
   */
  private Date arriveDate;
  
  /**
   * 标识是否是称重商品，0标识标品，1标识称重商品
   */
  private Short steelyardSku;

  /**
   * 箱规
   */
  private Integer pcProportion;

  /**
   * 箱规单位
   */
  private String pcProportionUnit;

  /**
   * 是否采购  0 已采购 1 表示未采购
   */
  private Short partTag;
  
  /**
   * 入库详细id
   */
  private Long storageDetailId;

  public Date getArriveDate() {
    return arriveDate;
  }

  public void setArriveDate(Date arriveDate) {
    this.arriveDate = arriveDate;
  }

  public String getCheckinfo() {
    return checkinfo;
  }

  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo == null ? null : checkinfo.trim();
  }

  public Integer getPurchaseSubId() {
    return purchaseSubId;
  }

  public void setPurchaseSubId(Integer purchaseSubId) {
    this.purchaseSubId = purchaseSubId;
  }

  public Short getStatus() {
    return status == null ? PurchaseSubOrderStatusConstants.SUBORDER_STATUS_WAITCHECK : status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Short getFlowStatus() {
    return flowStatus == null ? PurchaseSubOrderFlowStatusConstants.FLOW_STATUS_NOT_START
        : flowStatus;
  }

  public void setFlowStatus(Short flowStatus) {
    this.flowStatus = flowStatus;
  }

  public Short getType() {
    return type == null ? PurchaseSubOrderTypeConstants.PURCHASE_TYPE_NEW : type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public Long getSkuId() {
    return skuId;
  }

  public void setSkuId(Long skuId) {
    this.skuId = skuId;
  }

  public Integer getPurchaseId() {
    return purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    this.purchaseId = purchaseId;
  }

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }

  public Integer getPurchasingAgent() {
    return purchasingAgent;
  }

  public void setPurchasingAgent(Integer purchasingAgent) {
    this.purchasingAgent = purchasingAgent;
  }

  public Integer getContractId() {
    return contractId;
  }

  public void setContractId(Integer contractId) {
    this.contractId = contractId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public Date getPurchaseDate() {
    return purchaseDate;
  }

  public void setPurchaseDate(Date purchaseDate) {
    this.purchaseDate = purchaseDate;
  }

  public Date getExpectArriveDate() {
    return expectArriveDate;
  }

  public void setExpectArriveDate(Date expectArriveDate) {
    this.expectArriveDate = expectArriveDate;
  }

  public Integer getCount() {
    return count == null ? 0 : count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Integer getUnitCost() {
    return unitCost;
  }

  public void setUnitCost(Integer unitCost) {
    this.unitCost = unitCost;
  }

  public Integer getSettlementPrice() {
    return settlementPrice;
  }

  public void setSettlementPrice(Integer settlementPrice) {
    this.settlementPrice = settlementPrice;
  }

  public Integer getWareHouse() {
    return wareHouse;
  }

  public void setWareHouse(Integer wareHouse) {
    this.wareHouse = wareHouse;
  }

  public Integer getCreator() {
    return creator;
  }

  public void setCreator(Integer creator) {
    this.creator = creator;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getCheckedFailedReason() {
    return checkedFailedReason;
  }

  public void setCheckedFailedReason(String checkedFailedReason) {
    this.checkedFailedReason = checkedFailedReason;
  }

  public List<Integer> getCheckerList() {
    return checkerList == null ? Collections.<Integer>emptyList() : checkerList;
  }

  public void setCheckerList(List<Integer> checkerList) {
    this.checkerList = checkerList;
  }
  
  public Long getStorageId() {
    return storageId;
  }

  public void setStorageId(Long storageId) {
    this.storageId = storageId;
  }

  public void setPartTag(Short partTag) {
    this.partTag = partTag;
  }

  public Short getPartTag() {
    return partTag;
  }

  public Short getSteelyardSku() {
    return steelyardSku;
  }

  public void setSteelyardSku(Short steelyardSku) {
    this.steelyardSku = steelyardSku;
  }

  public Integer getPcProportion() {
    return pcProportion;
  }

  public void setPcProportion(Integer pcProportion) {
    this.pcProportion = pcProportion;
  }

  public String getPcProportionUnit() {
    return pcProportionUnit;
  }

  public void setPcProportionUnit(String pcProportionUnit) {
    this.pcProportionUnit = pcProportionUnit;
  }

  public Long getStorageDetailId() {
    return storageDetailId;
  }

  public void setStorageDetailId(Long storageDetailId) {
    this.storageDetailId = storageDetailId;
  }


  /**
   * 采购订单状态类
   * 
   * @author zhancang
   */
  public static class PurchaseSubOrderStatusConstants implements Serializable {

    private static final long serialVersionUID = -3467189058053112453L;
    
    /**
     * 子采购单无效
     */
    public static final Short SUBORDER_STATUS_INVALID = -1;

    /**
     * 草稿状态
     */
    public static final Short SUBORDER_STATUS_WAITCHECK = 0;

    /**
     * 审核通过状态
     */
    public static final Short SUBORDER_STATUS_PASS = 1;

    /**
     * 审核不通过状态
     */
    public static final Short SUBORDER_STATUS_CHECKED_NOTPASS = 2;

    /**
     * 正在审核状态
     */
    public static final Short SUBORDER_STATUS_CHECKING = 3;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return SUBORDER_STATUS_INVALID.equals(parameter) || SUBORDER_STATUS_WAITCHECK.equals(parameter) || SUBORDER_STATUS_PASS.equals(parameter)
          || SUBORDER_STATUS_CHECKED_NOTPASS.equals(parameter) || SUBORDER_STATUS_CHECKING.equals(parameter);
    }
  }

  /**
   * 采购订单流动状态类
   * 
   * @author zhancang
   */
  public static class PurchaseSubOrderFlowStatusConstants implements Serializable {

    private static final long serialVersionUID = -3467189058053122453L;

    /**
     * 未开始
     */
    public static final Short FLOW_STATUS_NOT_START = 0;

    /**
     * 采购中
     */
    public static final Short FLOW_STATUS_PURCHASING = 2;

    /**
     * 已质检
     */
    public static final Short FLOW_STATUS_QUALITYCHECKED = 3;

    /**
     * 拒绝入库
     */
    public static final Short FLOW_STATUS_REJECTSTORAGE = 5;
    /**
     * 入库完成帐期中
     */
    public static final Short FLOW_STATUS_STORAGE_END = 7;

    
    /**
     * 部分入库
     */
    public static final Short STORAGE_STATUS_PART = 8;

    /**
     * 全部入库
     */
    public static final Short STORAGE_STATUS_ALL = 9;

    /**
     * 采购结束
     */
    public static final Short FLOW_STATUS_FINISH = 10;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return FLOW_STATUS_NOT_START.equals(parameter) || FLOW_STATUS_PURCHASING.equals(parameter)
          || FLOW_STATUS_QUALITYCHECKED.equals(parameter) || FLOW_STATUS_REJECTSTORAGE.equals(parameter)
          || FLOW_STATUS_STORAGE_END.equals(parameter) || FLOW_STATUS_FINISH.equals(parameter);
    }
  }

  /**
   * 采购订单流动状态类
   * 
   * @author zhancang
   */
  public static class PurchaseSubOrderTypeConstants implements Serializable {

    private static final long serialVersionUID = -3467189054053122453L;

    /**
     * 新品采购
     */
    public static final Short PURCHASE_TYPE_NEW = 0;

    /**
     * 补货采购
     */
    public static final Short PURCHASE_TYPE_REPLENISHMENT = 1;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return PURCHASE_TYPE_NEW.equals(parameter) || PURCHASE_TYPE_REPLENISHMENT.equals(parameter);
    }
  }

public String getCreateName() {
	return createName;
}

public void setCreateName(String createName) {
	this.createName = createName;
}

public String getSupplierName() {
	return supplierName;
}

public void setSupplierName(String supplierName) {
	this.supplierName = supplierName;
}


  /**
   * 采购单入库类型
   *
   * @author zhancang
   */
  public static class PurchaseSubOrderStorageTypeConstants implements Serializable {

    private static final long serialVersionUID = -3467189054053122453L;

    /**
     * 已采购
     */
    public static final Short PURCHASE_YES = 0;

    /**
     * 未采购
     */
    public static final Short PURCHASE_NO = 1;


  }
  
}
