package com.xianzaishi.purchasecenter.client.purchasereturn;

import java.util.Map;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.purchasereturn.dto.PurchaseReturnOrderDTO;
import com.xianzaishi.purchasecenter.client.purchasereturn.query.PurchaseReturnQuery;

public interface PurchaseReturnService {

  /**
   * 创建一笔退货单
   * 
   * @param purchaseOrderDTO
   * @return
   */
  public Result<Integer> insertPurchaseReturn(PurchaseReturnOrderDTO purchaseReturnOrderDTO);

  /**
   * 更新一笔退货单
   * 
   * @param purchaseOrderDTO
   * @return
   */
  public Result<Boolean> updatePurchaseReturn(PurchaseReturnOrderDTO purchaseReturnOrderDTO);

  /**
   * 查询符合条件的采购单列表
   * 
   * @param query
   * @return
   */
  public Result<Map<String, Object>> queryPurchaseReturn(PurchaseReturnQuery query);
}
