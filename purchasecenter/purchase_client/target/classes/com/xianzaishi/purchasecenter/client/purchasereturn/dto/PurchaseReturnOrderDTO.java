package com.xianzaishi.purchasecenter.client.purchasereturn.dto;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * 退货单详细
 * 
 * @author zhancang
 */
public class PurchaseReturnOrderDTO implements Serializable {

  private static final long serialVersionUID = 5974930139377816976L;

  private Date now = new Date();

  /**
   * 退货单id
   */
  private Integer returnId;

  /**
   * 对应子采购单
   */
  private Integer purchaseId;

  /**
   * 退货单状态
   */
  private Short status = PurchaseReturnOrderStatusConstants.STATUS_WAITCHECK;

  /**
   * 
   */
  private Long skuId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 采购id
   */
  private Integer purchasingAgent;

  /**
   * 
   */
  private String title;

  /**
   * 退货数量
   */
  private Integer count;

  /**
   * 退货价格
   */
  private Integer unitCost;

  /**
   * 创建人
   */
  private Integer creator;

  /**
   * 退货理由
   */
  private String reason;

  /**
   * 
   */
  private Date gmtCreate = now;

  /**
   * 
   */
  private Date gmtModified = now;

  /**
   * 审核失败原因
   */
  private String checkedFailedReason;

  /**
   * 审核人列表
   */
  private List<Integer> checkerList;

  public Integer getReturnId() {
    return returnId == null ? 0 : returnId;
  }

  public void setReturnId(Integer returnId) {
    Preconditions.checkNotNull(returnId, "returnId == null");
    Preconditions.checkArgument(returnId > 0, "returnId <= 0");
    this.returnId = returnId;
  }

  public Integer getPurchaseId() {
    return purchaseId == null ? 0 : purchaseId;
  }

  public void setPurchaseId(Integer purchaseId) {
    Preconditions.checkNotNull(purchaseId, "purchaseId == null");
    Preconditions.checkArgument(purchaseId > 0, "purchaseId <= 0");
    this.purchaseId = purchaseId;
  }

  public Short getStatus() {
    return status == null ? PurchaseReturnOrderStatusConstants.STATUS_WAITCHECK : status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument(PurchaseReturnOrderStatusConstants.isCorrectParameter(status), "status not in(0,1,2,3)");
    this.status = status;
  }

  public Long getSkuId() {
    return skuId == null ? 0 : skuId;
  }

  public void setSkuId(Long skuId) {
    Preconditions.checkNotNull(skuId, "skuId == null");
    Preconditions.checkArgument(skuId > 0, "skuId <= 0");
    this.skuId = skuId;
  }

  public Integer getSupplierId() {
    return supplierId == null ? 0 : supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    Preconditions.checkNotNull(supplierId, "supplierId == null");
    Preconditions.checkArgument(supplierId > 0, "supplierId <= 0");
    this.supplierId = supplierId;
  }

  public Integer getPurchasingAgent() {
    return purchasingAgent == null ? 0 : purchasingAgent;
  }

  public void setPurchasingAgent(Integer purchasingAgent) {
    Preconditions.checkNotNull(purchasingAgent, "purchasingAgent == null");
    Preconditions.checkArgument(purchasingAgent > 0, "purchasingAgent <= 0");
    this.purchasingAgent = purchasingAgent;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title == null ? null : title.trim();
  }

  public Integer getCount() {
    return count == null ? 0 : count;
  }

  public void setCount(Integer count) {
    Preconditions.checkNotNull(count, "count == null");
    Preconditions.checkArgument(count > 0, "count <= 0");
    this.count = count;
  }

  public Integer getUnitCost() {
    return unitCost == null ? 0 : unitCost;
  }

  public void setUnitCost(Integer unitCost) {
    Preconditions.checkNotNull(unitCost, "unitCost == null");
    Preconditions.checkArgument(unitCost > 0, "unitCost <= 0");
    this.unitCost = unitCost;
  }

  public Integer getCreator() {
    return creator == null ? 0 : creator;
  }

  public void setCreator(Integer creator) {
    Preconditions.checkNotNull(creator, "creator == null");
    Preconditions.checkArgument(creator > 0, "creator <= 0");
    this.creator = creator;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason == null ? null : reason.trim();
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    Preconditions.checkNotNull(gmtCreate, "creator == null");
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    Preconditions.checkNotNull(gmtModified, "gmtModified == null");
    this.gmtModified = gmtModified;
  }

  public String getCheckedFailedReason() {
    return checkedFailedReason;
  }

  public void setCheckedFailedReason(String checkedFailedReason) {
    this.checkedFailedReason = checkedFailedReason;
  }

  public List<Integer> getCheckerList() {
    return checkerList == null ? Collections.<Integer>emptyList():checkerList;
  }

  public void setCheckerList(List<Integer> checkerList) {
    this.checkerList = checkerList;
  }

  /**
   * 采购退货单状态类
   * 
   * @author zhancang
   */
  public static class PurchaseReturnOrderStatusConstants implements Serializable {

    private static final long serialVersionUID = -3467189348053112453L;

    /**
     * 正在待审核状态
     */
    public static final Short STATUS_WAITCHECK = 0;

    /**
     * 审核通过状态
     */
    public static final Short STATUS_PASS = 1;

    /**
     * 审核不通过状态
     */
    public static final Short STATUS_CHECKED_NOTPASS = 2;

    /**
     * 正在审核状态
     */
    public static final Short STATUS_CHECKING = 3;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == STATUS_WAITCHECK || parameter == STATUS_PASS
          || parameter == STATUS_CHECKED_NOTPASS || parameter == STATUS_CHECKING;
    }
  }
}
