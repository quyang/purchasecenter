package com.xianzaishi.purchasecenter.client.role;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.role.dto.RoleDTO;

/**
 * 角色接口，包括查询角色、插入角色以及更新角色信息
 * 
 * @author dongpo
 * 
 */
public interface RoleService {

  /**
   * 通过角色id查询角色
   * 
   * @param role
   * @return
   */
  public Result<RoleDTO> queryRoleById(Integer role);

  /**
   * 通过角色id查询角色
   * 
   * @param role
   * @return
   */
  public Result<RoleDTO> queryRoleByName(String name);
  
  /**
   * 插入角色
   * 
   * @param roleDto
   * @return
   */
  public Result<Integer> insertRole(RoleDTO roleDto);

  /**
   * 更新角色
   * 
   * @param roleDto
   * @return
   */
  public Result<Boolean> updateRole(RoleDTO roleDto);
  
  /**
   * 查询一个节点需要的角色列表
   */
  public Result<List<RoleDTO>> queryRoleList(String pageId);
  
  /**
   * 给某个节点增加角色
   */
  public Result<Boolean> addRole(String pageId, Integer roleId);
  
//  /**
//   * 给某个节点创建并增加角色
//   */
//  public Result<Boolean> createAndAddRole(String pageId, RoleDTO role);
  
  /**
   * 给某个节点减角色
   */
  public Result<Boolean> romoveRole(String pageId, Integer roleId);
}
