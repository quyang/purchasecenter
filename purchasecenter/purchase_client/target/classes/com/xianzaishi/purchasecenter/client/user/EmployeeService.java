package com.xianzaishi.purchasecenter.client.user;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.EmployeeDTO;
import com.xianzaishi.purchasecenter.client.user.dto.UserDTO;
import com.xianzaishi.purchasecenter.client.user.query.EmployeeQuery;
import java.util.List;

/**
 * 雇员接口，包括单条查询和批量查询员工信息，插入和更新员工信息表
 * 
 * @author dongpo
 * 
 */
public interface EmployeeService {

  /**
   * 通过员工号查询员工信息
   * 
   * @param userId
   * @return
   */
  public Result<EmployeeDTO> queryEmployeeById(Integer userId);
  
  /**
   * 通过员工名称查询员工信息
   * 
   * @param userId
   * @return
   */
  public Result<EmployeeDTO> queryEmployeeByName(String userName);

  /**
   * 通过员工名、花名、角色id、机构id等查询员工信息
   * 
   * @param query
   * @return
   */
  public Result<List<EmployeeDTO>> queryEmployeeByQuery(EmployeeQuery query);
  
  /**
   * 插入员工信息
   * 
   * @param employeeDto
   * @return
   */
  public Result<Integer> insertEmployee(EmployeeDTO employeeDto);
  

  /**
   * 更新员工信息
   * 
   * @param employeeDto
   * @return
   */
  public Result<Boolean> updateEmployee(EmployeeDTO employeeDto);

  /**
   * 查询userDO
   * @param phone
   * @return
   */
  public Result<BackGroundUserDTO> queryUserDOByPhone(Long phone);

}
