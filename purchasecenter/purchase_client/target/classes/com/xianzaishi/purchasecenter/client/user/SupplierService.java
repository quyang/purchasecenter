package com.xianzaishi.purchasecenter.client.user;

import java.util.List;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierSkuDTO;
import com.xianzaishi.purchasecenter.client.user.query.SupplierQuery;

public interface SupplierService {

  /**
   * 创建一个供应商详细
   * 
   * @param userContactDTO
   * @return
   */
  public Result<Integer> insertSupplier(SupplierDTO supplierDTO);
  
  /**
   * 更新一个供应商详细
   * 
   * @param userContactDTO
   * @return
   */
  public Result<Boolean> updateSupplier(SupplierDTO supplierDTO);
  
  /**
   * 查询符合条件的供应商
   * @param query
   * @return
   */
  public Result<List<SupplierDTO>> querySupplier(SupplierQuery query);
  
  /**
   * 查询符合条件的供应商数量
   * @param query
   * @return
   */
  Result<Integer> querySupplierCount(SupplierQuery query);
  
  /**
   * 查询符合条件的供应商
   * @param userId
   * @return
   */
  public Result<SupplierDTO> querySupplierById(int userId, boolean includeAll);
  
  /**
   * 查询符合条件的供应商
   * @param userId
   * @return
   */
  public Result<SupplierDTO> querySupplierByName(String name, boolean includeAll);
  
  /**
   * 关联一个sku与供应商
   * @param supplierSkuDTO
   * @return
   */
  public Result<Boolean> insertSupplierSkuRelation(SupplierSkuDTO supplierSkuDTO);
  
  /**
   * 更新一个sku与供应商
   * @param supplierSkuDTO
   * @return
   */
  public Result<Boolean> updateSupplierSkuRelation(SupplierSkuDTO supplierSkuDTO);
  
  /**
   * 查询一个sku与供应商的关系
   * @param supplierId
   * @param skuId
   * @return
   */
  public Result<SupplierSkuDTO> querySupplierSkuRelation(int supplierId,long skuId);
  
  /**
   * 查询供应商列表，当前供应商列表排列第一位。暂时只会返回供应商基本信息
   * @param skuId
   * @return
   */
  public Result<List<SupplierDTO>> querySkuSupplierList(long skuId);
}
