package com.xianzaishi.purchasecenter.client.user.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author quyang
 */
public class UserBillDTO implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -8752877269552340855L;

  /**
   * 订单id
   */
  private Long orderId;

  /**
   * 用户id
   */
  private Integer userId;
  /**
   * 发票号
   */
  private Long billNumber;
  /**
   * 发票金额
   */
  private Long money;

  /**
   * 创建时间
   */
  protected Date gmtCreate;

  /**
   * 修改时间
   */
  protected Date gmtModified;
  /**
   * 发票描述
   */
  private String contribution;

  /**
   * 操作员
   */
  private String operator;


  public String getOperator() {
    return operator;
  }

  public void setOperator(String operator) {
    this.operator = operator;
  }

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  public Long getBillNumber() {
    return billNumber;
  }

  public void setBillNumber(Long billNumber) {
    this.billNumber = billNumber;
  }

  public Long getMoney() {
    return money;
  }

  public void setMoney(Long money) {
    this.money = money;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public String getContribution() {
    return contribution;
  }

  public void setContribution(String contribution) {
    this.contribution = contribution;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }
}
