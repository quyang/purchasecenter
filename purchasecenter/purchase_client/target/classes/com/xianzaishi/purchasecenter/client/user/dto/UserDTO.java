package com.xianzaishi.purchasecenter.client.user.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by quyang on 2017/2/27.
 */
public class UserDTO implements Serializable {



  /**
   * 
   */
  private static final long serialVersionUID = -3336230928068967425L;


  public static final String ROLE_SPLIT = ";";// 角色字段底层分隔符
  /**
   * 用户id
   */
  protected Integer userId;

  /**
   * 用户名称
   */
  protected String name;

  /**
   * 雇员登陆token
   */
  protected String token;

  /**
   * 用户类型
   */
  protected Short userType;

  /**
   * 创建时间
   */
  protected Date gmtCreate;

  /**
   * 修改时间
   */
  protected Date gmtModified;

  /**
   * token过期时间
   */
  protected Date expireTime;

  /**
   * 硬件编码
   */
  protected String hardwareCode;

  /**
   * 用户角色
   */
  protected String role;

  /**
   * 密码数据
   */
  protected String pwd;

  /**
   * 电话号码
   */
  protected Long phone;

  /**
   * 激活码、创建时间混合字段 code***;timelong****
   */
  protected String activeCode;


  public static String getRoleSplit() {
    return ROLE_SPLIT;
  }

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short userType) {
    this.userType = userType;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Date getExpireTime() {
    return expireTime;
  }

  public void setExpireTime(Date expireTime) {
    this.expireTime = expireTime;
  }

  public String getHardwareCode() {
    return hardwareCode;
  }

  public void setHardwareCode(String hardwareCode) {
    this.hardwareCode = hardwareCode;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public String getPwd() {
    return pwd;
  }

  public void setPwd(String pwd) {
    this.pwd = pwd;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }

  public String getActiveCode() {
    return activeCode;
  }

  public void setActiveCode(String activeCode) {
    this.activeCode = activeCode;
  }
}
