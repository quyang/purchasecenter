package com.xianzaishi.purchasecenter.client.user.dto.supplier;

import java.io.Serializable;

import com.google.common.base.Preconditions;

/**
 * 供应商公司信息
 * 
 * @author zhancang
 */
public class SupplierCompanyDTO implements Serializable {

  private static final long serialVersionUID = 3747881551550710001L;

  /**
   * 供应商供应类型
   */
  private Short supplierType = SUPPLIER_TYPE_CONSTANTS.SUPPLIERTYPE_RAWMATERIALS;

  /**
   * 供应商公司类型
   */
  private Short companyType = COMPANY_TYPE_CONSTANTS.COMPANY_LIMITED_LIABILITY;

  /**
   * 公司经营方针
   */
  private Short marketingType = MARKETING_TYPE_CONSTANTS.MARKETING_DIRECT;

  /**
   * 送货方式
   */
  private Short deliveryType = DELIVERY_TYPE_CONSTANTS.DELIVERY_BY_COMPANY;

  private Integer supplierId;

  /**
   * 公司名称
   */
  private String companyName;

  /**
   * 公司地址
   */
  private String companyAddress;

  /**
   * 公司邮编
   */
  private String companyPostcode;

  /**
   * 传真号
   */
  private String companyFax;

  /**
   * 公司邮件地址
   */
  private String companyEmail;

  /**
   * 公司联系电话
   */
  private String companyPhone;

  /**
   * 公司法人
   */
  private String juridicalPerson;

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    this.supplierId = supplierId;
  }
  
  public Short getSupplierType() {
    return supplierType == null ? SUPPLIER_TYPE_CONSTANTS.SUPPLIERTYPE_RAWMATERIALS : supplierType;
  }

  public void setSupplierType(Short supplierType) {
    Preconditions.checkNotNull(supplierType, "supplierType == null");
    Preconditions.checkArgument(SUPPLIER_TYPE_CONSTANTS.isCorrectParameter(supplierType),
        "supplierType not in(1,2,3,4,5,6)");
    this.supplierType = supplierType;
  }

  public Short getCompanyType() {
    return companyType == null ? COMPANY_TYPE_CONSTANTS.COMPANY_LIMITED_LIABILITY : companyType;
  }

  public void setCompanyType(Short companyType) {
    Preconditions.checkNotNull(companyType, "companyType == null");
    Preconditions.checkArgument(COMPANY_TYPE_CONSTANTS.isCorrectParameter(companyType),
        "companyType not in(1,2,3,4,5)");
    this.companyType = companyType;
  }

  public Short getMarketingType() {
    return marketingType == null ? MARKETING_TYPE_CONSTANTS.MARKETING_DIRECT : marketingType;
  }

  public void setMarketingType(Short marketingType) {
    Preconditions.checkNotNull(marketingType, "marketingType == null");
    Preconditions.checkArgument(MARKETING_TYPE_CONSTANTS.isCorrectParameter(marketingType),
        "marketingType not in(1,2)");
    this.marketingType = marketingType;
  }

  public Short getDeliveryType() {
    return deliveryType == null ? DELIVERY_TYPE_CONSTANTS.DELIVERY_BY_COMPANY : deliveryType;
  }

  public void setDeliveryType(Short deliveryType) {
    Preconditions.checkNotNull(deliveryType, "deliveryType == null");
    Preconditions.checkArgument(DELIVERY_TYPE_CONSTANTS.isCorrectParameter(deliveryType),
        "deliveryType not in(1,2)");

    this.deliveryType = deliveryType;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName == null ? null : companyName.trim();
  }

  public String getCompanyAddress() {
    return companyAddress;
  }

  public void setCompanyAddress(String companyAddress) {
    this.companyAddress = companyAddress == null ? null : companyAddress.trim();
  }

  public String getCompanyPostcode() {
    return companyPostcode;
  }

  public void setCompanyPostcode(String companyPostcode) {
    this.companyPostcode = companyPostcode == null ? null : companyPostcode.trim();
  }

  public String getCompanyFax() {
    return companyFax;
  }

  public void setCompanyFax(String companyFax) {
    this.companyFax = companyFax == null ? null : companyFax.trim();
  }

  public String getCompanyEmail() {
    return companyEmail;
  }

  public void setCompanyEmail(String companyEmail) {
    this.companyEmail = companyEmail == null ? null : companyEmail.trim();
  }

  public String getCompanyPhone() {
    return companyPhone;
  }

  public void setCompanyPhone(String companyPhone) {
    this.companyPhone = companyPhone == null ? null : companyPhone.trim();
  }

  public String getJuridicalPerson() {
    return juridicalPerson;
  }

  public void setJuridicalPerson(String juridicalPerson) {
    this.juridicalPerson = juridicalPerson == null ? null : juridicalPerson.trim();
  }

  /**
   * 定义公司供应类型
   * 
   * @author zhancang
   */
  public static class SUPPLIER_TYPE_CONSTANTS implements Serializable {

    private static final long serialVersionUID = -6859796701554627299L;

    /**
     * 原材料供应商
     */
    public static final Short SUPPLIERTYPE_RAWMATERIALS = 1;

    /**
     * 包装供应商
     */
    public static final Short SUPPLIERTYPE_PACKAGING = 2;

    /**
     * 低值易耗供应商
     */
    public static final Short SUPPLIERTYPE_LOWVALUE_CONSUMABLES = 3;

    /**
     * 固定资产供应商
     */
    public static final Short SUPPLIERTYPE_FIXED_ASSETS = 4;

    /**
     * 服务产品供应商
     */
    public static final Short SUPPLIERTYPE_SERVICE = 5;
    
    /**
     * 服务产品供应商
     */
    public static final Short SUPPLIERTYPE_FRESH = 6;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      if(null == parameter){
        return false;
      }
      return parameter.equals(SUPPLIERTYPE_RAWMATERIALS) || parameter.equals(SUPPLIERTYPE_PACKAGING)
          || parameter.equals(SUPPLIERTYPE_LOWVALUE_CONSUMABLES) || parameter.equals(SUPPLIERTYPE_FRESH) 
          || parameter.equals(SUPPLIERTYPE_FIXED_ASSETS) || parameter.equals(SUPPLIERTYPE_SERVICE);
    }
  }

  /**
   * 定义公司资本类型
   * 
   * @author zhancang
   */
  public static class COMPANY_TYPE_CONSTANTS implements Serializable {

    private static final long serialVersionUID = -6859796701454627299L;

    /**
     * 有限责任公司
     */
    public static final Short COMPANY_LIMITED_LIABILITY = 1;

    /**
     * 中外合作经营企业
     */
    public static final Short COMPANY_JOINT_VENTURES = 2;

    /**
     * 私营有限责任公司
     */
    public static final Short COMPANY_PRIVATE_LIMITED_LIABILITY = 3;

    /**
     * 外资企业
     */
    public static final Short COMPANY_FOREIGN_FUNDED = 4;

    /**
     * 港、澳、台商独资经营
     */
    public static final Short COMPANY_HK_MACAO_TAIWAN = 5;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      if(null == parameter){
        return false;
      }
      return parameter.shortValue() == COMPANY_LIMITED_LIABILITY.shortValue() || parameter.shortValue() == COMPANY_JOINT_VENTURES.shortValue()
          || parameter.shortValue() == COMPANY_PRIVATE_LIMITED_LIABILITY.shortValue() || parameter.shortValue() == COMPANY_FOREIGN_FUNDED.shortValue()
          || parameter.shortValue() == COMPANY_HK_MACAO_TAIWAN.shortValue();
    }
  }

  /**
   * 定义公司经营类型
   * 
   * @author zhancang
   */
  public static class MARKETING_TYPE_CONSTANTS implements Serializable {

    private static final long serialVersionUID = -6859796711454627299L;

    /**
     * 直营经营
     */
    public static final Short MARKETING_DIRECT = 1;

    /**
     * 加盟经营
     */
    public static final Short MARKETING_JOINING = 2;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      if(null == parameter){
        return false;
      }
      return parameter.shortValue() == MARKETING_DIRECT.shortValue() || parameter.shortValue() == MARKETING_JOINING.shortValue();
    }
  }

  /**
   * 定义公司送货方式
   * 
   * @author zhancang
   */
  public static class DELIVERY_TYPE_CONSTANTS implements Serializable {

    private static final long serialVersionUID = -6859796711454627299L;

    /**
     * 送货上门
     */
    public static final Short DELIVERY_BY_COMPANY = 1;

    /**
     * 上门自提
     */
    public static final Short DELIVERY_BY_SELF_COLLECTION = 2;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      if(null == parameter){
        return false;
      }
      return parameter.shortValue() == DELIVERY_BY_COMPANY.shortValue() || parameter.shortValue() == DELIVERY_BY_SELF_COLLECTION.shortValue();
    }
  }
}
