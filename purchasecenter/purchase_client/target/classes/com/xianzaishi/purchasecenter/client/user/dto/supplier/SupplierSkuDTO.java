package com.xianzaishi.purchasecenter.client.user.dto.supplier;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.common.base.Preconditions;

/**
 * 供应商切换sku
 * 
 * @author zhancang
 */
public class SupplierSkuDTO implements Serializable {

  private static final long serialVersionUID = -5363481944278058684L;

  /**
   * skuId
   */
  private Long skuId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 供应商类型
   */
  private Short relationType = SupplierSkuRelationTypeConstants.SUPPLIER_SKU_MAJOR;

  /**
   * 
   */
  private Date gmtCreate;

  /**
   * 
   */
  private Date gmtModified;

  /**
   * 审核失败原因
   */
  private String checkedFailedReason;

  /**
   * 审核人列表
   */
  private List<Integer> checkerList;

  /**
   * 当前状态
   */
  private Short status = SupplierSkuRelationStatusConstants.SUPPLIER_STATUS_PASS;

  public Long getSkuId() {
    return skuId == null ? 0 : skuId;
  }

  public void setSkuId(Long skuId) {
    Preconditions.checkNotNull(skuId, "skuId == null");
    Preconditions.checkArgument(skuId > 0, "skuId <= 0");
    this.skuId = skuId;
  }

  public Integer getSupplierId() {
    return supplierId == null ? 0 : supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    Preconditions.checkNotNull(supplierId, "supplierId == null");
    Preconditions.checkArgument(supplierId > 0, "supplierId <= 0");
    this.supplierId = supplierId;
  }

  public Short getRelationType() {
    return relationType == null ? SupplierSkuRelationTypeConstants.SUPPLIER_SKU_MINOR
        : relationType;
  }

  public void setRelationType(Short relationType) {
    Preconditions.checkNotNull(relationType, "relationType == null");
    Preconditions.checkArgument(SupplierSkuRelationTypeConstants.isCorrectParameter(relationType),
        "relationType not in(1,0)");
    this.relationType = relationType;
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  public Short getStatus() {
    return status == null ? SupplierSkuRelationStatusConstants.SUPPLIER_STATUS_CHECKED_NOTPASS
        : null;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument(SupplierSkuRelationStatusConstants.isCorrectParameter(status),
        "status not in(1,4)");
    this.status = status;
  }

  public String getCheckedFailedReason() {
    return checkedFailedReason;
  }

  public void setCheckedFailedReason(String checkedFailedReason) {
    this.checkedFailedReason = checkedFailedReason;
  }

  public List<Integer> getCheckerList() {
    return checkerList == null ? Collections.<Integer>emptyList() : checkerList;
  }

  public void setCheckerList(List<Integer> checkerList) {
    this.checkerList = checkerList;
  }

  /**
   * 供应商sku关系状态类
   * 
   * @author zhancang
   */
  public static class SupplierSkuRelationStatusConstants implements Serializable {

    private static final long serialVersionUID = -3467181058053114453L;

    /**
     * 审核通过状态
     */
    public static final Short SUPPLIER_STATUS_PASS = 1;

    /**
     * 审核不通过状态
     */
    public static final Short SUPPLIER_STATUS_CHECKED_NOTPASS = 4;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == SUPPLIER_STATUS_PASS || parameter == SUPPLIER_STATUS_CHECKED_NOTPASS;
    }
  }

  /**
   * 供应商sku关系状态类
   * 
   * @author zhancang
   */
  public static class SupplierSkuRelationTypeConstants implements Serializable {

    private static final long serialVersionUID = -3467181058453114453L;

    /**
     * sku主供应商
     */
    public static final Short SUPPLIER_SKU_MAJOR = 1;

    /**
     * sku普通供应商
     */
    public static final Short SUPPLIER_SKU_MINOR = 0;

    /**
     * 是否正确的参数
     * 
     * @param parameter
     * @return
     */
    public static boolean isCorrectParameter(Short parameter) {
      return parameter == SUPPLIER_SKU_MAJOR || parameter == SUPPLIER_SKU_MINOR;
    }
  }
}
