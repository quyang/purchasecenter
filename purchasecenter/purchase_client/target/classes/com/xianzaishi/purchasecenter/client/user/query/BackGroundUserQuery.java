package com.xianzaishi.purchasecenter.client.user.query;

import java.io.Serializable;
import java.util.List;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

public class BackGroundUserQuery extends BaseQuery implements Serializable{
  
  /**
   * serial version UID
   */
  private static final long serialVersionUID = -3987762646407556055L;

  /**
   * 根据用户id查找
   */
  private List<Integer> userIds;
  
  /**
   * 根据用户名查找
   */
  private String name;
  
  /**
   * 根据用户类型查找
   */
  private Short userType;
  
  /**
   * 根据用户手机号查找
   */
  private Long phone;

  public List<Integer> getUserIds() {
    return userIds;
  }

  public void setUserIds(List<Integer> userIds) {
    this.userIds = userIds;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Short getUserType() {
    return userType;
  }

  public void setUserType(Short userType) {
    this.userType = userType;
  }

  public Long getPhone() {
    return phone;
  }

  public void setPhone(Long phone) {
    this.phone = phone;
  }
  
}
