package com.xianzaishi.purchasecenter.client.user.query;

import java.io.Serializable;

import com.xianzaishi.itemcenter.common.query.BaseQuery;

/**
 * 员工查询条件，包括员工名、花名、角色id以及机构id
 * 
 * @author dongpo
 * 
 */
public class EmployeeQuery extends BaseQuery implements Serializable {

  /**
   * serial version UID
   */
  private static final long serialVersionUID = -3677401029963713595L;

  /**
   * 机构id
   */
  private Integer organizationId;

  public Integer getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(Integer organizationId) {
    this.organizationId = organizationId;
  }



}
