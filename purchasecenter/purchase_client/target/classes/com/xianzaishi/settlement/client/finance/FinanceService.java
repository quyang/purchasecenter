package com.xianzaishi.settlement.client.finance;

import java.util.Map;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.settlement.client.finance.dto.FinancialOrderDTO;
import com.xianzaishi.settlement.client.finance.dto.MergeOrderDTO;
import com.xianzaishi.settlement.client.finance.query.FinancialQuery;

public interface FinanceService {

  /**
   * 创建一笔财务流水记录
   * 
   * @param financialOrderDTO
   * @return
   */
  public Result<Integer> insertFinancialOrder(FinancialOrderDTO financialOrderDTO);

  /**
   * 更新一笔财务流水记录
   * 
   * @param financialOrderDTO
   * @return
   */
  public Result<Boolean> updateFinancialOrder(FinancialOrderDTO financialOrderDTO);

  /**
   * 创建一笔财务抵扣记录
   * 
   * @param mergeOrderDTO
   * @return
   */
  public Result<Integer> insertMergeOrder(MergeOrderDTO mergeOrderDTO);

  /**
   * 更新一个财务抵扣记录
   * 
   * @param mergeOrderDTO
   * @return
   */
  public Result<Boolean> updateMergeOrder(MergeOrderDTO mergeOrderDTO);

  /**
   * 查询符合条件的财务流水记录
   * 
   * @param query
   * @return
   */
  public Result<Map<String, Object>> queryFinancialOrder(FinancialQuery query);

}
