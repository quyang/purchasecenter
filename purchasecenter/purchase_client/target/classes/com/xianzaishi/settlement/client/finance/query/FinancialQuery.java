package com.xianzaishi.settlement.client.finance.query;

import java.io.Serializable;

import com.google.common.base.Preconditions;
import com.xianzaishi.settlement.client.finance.dto.FinancialOrderDTO.FinancialOrderStatusConstants;
import com.xianzaishi.settlement.client.finance.dto.FinancialOrderDTO.FinancialOrderTypeConstants;

/**
 * 采购单查询参数对象
 * 
 * @author zhancang
 */
public class FinancialQuery implements Serializable {

  private static final long serialVersionUID = -4513568945251036607L;

  /**
   * 供应商id
   */
  private Integer orderId;

  /**
   * 供应商id
   */
  private Integer supplierId;

  /**
   * 采购单状态
   */
  private Short status;

  /**
   * 采购单状态
   */
  private Short type;

  /**
   * 默认查询都查20条记录
   */
  private Integer pageSize = 20;

  /**
   * 默认查询从0页开始
   */
  private Integer pageNum = 0;

  public Integer getSupplierId() {
    return supplierId;
  }

  public void setSupplierId(Integer supplierId) {
    Preconditions.checkNotNull(supplierId, "supplierId == null");
    Preconditions.checkArgument(supplierId > 0, "supplierId <= 0");
    this.supplierId = supplierId;
  }

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument(FinancialOrderStatusConstants.isCorrectParameter(status),
        "status not in(0,2,4)");
    this.status = status;
  }

  public Integer getOrderId() {
    return orderId;
  }

  public void setOrderId(Integer orderId) {
    Preconditions.checkNotNull(orderId, "orderId == null");
    Preconditions.checkArgument(orderId > 0, "orderId <= 0");
    this.orderId = orderId;
  }

  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    Preconditions.checkNotNull(status, "status == null");
    Preconditions.checkArgument(FinancialOrderTypeConstants.isCorrectParameter(status),
        "status not in(1,2)");
    this.type = type;
  }

  public Integer getPageSize() {
    return pageSize;
  }

  public void setPageSize(Integer pageSize) {
    Preconditions.checkNotNull(pageSize, "pageSize == null");
    Preconditions.checkArgument(pageSize > 0, "pageSize <= 0");
    this.pageSize = pageSize;
  }

  public Integer getPageNum() {
    return pageNum;
  }

  public void setPageNum(Integer pageNum) {
    Preconditions.checkNotNull(pageNum, "pageNum == null");
    Preconditions.checkArgument(pageNum >= 0, "pageNum < 0");
    this.pageNum = pageNum;
  }

}
