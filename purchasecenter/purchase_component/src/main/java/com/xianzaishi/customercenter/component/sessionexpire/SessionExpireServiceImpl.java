package com.xianzaishi.customercenter.component.sessionexpire;

import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.xianzaishi.customercenter.client.sessionexpireservice.SessionExpireService;
import com.xianzaishi.itemcenter.common.result.Result;

@Service("sessionExpireService")
public class SessionExpireServiceImpl implements SessionExpireService {

  private static final Logger logger = Logger.getLogger(SessionExpireServiceImpl.class);
  
  private static ConcurrentHashMap<String, Long> sessionCache = new ConcurrentHashMap<String, Long>();

  @Override
  public Result<Boolean> sessionExpire(String sessionType) {
    Long lastTime = (Long) sessionCache.get(sessionType);
    if (null == lastTime) {
      lastTime = 0l;
    }
    Boolean isExpire = false;
    Long now = new Date().getTime();
    Long interval = now - lastTime;
    logger.info("lastTime:" + lastTime + ",interval:" + interval);
    if (0 == lastTime || interval <= 60000) {
      sessionCache.put(sessionType, now);
      isExpire = true;
      return Result.getSuccDataResult(isExpire);
    }
    sessionCache.remove(sessionType);
    return Result.getSuccDataResult(isExpire);
  }

}
