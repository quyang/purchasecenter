package com.xianzaishi.purchasecenter.component.activity;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.client.sysproperty.SystemPropertyService;
import com.xianzaishi.itemcenter.client.sysproperty.dto.SystemConfigDTO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.pic.PicConstants;
import com.xianzaishi.itemcenter.common.pic.PicUtil;
import com.xianzaishi.itemcenter.common.query.BaseQuery;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.activity.ActivityPageService;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityPageDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepCatDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepContentDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepDTO.StepTypeConstants;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepItemDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepPicDTO;
import com.xianzaishi.purchasecenter.client.activity.dto.ActivityStepPicDTO.ActivityPicTypeConstants;
import com.xianzaishi.purchasecenter.dal.activity.dao.ActivityPageDOMapper;
import com.xianzaishi.purchasecenter.dal.activity.dataobject.ActivityPageDO;

@Service("activitypageservice")
public class ActivityPageServiceImpl implements ActivityPageService {

  private static final Logger logger = Logger.getLogger(ActivityPageServiceImpl.class);

  @Autowired
  private ActivityPageDOMapper activityPageDOMapper;

  @Autowired
  private SystemPropertyService systemPropertyService;

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");

  /**
   * 插入活动楼层数据
   * 
   * @param activityPageDTO
   * @return
   */
  public Result<Integer> insertPageStepInfo(ActivityPageDTO activityPageDTO) {
    if (null == activityPageDTO || StringUtils.isEmpty(activityPageDTO.getTitle())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input parameter is null");
    }
    Integer maxId = activityPageDOMapper.setMaxPageId();
    if (null == maxId) {
      maxId = 1;
    } else {
      ++maxId;
    }

    ActivityPageDO page = new ActivityPageDO();
    page.setTitle(activityPageDTO.getTitle());
    page.setPageId(maxId);
    Date now = new Date();
    page.setGmtCreate(now);
    page.setGmtModified(now);

    int count = activityPageDOMapper.insert(page);
    if (count != 1) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_INSERT_ERROR,
          "Input page error,page title:" + activityPageDTO.getTitle());
    }
    return Result.getSuccDataResult(page.getStepId());
  }

  @Override
  public Result<Integer> addPageStepInfo(ActivityStepDTO activityStepDTO, int pageId) {
    // Result<Boolean> checkResult = checkStepInfo(activityStepDTO);
    // if (null != checkResult) {
    // return Result.getErrDataResult(checkResult.getResultCode(), checkResult.getErrorMsg());
    // }
    if (pageId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Add step info page num is error");
    }

    updataActivityStepDTOPic(activityStepDTO, false);
    String stepJsonInfo = JackSonUtil.getJson(activityStepDTO);
    if (StringUtils.isEmpty(stepJsonInfo)) {
      Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_INFO_CHANGE_ERROR,
          "Step info change error:" + activityStepDTO.getStepType());
    }

    List<ActivityPageDO> tmpList = activityPageDOMapper.select(pageId, null);
    if (!(CollectionUtils.isNotEmpty(tmpList) && tmpList.size() >= 0)) {
      Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_INFO_UPDATE_ERROR,
          "Step info insert page load error.pageId:" + pageId);
    }
    ActivityPageDO dbPageInfo = tmpList.get(0);
    ActivityPageDO page = new ActivityPageDO();
    Date now = new Date();
    page.setGmtCreate(now);
    page.setGmtModified(now);
    page.setPageId(pageId);
    page.setTitle(dbPageInfo.getTitle());
    page.setStepData(stepJsonInfo);
    page.setStepSort(activityStepDTO.getSortId());
    page.setStepType(activityStepDTO.getStepType().intValue());

    if (dbPageInfo.getStepType() == null && StringUtils.isEmpty(dbPageInfo.getStepData())) {
      page.setStepId(dbPageInfo.getStepId());
      int updateCount = activityPageDOMapper.update(page);
      if (updateCount != 1) {
        return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_INFO_UPDATE_ERROR,
            "Step info update failed:" + activityStepDTO.getStepId());
      } else {
        return Result.getSuccDataResult(dbPageInfo.getStepId());
      }
    }

    int inserCount = activityPageDOMapper.insert(page);
    if (inserCount != 1) {
      Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_INFO_INSERT_ERROR,
          "Step info insert error:" + activityStepDTO.getStepType());
    }

    return Result.getSuccDataResult(page.getStepId());
  }

  @Override
  public Result<Boolean> updateStep(ActivityStepDTO activityStepDTO, int pageId) {
    // Result<Boolean> checkResult = checkStepInfo(activityStepDTO);
    // if (null != checkResult) {
    // return Result.getErrDataResult(checkResult.getResultCode(), checkResult.getErrorMsg());
    // }
    if (activityStepDTO.getStepId() <= 0) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_INFO_UPDATE_ERROR,
          "update step info stepId num is error");
    }

    updataActivityStepDTOPic(activityStepDTO, false);
    String stepJsonInfo = JackSonUtil.getJson(activityStepDTO);
    if (StringUtils.isEmpty(stepJsonInfo)) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_INFO_CHANGE_ERROR,
          "Step info change error:" + stepJsonInfo);
    }
    ActivityPageDO page = new ActivityPageDO();
    List<ActivityPageDO> tmpList =
        activityPageDOMapper.select(pageId, Arrays.asList(activityStepDTO.getStepId()));
    if (CollectionUtils.isNotEmpty(tmpList) && tmpList.size() == 1) {
      page.setStepDataBk(tmpList.get(0).getStepData());
    } else {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_INFO_UPDATE_ERROR,
          "Step info update load error:" + activityStepDTO.getStepId());
    }

    Date now = new Date();
    page.setGmtCreate(now);
    page.setGmtModified(now);
    page.setPageId(pageId);
    page.setTitle(tmpList.get(0).getTitle());
    page.setStepData(stepJsonInfo);
    page.setStepSort(activityStepDTO.getSortId());
    page.setStepType(activityStepDTO.getStepType().intValue());
    page.setStepId(activityStepDTO.getStepId());

    int updateCount = activityPageDOMapper.update(page);
    if (updateCount != 1) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_INFO_UPDATE_ERROR,
          "Step info update failed:" + activityStepDTO.getStepId());
    } else {
      return Result.getSuccDataResult(true);
    }
  }

  @Override
  public Result<Boolean> resetStep(int pageId, int stepId) {
    List<ActivityPageDO> stepList = activityPageDOMapper.select(pageId, Arrays.asList(stepId));
    if (CollectionUtils.isEmpty(stepList)) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
          "query page step is null,pageId:" + pageId + ",stepId" + stepId);
    }

    ActivityPageDO dbActivity = stepList.get(0);
    if (StringUtils.isEmpty(dbActivity.getStepDataBk())) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_HISTORY_NULL_ERROR,
          "reset step error,pageId:" + pageId + ",stepId" + stepId);
    }

    try {
      ActivityStepDTO dbactivityStepDTO =
          (ActivityStepDTO) JackSonUtil.jsonToObject(dbActivity.getStepDataBk(),
              ActivityStepDTO.class);
      dbActivity.setStepData(dbActivity.getStepDataBk());
      dbActivity.setStepType(dbactivityStepDTO.getStepType().intValue());
    } catch (Exception e) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_HISTORY_NULL_ERROR,
          "reset step data change error,pageId:" + pageId + ",stepId" + stepId);
    }

    int updateCount = activityPageDOMapper.update(dbActivity);
    if (updateCount != 1) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_STEP_INFO_UPDATE_ERROR,
          "reset step info update failed:" + pageId + ",stepId" + stepId);
    } else {
      return Result.getSuccDataResult(true);
    }
  }

  /**
   * 检查对象信息
   * 
   * @param activityStepDTO
   * @return
   */
  private Result<Boolean> checkStepInfo(ActivityStepDTO activityStepDTO) {
    if (null == activityStepDTO
        || !StepTypeConstants.isCorrectParameter(activityStepDTO.getStepType())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Add step info parameter is null or page num is error");
    }

    if (StepTypeConstants.PIC_STEP.equals(activityStepDTO.getStepType())) {
      List<ActivityStepPicDTO> listPicList = activityStepDTO.getPicList();
      Result<Boolean> result = checkPic(listPicList);
      if (!result.getSuccess()) {
        return result;
      }
    } else if (StepTypeConstants.PIC_ITEM_STEP.equals(activityStepDTO.getStepType())) {
      List<ActivityStepPicDTO> listPicList = activityStepDTO.getPicList();
      Result<Boolean> result = checkPic(listPicList);
      if (!result.getSuccess()) {
        return result;
      }
      result = checkItem(activityStepDTO.getItemList());
      if (!result.getSuccess()) {
        return result;
      }
    } else if (StepTypeConstants.ITEM_STEP.equals(activityStepDTO.getStepType())) {
      Result<Boolean> result = checkItem(activityStepDTO.getItemList());
      if (!result.getSuccess()) {
        return result;
      }
    } else if (StepTypeConstants.TITLE_ITEM_STEP.equals(activityStepDTO.getStepType())) {
      Result<Boolean> result = checkItem(activityStepDTO.getItemList());
      if (!result.getSuccess()) {
        return result;
      }
      if (StringUtils.isEmpty(activityStepDTO.getTitle())) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
            "Add step info title empty:" + activityStepDTO.getStepType());
      }
    } else if (StepTypeConstants.ONE_PIC_STEP.equals(activityStepDTO.getStepType())) {
      List<ActivityStepPicDTO> listPicList = activityStepDTO.getPicList();
      Result<Boolean> result = checkPic(listPicList);
      if (!result.getSuccess()) {
        return result;
      }
    } else if (StepTypeConstants.PIC_LIST_STEP.equals(activityStepDTO.getStepType())) {
      List<ActivityStepPicDTO> listPicList = activityStepDTO.getPicList();
      Result<Boolean> result = checkPic(listPicList);
      if (!result.getSuccess()) {
        return result;
      }
    } else {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Add step info step type error:" + activityStepDTO.getStepType());
    }
    return null;
  }

  @Override
  public Result<ActivityPageDTO> getPage(int pageId, List<Integer> stepId) {
    if (pageId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query page info page num is error");
    }

    int joinId = 10;
    List<Integer> queryIds = stepId;
    List<Integer> joinIdList = null;
    boolean isJoinStep = null != stepId && stepId.contains(joinId);// 替换楼层id，如果为10，就认为是新的临时需要替换的楼层id
    if (isJoinStep) {
      joinIdList = this.getJoinIdList();
      if (CollectionUtils.isNotEmpty(joinIdList)) {
        queryIds = updateNewStepJoin(stepId, joinId, joinIdList);
      }
    }
    if (CollectionUtils.isEmpty(queryIds)) {
      queryIds = stepId;
    }
    List<ActivityPageDO> tmpList = activityPageDOMapper.select(pageId, queryIds);
    ActivityPageDTO result = new ActivityPageDTO();
    List<ActivityStepDTO> stepList = Lists.newArrayList();
    for (ActivityPageDO page : tmpList) {
      String stepJson = page.getStepData();
      ActivityStepDTO tmp = null;
      try {
        tmp = (ActivityStepDTO) JackSonUtil.jsonToObject(stepJson, ActivityStepDTO.class);
        updataActivityStepDTOPic(tmp, true);
      } catch (Exception e) {
        logger.equals("Translate db step info error,step json:" + stepJson);
      }
      if (null != tmp) {
        updateContentStep(tmp);
        updateCatStep(tmp);
        updateRushItemStep(tmp);
        tmp.setStepId(page.getStepId());
        tmp.setSortId(page.getStepSort());
        stepList.add(tmp);
      }
    }
    if (isJoinStep) {
      stepList = this.joinStepInfo(stepList, joinIdList, joinId);
    }
    if(CollectionUtils.isNotEmpty(stepList) && CollectionUtils.isNotEmpty(stepId)){
      result.setStepList(sortStepList(stepList, stepId));
    }else{
      result.setStepList(stepList);
    }
    result.setPageId(pageId);
    if (tmpList.size() > 0) {
      result.setTitle(tmpList.get(0).getTitle());
      result.setGmtCreate(tmpList.get(0).getGmtCreate());
      result.setGmtModified(tmpList.get(0).getGmtModified());
    }
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<List<ActivityPageDTO>> getPageAll() {
    List<ActivityPageDO> tmpList = activityPageDOMapper.selectall();
    if (CollectionUtils.isEmpty(tmpList)) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
          "get activity page is empty");
    }

    Map<Integer, List<ActivityPageDO>> tmpMap = Maps.newHashMap();
    List<Integer> pageIdList = Lists.newArrayList();
    for (ActivityPageDO pageInfo : tmpList) {
      List<ActivityPageDO> pageTmpList = tmpMap.get(pageInfo.getPageId());
      if (CollectionUtils.isEmpty(pageTmpList)) {
        pageTmpList = Lists.newArrayList();
      }
      pageTmpList.add(pageInfo);
      tmpMap.put(pageInfo.getPageId(), pageTmpList);
      if (!pageIdList.contains(pageInfo.getPageId())) {
        pageIdList.add(pageInfo.getPageId());
      }
    }

    List<ActivityPageDTO> resultList = Lists.newArrayList();
    for (int pageId : pageIdList) {
      ActivityPageDTO result = new ActivityPageDTO();
      List<ActivityPageDO> dbList = tmpMap.get(pageId);
      if (CollectionUtils.isEmpty(dbList)) {
        continue;
      }

      List<ActivityStepDTO> stepList = Lists.newArrayList();
      for (ActivityPageDO page : dbList) {
        String stepJson = page.getStepData();
        ActivityStepDTO tmp = null;
        try {
          tmp = (ActivityStepDTO) JackSonUtil.jsonToObject(stepJson, ActivityStepDTO.class);
          updataActivityStepDTOPic(tmp, true);
        } catch (Exception e) {
          logger.equals("Translate db step info error,step json:" + stepJson);
        }
        if (null != tmp) {
          tmp.setStepId(page.getStepId());
          tmp.setSortId(page.getStepSort());
          stepList.add(tmp);
        }
      }
      result.setStepList(stepList);
      result.setPageId(pageId);
      if (dbList.size() > 0) {
        result.setTitle(dbList.get(0).getTitle());
        result.setGmtCreate(dbList.get(0).getGmtCreate());
        result.setGmtModified(dbList.get(0).getGmtModified());
      }
      resultList.add(result);
    }

    return Result.getSuccDataResult(resultList);
  }

  private Result<Boolean> checkPic(List<ActivityStepPicDTO> listPicList) {
    if (CollectionUtils.isEmpty(listPicList)) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_PIC_STEP_PIC_EMPTY_ERROR,
          "Add pic step,piclist is empty");
    }
    for (ActivityStepPicDTO pic : listPicList) {
      if (!ActivityPicTypeConstants.isCorrectParameter(pic.getTargetType())) {
        return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_PIC_STEP_PIC_TYPE_ERROR,
            "Add pic step,pic type is wrong");
      }
      if (StringUtils.isEmpty(pic.getPicUrl())) {
        return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_PIC_STEP_PIC_EMPTY_ERROR,
            "Add pic step,pic is null");
      }
      if (ActivityPicTypeConstants.PIC_ITEM.equals(pic.getTargetType())
          || ActivityPicTypeConstants.PIC_SUBACTIVITYPAGE.equals(pic.getTargetType())) {
        if (StringUtils.isEmpty(pic.getTargetId())) {
          return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_PIC_STEP_PIC_TYPE_ERROR,
              "Add pic step,pic type and target is wrong");
        }
      }
    }
    return Result.getSuccDataResult(true);
  }

  private Result<Boolean> checkItem(List<ActivityStepItemDTO> itemList) {
    if (CollectionUtils.isEmpty(itemList)) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_ITEM_STEP_ITEM_NULL_ERROR,
          "Add item step,itemList is empty");
    }
    for (ActivityStepItemDTO item : itemList) {
      long itemId = item.getItemId();
      if (itemId <= 0) {
        return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_ITEM_STEP_ITEM_ID_ERROR,
            "Add item step,item id is error");
      }
    }

    return Result.getSuccDataResult(true);
  }

  /**
   * 更新图片数据
   * 
   * @param activityStepDTO
   * @param isAdd 是增加前缀还是删除前缀
   */
  private void updataActivityStepDTOPic(ActivityStepDTO activityStepDTO, boolean isAdd) {
    List<ActivityStepPicDTO> stepPicList = activityStepDTO.getPicList();
    if (CollectionUtils.isNotEmpty(stepPicList)) {
      for (ActivityStepPicDTO pic : stepPicList) {
        pic.setPicUrl(PicUtil.processPicHeader(pic.getPicUrl(), isAdd));
      }
    }

    List<ActivityStepItemDTO> stepItemList = activityStepDTO.getItemList();
    if (CollectionUtils.isNotEmpty(stepItemList)) {
      for (ActivityStepItemDTO item : stepItemList) {
        item.setPicUrl(PicUtil.processPicHeader(item.getPicUrl(), isAdd));
      }
    }
  }

  /**
   * 根据分页查询数据
   */
  @Override
  public Result<List<ActivityPageDTO>> getPageByPaging(BaseQuery query) {
    List<ActivityPageDO> activityPageDOs = activityPageDOMapper.selectByPaging(query);
    if (CollectionUtils.isEmpty(activityPageDOs)) {
      return Result.getErrDataResult(ServerResultCode.AVTIVITY_PAGE_NOT_EXIT_ERROR,
          "get activity page is empty");
    }
    Map<Integer, List<ActivityPageDO>> tmpMap = Maps.newHashMap();
    List<Integer> pageIdList = Lists.newArrayList();
    for (ActivityPageDO pageInfo : activityPageDOs) {
      List<ActivityPageDO> pageTmpList = tmpMap.get(pageInfo.getPageId());
      if (CollectionUtils.isEmpty(pageTmpList)) {
        pageTmpList = Lists.newArrayList();
      }
      pageTmpList.add(pageInfo);
      tmpMap.put(pageInfo.getPageId(), pageTmpList);
      if (!pageIdList.contains(pageInfo.getPageId())) {
        pageIdList.add(pageInfo.getPageId());
      }
    }

    List<ActivityPageDTO> resultList = Lists.newArrayList();
    for (int pageId : pageIdList) {
      ActivityPageDTO result = new ActivityPageDTO();
      List<ActivityPageDO> dbList = tmpMap.get(pageId);
      if (CollectionUtils.isEmpty(dbList)) {
        continue;
      }

      List<ActivityStepDTO> stepList = Lists.newArrayList();
      for (ActivityPageDO page : dbList) {
        String stepJson = page.getStepData();
        ActivityStepDTO tmp = null;
        try {
          tmp = (ActivityStepDTO) JackSonUtil.jsonToObject(stepJson, ActivityStepDTO.class);
          updataActivityStepDTOPic(tmp, true);
        } catch (Exception e) {
          logger.equals("Translate db step info error,step json:" + stepJson);
        }
        if (null != tmp) {
          tmp.setStepId(page.getStepId());
          tmp.setSortId(page.getStepSort());
          stepList.add(tmp);
        }
      }
      result.setStepList(stepList);
      result.setPageId(pageId);
      if (dbList.size() > 0) {
        result.setTitle(dbList.get(0).getTitle());
        result.setGmtCreate(dbList.get(0).getGmtCreate());
        result.setGmtModified(dbList.get(0).getGmtModified());
      }
      resultList.add(result);
    }

    return Result.getSuccDataResult(resultList);
  }

  /**
   * 给返回结果排序
   * 
   * @param stepList
   * @param stepId
   * @return
   */
  private List<ActivityStepDTO> sortStepList(List<ActivityStepDTO> stepList, List<Integer> stepId) {
    List<ActivityStepDTO> result = Lists.newArrayList();
    Map<Integer, ActivityStepDTO> cmpM = Maps.newHashMap();
    if (CollectionUtils.isEmpty(stepList)) {
      return stepList;
    }
    for (ActivityStepDTO step : stepList) {
      cmpM.put(step.getStepId(), step);
    }
    for (Integer id : stepId) {
      ActivityStepDTO step = cmpM.get(id);
      if (null != step) {
        result.add(step);
      }
    }
    if (CollectionUtils.isEmpty(result)) {
      return stepList;
    } else {
      return result;
    }
  }

  /**
   * 更新楼层信息，针对文案楼层，独立解析出来
   * 
   * @param tmp
   */
  private void updateContentStep(ActivityStepDTO tmp) {
    if (null != tmp && tmp.getStepType().equals(22)) {// 特殊的楼层类型type,特殊代表文案楼层
      Result<SystemConfigDTO> result = systemPropertyService.getSystemConfig(11);//后台写死配置项id
      if (null != result && result.getSuccess()) {
        String jsonInfo = result.getModule().getConfigInfo();
        List<ActivityStepContentDTO> contentList =
            JackSonUtil.jsonToList(jsonInfo, ActivityStepContentDTO.class);
        if (null != contentList && CollectionUtils.isNotEmpty(contentList)) {
          tmp.setContentList(contentList);
        }
      }
    }
  }

  /**
   * 更新楼层信息，针对类目楼层，独立解析出来
   * 
   * @param tmp
   */
  private void updateCatStep(ActivityStepDTO tmp) {
    if (null != tmp && tmp.getStepType().equals(17)) {// 特殊的楼层类型id,特殊代表类目楼层
      List<ActivityStepCatDTO> catList = Lists.newArrayList();
      String title = tmp.getTitle();
      String catStrArray[] = title.split(";");
      for (String catStr : catStrArray) {
        String cat[] = catStr.split("#");
        ActivityStepCatDTO catDTO = new ActivityStepCatDTO();
        catDTO.setCatName(cat[0]);
        catDTO.setCatId(Long.valueOf(cat[1]));
        catDTO.setPicUrl(PicConstants.PIC_PREFIX+cat[2]);
        if(cat.length >= 4){
          if(StringUtils.isNotEmpty(cat[3])){
            catDTO.setJumpLink(cat[3]);
          }
        }
        if(cat.length >= 5){
          catDTO.setJumpPage(cat[4]);
        }
        catList.add(catDTO);
      }
      tmp.setCatList(catList);
    }
  }

  /**
   * 秒杀抢购楼层特殊设置，解析时间字段，格式为 2015-12-12-12-1-13=2015-12-14-12-1-13
   * 
   * @param tmp
   */
  private void updateRushItemStep(ActivityStepDTO tmp) {
    if (null != tmp && tmp.getStepType().equals(19)) {// 特殊的楼层类型id,特殊代表秒杀抢购楼层，需要解析时间字段
      String title = tmp.getTitle();
      String time[] = title.split("=");
      List<Date> timeList = Lists.newArrayList();
      try {
        for (String timeStr : time) {
          Date timeInfo = sdf.parse(timeStr);
          timeList.add(timeInfo);
        }
      } catch (Exception e) {
      }
      if (timeList.size() < 2) {
        Date now = new Date();
        Date begin = DateUtils.addDays(now, 1);
        Date end = DateUtils.addDays(now, 2);
        timeList.set(0, begin);
        timeList.set(1, end);
      }
      tmp.setBegin(timeList.get(0).getTime());
      tmp.setEnd(timeList.get(1).getTime());
    }
  }

  /**
   * 获取要替换的id值
   * 
   * @param keyId
   * @return
   */
  private List<Integer> getJoinIdList() {
    List<Integer> idList = Lists.newArrayList();
    Result<SystemConfigDTO> result = systemPropertyService.getSystemConfig(10);
    if (null != result && result.getSuccess() && result.getModule() != null
        && StringUtils.isNotEmpty(result.getModule().getConfigInfo())) {
      String idArray[] = result.getModule().getConfigInfo().split(",");
      for (String idStr : idArray) {
        idList.add(Integer.valueOf(idStr));
      }
    }
    return idList;
  }

  /**
   * 针对输入楼层，判断是否有特殊处理的楼层，有特殊处理的楼层则返回新的楼层列表，否则返回空
   * 
   * @param stepIdList
   * @return
   */
  private List<Integer> updateNewStepJoin(List<Integer> stepIdList, Integer replaceId,
      List<Integer> joinIdList) {
    if (CollectionUtils.isEmpty(stepIdList)) {
      return stepIdList;
    }
    List<Integer> idList = Lists.newArrayList();
    for (Integer id : stepIdList) {
      if (id.equals(replaceId)) {// 特殊标记，代表限时特惠楼层id
        idList.addAll(joinIdList);
      } else {
        idList.add(id);
      }
    }
    return idList;
  }

  /**
   * 特殊调用情况下，合并楼层，只针对限时特购着一种场景
   * 
   * @param stepList
   */
  private List<ActivityStepDTO> joinStepInfo(List<ActivityStepDTO> stepList, List<Integer> idList,
      Integer oldId) {
    List<ActivityStepDTO> result = Lists.newArrayList();
    List<ActivityStepDTO> joinList = Lists.newArrayList();
    for (ActivityStepDTO activity : stepList) {
      if (idList.contains(activity.getStepId())) {
        joinList.add(activity);
      } else {
        result.add(activity);
      }
    }
    if (CollectionUtils.isNotEmpty(joinList)) {
      ActivityStepDTO step = new ActivityStepDTO();
      step.setStepId(oldId);
      step.setTitle("");
      step.setInnerStepList(joinList);
      for (ActivityStepDTO inner : joinList) {
        if (CollectionUtils.isNotEmpty(inner.getPicList())) {
          step.setPicList(inner.getPicList());
          break;
        }
      }
      step.setStepType(18);// 设置独立的楼层格式
      result.add(step);
    }
    return result;
  }

  /**
   * 获取所有数据数量
   */
  @Override
  public Result<Integer> getPageCount() {
    Integer count = activityPageDOMapper.selectCount();

    return Result.getSuccDataResult(count);
  }

  @Override
  public Result<Boolean> deletePageStepInfo(int stepId) {
    int deleteCount = activityPageDOMapper.deleteByStepId(stepId);
    Boolean deleteSuccess = deleteCount == 1 ? true : false;
    return Result.getSuccDataResult(deleteSuccess);
  }

}
