package com.xianzaishi.purchasecenter.component.logmonitorconfig;

import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.logmonitorconfig.LogMonitorConfigService;
import com.xianzaishi.purchasecenter.client.logmonitorconfig.dto.LogMonitorConfigDTO;
import com.xianzaishi.purchasecenter.dal.logmonitorconfig.dao.LogMonitorConfigDOMapper;
import com.xianzaishi.purchasecenter.dal.logmonitorconfig.dataobject.LogMonitorConfigDO;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * quyang 2017年3月28日 14:45:55
 */
@Service("logMonitorConfigService")
public class LogMonitorConfigServiceImpl implements LogMonitorConfigService {

  @Autowired
  private LogMonitorConfigDOMapper logMonitorConfigDOMapper;

  private static final Logger logger = Logger.getLogger(LogMonitorConfigServiceImpl.class);


  @Override
  public Result<Boolean> insert(LogMonitorConfigDTO logMonitorConfigDTO) {
    if (null == logMonitorConfigDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "插入数据异常");
    }

    LogMonitorConfigDO logMonitorConfigDO = new LogMonitorConfigDO();
    BeanCopierUtils.copyProperties(logMonitorConfigDTO, logMonitorConfigDO);

//    logger.error("拷贝后的数据=" + JackSonUtil.getJson(logMonitorConfigDO));

    Integer insert = logMonitorConfigDOMapper.insert(logMonitorConfigDO);

//    logger.error("插入结果=" + JackSonUtil.getJson(insert));

    if (null != insert && insert == 1) {
      return Result.getSuccDataResult(true);
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "插入数据失败");
  }

  @Override
  public Result<Boolean> update(LogMonitorConfigDTO logMonitorConfigDTO) {
    if (null == logMonitorConfigDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新参数异常");
    }

    LogMonitorConfigDO logMonitorConfigDO = new LogMonitorConfigDO();
    BeanCopierUtils.copyProperties(logMonitorConfigDTO, logMonitorConfigDO);
//    logger.error("拷贝后=" + JackSonUtil.getJson(logMonitorConfigDO));
    Integer update = logMonitorConfigDOMapper.update(logMonitorConfigDO);
//    logger.error("更新结果=" + JackSonUtil.getJson(update));
    if (null != update && update == 1) {
      return Result.getSuccDataResult(true);
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新数据失败");
  }

  @Override
  public Result<List<LogMonitorConfigDTO>> select(LogMonitorConfigDTO logMonitorConfigDTO) {
    if (null == logMonitorConfigDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询参数对象异常");
    }

    LogMonitorConfigDO logMonitorConfigDO = new LogMonitorConfigDO();
    BeanCopierUtils.copyProperties(logMonitorConfigDTO, logMonitorConfigDO);
//    logger.error("拷贝后=" + JackSonUtil.getJson(logMonitorConfigDO));
    List<LogMonitorConfigDO> dtoList = logMonitorConfigDOMapper.select(logMonitorConfigDO);
//    logger.error("更新结果=" + JackSonUtil.getJson(dtoList));

    List<LogMonitorConfigDTO> list = new ArrayList<>();
    if (CollectionUtils.isNotEmpty(dtoList)) {
      for (LogMonitorConfigDO configDO : dtoList) {
        LogMonitorConfigDTO monitorConfigDTO = new LogMonitorConfigDTO();
        BeanCopierUtils.copyProperties(configDO, monitorConfigDTO);
        list.add(monitorConfigDTO);
      }
    }

//    logger.error("结果=" + JackSonUtil.getJson(list));

    if (CollectionUtils.isEmpty(list)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询数据为空");
    }

    return Result.getSuccDataResult(list);
  }
}
