package com.xianzaishi.purchasecenter.component.organization;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.organization.OrganizationService;
import com.xianzaishi.purchasecenter.client.organization.dto.OrganizationDTO;
import com.xianzaishi.purchasecenter.dal.organization.dao.OrganizationDOMapper;
import com.xianzaishi.purchasecenter.dal.organization.dataobject.OrganizationDO;
import com.xianzaishi.purchasecenter.dal.user.dao.EmployeeDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.UserDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dataobject.EmployeeDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.UserDO;

@Service("organizationService")
public class OrganizationServiceImpl implements OrganizationService {

  @Autowired
  private OrganizationDOMapper organizationDOMapper;

  @Autowired
  private UserDOMapper userDOMapper;
  
  @Autowired
  private EmployeeDOMapper employeeDOMapper;

//  private OrganizationDTO get() {
//    OrganizationDTO organization = new OrganizationDTO();
//    Date now = new Date();
//    organization.setAddress("上海市浦东新区区东方路1367号富都广场");
//    organization.setArea("区域属性待定");
//    organization.setBegin(now);
//    organization.setEnd(now);
//    organization.setFeature("属性扩展值待定");
//    organization.setGmtCreate(now);
//    organization.setGmtModified(now);
//    organization.setName("鲜在时1号店");
//    organization.setOrganizationId(1);
//    organization.setOwnerId(23746238);
//    organization.setPicUrl("i1/TB14iJlKXXXXXaZXVXX2kLU.VXX-346-200.png_210x1000q90.jpg");
//    organization.setPoiX("121.5286");
//    organization.setPoiY("31.211829");
//    organization.setStatus(OrganizationStatusConstants.ORGANIZATION_STATUS_NORMAL);
//    return organization;
//  }

  @Override
  public Result<OrganizationDTO> queryOrganization(Integer organizationId) {
    if (null == organizationId || organizationId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input orgId is error number,input id:" + organizationId);
    }
    OrganizationDO doResult = organizationDOMapper.select(organizationId);
    if (null == doResult) {
      return Result.getErrDataResult(ServerResultCode.ORGANIZATION_ERROR_CODE_NOT_EXIT,
          "input orgId query Organization not exit,input id:" + organizationId);
    }
    OrganizationDTO result = new OrganizationDTO();
    BeanCopierUtils.copyProperties(doResult, result);
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<OrganizationDTO> queryOrganizationByEmployId(int userId) {
    EmployeeDO employeeDO = employeeDOMapper.selectById(userId);
    if(null == employeeDO){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input token query user is null,id:" + userId);
    }
    
    return queryOrganization(employeeDO.getOrganizationId());
  }

  @Override
  public Result<OrganizationDTO> queryOrganizationByEmployToken(String token) {
    
    if (StringUtils.isEmpty(token)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input token is null");
    }
    
    UserDO userDO = userDOMapper.selectByToken(token);
    if(null == userDO){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "input token query user is null,token:" + token);
    }
    
    return queryOrganizationByEmployId(userDO.getUserId());
  }

  @Override
  public Result<OrganizationDTO> queryDefaultOrganization() {
    return queryOrganization(1);
  }

}
