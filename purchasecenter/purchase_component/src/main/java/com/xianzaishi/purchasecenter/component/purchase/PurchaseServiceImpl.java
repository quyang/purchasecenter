package com.xianzaishi.purchasecenter.component.purchase;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO.PurchaseDeliveryStatusConstants;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseQualityDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.StorageOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.query.PurchaseQuery;
import com.xianzaishi.purchasecenter.client.user.query.EmployeeQuery;
import com.xianzaishi.purchasecenter.dal.purchase.dao.PurchaseOrderDOMapper;
import com.xianzaishi.purchasecenter.dal.purchase.dao.PurchaseQualityDOMapper;
import com.xianzaishi.purchasecenter.dal.purchase.dao.PurchaseSubOrderDOMapper;
import com.xianzaishi.purchasecenter.dal.purchase.dao.StorageOrderDOMapper;
import com.xianzaishi.purchasecenter.dal.purchase.dataobject.PurchaseOrderDO;
import com.xianzaishi.purchasecenter.dal.purchase.dataobject.PurchaseQualityDO;
import com.xianzaishi.purchasecenter.dal.purchase.dataobject.PurchaseSubOrderDO;
import com.xianzaishi.purchasecenter.dal.purchase.dataobject.StorageOrderDO;
import com.xianzaishi.purchasecenter.dal.user.dao.EmployeeDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.supplier.SupplierDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dataobject.EmployeeDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierDO;

@Service("purchaseService")
public class PurchaseServiceImpl implements PurchaseService {
  @Autowired
  private PurchaseSubOrderDOMapper myPurchaseSubOrderDOMapper;

  @Autowired
  private PurchaseOrderDOMapper myPurchaseOrderDOMapper;

  @Autowired
  private StorageOrderDOMapper myStorageOrderDOMapper;

  @Autowired
  private PurchaseQualityDOMapper myPurchaseQualityDOMapper;
  @Autowired
  private EmployeeDOMapper  employeeDompper;
  
  @Autowired
  private SupplierDOMapper supplierDompper;

  private static final Logger LOGGER = Logger.getLogger(PurchaseServiceImpl.class);

  @Override
  public Result<Integer> insertPurchase(PurchaseOrderDTO purchaseOrderDTO) {
    if (null == purchaseOrderDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "purchaseOrderDTO is null");
    }
    PurchaseOrderDO purchaseOrderDO = new PurchaseOrderDO();
    BeanCopierUtils.copyProperties(purchaseOrderDTO, purchaseOrderDO);
//    purchaseOrderDO = (PurchaseOrderDO) purchaseOrderDTO;
    List<PurchaseSubOrderDTO> subOrderDTOs = purchaseOrderDTO.getSubOrderList();

    Integer insertNum = myPurchaseOrderDOMapper.insert(purchaseOrderDO);
    if (insertNum <= 0) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "db error");
    }
    Integer purchaseOrderId = purchaseOrderDO.getPurchaseId();
    List<PurchaseSubOrderDO> subOrderDOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(subOrderDTOs, subOrderDOs, PurchaseSubOrderDO.class);
//      for (PurchaseSubOrderDTO purchaseSubOrderDTO : subOrderDTOs) {
//        PurchaseSubOrderDO subOrderDO = new PurchaseSubOrderDO();
//        subOrderDO.setCheckedFailedReason(purchaseSubOrderDTO.getCheckedFailedReason());
//        subOrderDO.setCheckinfo(purchaseSubOrderDTO.getCheckinfo());
//        subOrderDO.setContractId(purchaseSubOrderDTO.getContractId());
//        subOrderDO.setCount(purchaseSubOrderDTO.getCount());
//        subOrderDO.setCreator(purchaseSubOrderDTO.getCreator());
//        subOrderDO.setExpectArriveDate(purchaseSubOrderDTO.getExpectArriveDate());
//        subOrderDO.setFlowStatus(purchaseSubOrderDTO.getFlowStatus());
//        subOrderDO.setPurchaseDate(purchaseSubOrderDTO.getPurchaseDate());
//        subOrderDO.setPurchaseId(purchaseOrderId);
//        subOrderDO.setPurchasingAgent(purchaseSubOrderDTO.getPurchasingAgent());
//        subOrderDO.setSettlementPrice(purchaseSubOrderDTO.getSettlementPrice());
//        subOrderDO.setSkuId(purchaseSubOrderDTO.getSkuId());
//        subOrderDO.setStatus(purchaseSubOrderDTO.getStatus());
//        subOrderDO.setStorageId(purchaseSubOrderDTO.getStorageId());
//        subOrderDO.setSupplierId(purchaseSubOrderDTO.getSupplierId());
//        subOrderDO.setTitle(purchaseSubOrderDTO.getTitle());
//        subOrderDO.setType(purchaseSubOrderDTO.getType());
//        subOrderDO.setUnitCost(purchaseSubOrderDTO.getUnitCost());
//        subOrderDO.setWareHouse(purchaseSubOrderDTO.getWareHouse());
//        subOrderDO = (PurchaseSubOrderDO) purchaseSubOrderDTO;
//        subOrderDOs.add(subOrderDO);
//      }
    for (PurchaseSubOrderDO subOrderDO : subOrderDOs) {
      subOrderDO.setPurchaseId(purchaseOrderId);
    }
    LOGGER.info("bean值,dto:" + JackSonUtil.getJson(subOrderDTOs) + ",do"
        + JackSonUtil.getJson(subOrderDOs));

    Integer insertSubOrderNum = myPurchaseSubOrderDOMapper.insert(subOrderDOs);
    if (insertSubOrderNum <= 0) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "db error");
    }
    return Result.getSuccDataResult(purchaseOrderId);
  }

  @Override
  public Result<Boolean> updatePurchaseSubOrder(PurchaseSubOrderDTO purchaseSubOrderDTO) {
    if (null == purchaseSubOrderDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "purchaseSubOrderDTO is null");
    }
    PurchaseSubOrderDO purchaseSubOrderDO = new PurchaseSubOrderDO();
    BeanCopierUtils.copyProperties(purchaseSubOrderDTO, purchaseSubOrderDO);
    Integer updateNum = myPurchaseSubOrderDOMapper.updateByPrimaryKey(purchaseSubOrderDO);
    Boolean isUpdate = updateNum > 0 ? true : false;
    return Result.getSuccDataResult(isUpdate);
  }

  @Override
  public Result<Integer> insertStorageOrder(StorageOrderDTO storageOrderDTO) {
    if (null == storageOrderDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "storageOrderDTO is null");
    }
    StorageOrderDO storageOrderDO = new StorageOrderDO();
    BeanCopierUtils.copyProperties(storageOrderDTO, storageOrderDO);
    Integer insertNum = myStorageOrderDOMapper.insert(storageOrderDO);
    if (insertNum <= 0) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "db error");
    }

    return Result.getSuccDataResult(storageOrderDO.getStorageId());
  }

  @Override
  public Result<Boolean> updateStorageOrder(StorageOrderDTO storageOrderDTO) {
    if (null == storageOrderDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "storageOrderDTO is null");
    }

    StorageOrderDO storageOrderDO = new StorageOrderDO();
    BeanCopierUtils.copyProperties(storageOrderDTO, storageOrderDO);
    Integer updateNum = myStorageOrderDOMapper.updateByPrimaryKey(storageOrderDO);
    Boolean isUpdate = updateNum > 0 ? true : false;
    return Result.getSuccDataResult(isUpdate);
  }

  @Override
  public Result<Integer> insertPurchaseQuality(PurchaseQualityDTO purchaseQualityDTO) {
    if (null == purchaseQualityDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "purchaseQualityDTO is null");
    }
    PurchaseQualityDO purchaseQualityDO = new PurchaseQualityDO();
    BeanCopierUtils.copyProperties(purchaseQualityDTO, purchaseQualityDO);
    Integer insertNum = myPurchaseQualityDOMapper.insert(purchaseQualityDO);
    if (insertNum <= 0) {
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, "db error");
    }

    return Result.getSuccDataResult(purchaseQualityDO.getQualityId());
  }

  @Override
  public Result<Boolean> updatePurchaseQuality(PurchaseQualityDTO purchaseQualityDTO) {
    if (null == purchaseQualityDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "purchaseQualityDTO is null");
    }
    PurchaseQualityDO purchaseQualityDO = new PurchaseQualityDO();
    BeanCopierUtils.copyProperties(purchaseQualityDTO, purchaseQualityDO);
    Integer updateNum = myPurchaseQualityDOMapper.updateByPrimaryKey(purchaseQualityDO);
    Boolean isUpdate = updateNum > 0 ? true : false;

    return Result.getSuccDataResult(isUpdate);
  }

  @Override
  public Result<List<PurchaseSubOrderDTO>> querySubPurchaseListByPurId(int purchaseId) {
    if(purchaseId==0){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "query is null");
    }
    System.out.println("querySubPurchaseList111");
    
    PurchaseQuery purchaseQuery=new PurchaseQuery();
    purchaseQuery.setParentPurchaseId(purchaseId);
    List<PurchaseSubOrderDO> purchaseSubOrderDOs = myPurchaseSubOrderDOMapper.selectByPurchaseQuery(purchaseQuery);
    System.out.println("querySubPurchaseList222");
    if(null == purchaseSubOrderDOs || purchaseSubOrderDOs.size() == 0){
      return Result.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT, "purchase order is not exit with this query");
    }
    List<PurchaseSubOrderDTO> purchaseSubOrderDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(purchaseSubOrderDOs, purchaseSubOrderDTOs, PurchaseSubOrderDTO.class);

    //获取对应创建人信息
    List<Integer> cresteIds=Lists.newArrayList();
    //供应商ID
    List<Integer>   supplierIDs= Lists.newArrayList();
    for (int i = 0; i < purchaseSubOrderDTOs.size(); i++) {
    	cresteIds.add(purchaseSubOrderDTOs.get(i).getCreator());
    	supplierIDs.add(purchaseSubOrderDOs.get(i).getSupplierId());
	}
    //创建人信息
    System.out.println("querySubPurchaseList3");
     List<EmployeeDO> selectByAllId = employeeDompper.selectByAllId(cresteIds);
     if (selectByAllId!=null) {
    	 for (PurchaseSubOrderDTO pItem : purchaseSubOrderDTOs) {
    		 for (int i = 0; i < selectByAllId.size(); i++) {
    			 //查询列表的创建人ID和创建人集合对应
    			 if (pItem.getCreator()==selectByAllId.get(i).getUserId()) {
    				 pItem.setCreateName(selectByAllId.get(i).getName());
    				 break;
				 }
    		 }
    	 }
	}
    //获取供应商信息
    List<SupplierDO> selectIdList = supplierDompper.selectIdList(supplierIDs);
    if (selectIdList!=null) {
    	 for (PurchaseSubOrderDTO pItem : purchaseSubOrderDTOs) {
    		 for (int i = 0; i < selectIdList.size(); i++) {
    			 System.out.println("selectIdList"+selectIdList.get(i).getName());
    			 //查询列表的供应商ID和供应商集合对应
    			 if (pItem.getSupplierId()==selectIdList.get(i).getUserId()) {
    				 pItem.setSupplierName(selectIdList.get(i).getCompanyName());
    				 break;
				 }
    		 }
    	 }
	}
    return Result.getSuccDataResult(purchaseSubOrderDTOs);
  }
  
  
  
  
  @Override
  public Result<Map<String, Object>> querySubPurchaseList(PurchaseQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "query is null");
    }

    List<PurchaseSubOrderDO> purchaseSubOrderDOs =
        myPurchaseSubOrderDOMapper.selectByPurchaseQuery(query);
    if (null == purchaseSubOrderDOs || purchaseSubOrderDOs.size() == 0) {
      return Result.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT,
          "purchase order is not exit with this query");
    }
    List<PurchaseSubOrderDTO> purchaseSubOrderDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(purchaseSubOrderDOs, purchaseSubOrderDTOs,
        PurchaseSubOrderDTO.class);
    Integer count = purchaseSubOrderDOs.size();
    Map<String, Object> map = Maps.newHashMap();
    map.put(SUB_PURCHASE_LIST_KEY, purchaseSubOrderDTOs);
    map.put(SUB_PURCHASE_COUNT_KEY, count);
    return Result.getSuccDataResult(map);
  }


  @Override
  public Result<Integer> querySubPurchaseCount(PurchaseQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "query is null");
    }
    Integer count = myPurchaseSubOrderDOMapper.selectByPurchaseCount(query);
    return Result.getSuccDataResult(count);
  }

  @Override
  public Result<PurchaseSubOrderDTO> querySubPurchase(Integer purchaseSubOrderId) {
    if (purchaseSubOrderId == null) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "purchaseSubOrderId is null");
    }
    PurchaseSubOrderDO purchaseSubOrderDO =
        myPurchaseSubOrderDOMapper.selectByPrimaryKey(purchaseSubOrderId);
    if (null == purchaseSubOrderDO || null == purchaseSubOrderDO.getPurchaseSubId()
        || purchaseSubOrderDO.getPurchaseSubId() == 0) {
      return Result.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT,
          "purchase order is not exit with purhcaseSubOrderId :" + purchaseSubOrderId);
    }
    PurchaseSubOrderDTO purchaseSubOrderDTO = new PurchaseSubOrderDTO();
    BeanCopierUtils.copyProperties(purchaseSubOrderDO, purchaseSubOrderDTO);
    return Result.getSuccDataResult(purchaseSubOrderDTO);
  }

  @Override
  public Result<List<StorageOrderDTO>> queryStorageOrderByPurchaseSubOrder(
      Integer purchaseSubOrderId) {
    if (null == purchaseSubOrderId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "purchaseSubOrderId is null");
    }
    List<StorageOrderDO> storageOrderDOs =
        myStorageOrderDOMapper.selectBySubOrderId(purchaseSubOrderId);
    if (null == storageOrderDOs) {
      return Result.getErrDataResult(ServerResultCode.STORAGE_ORDER_NOT_EXIT,
          "storageOrder not exit");
    }
    List<StorageOrderDTO> storageOrderDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(storageOrderDOs, storageOrderDTOs, StorageOrderDTO.class);

    return Result.getSuccDataResult(storageOrderDTOs);
  }

  @Override
  public Result<List<PurchaseQualityDTO>> queryQualityByPurchaseSubOrder(Integer purchaseSubOrderId) {
    if (null == purchaseSubOrderId) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "purchaseSubOrderId is null");
    }
    List<PurchaseQualityDO> purchaseQualityDOs =
        myPurchaseQualityDOMapper.selectByPurchaseId(purchaseSubOrderId);
    if (null == purchaseQualityDOs) {
      return Result.getErrDataResult(ServerResultCode.PURCHASE_QUQLITY_NOT_EXIT,
          "purchase quality is not exit");
    }
    List<PurchaseQualityDTO> purchaseQualityDTOs = Lists.newArrayList();
    
    BeanCopierUtils.copyListBean(purchaseQualityDOs, purchaseQualityDTOs, PurchaseQualityDTO.class);

    return Result.getSuccDataResult(purchaseQualityDTOs);
  }

  @Override
  public PagedResult<List<PurchaseOrderDTO>> queryPurchase(PurchaseQuery query) {
    if (null == query) {
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query is null");
    }

    List<PurchaseOrderDO> purchaseOrderDOs = myPurchaseOrderDOMapper.selectByPurchaseQuery(query);
    if (CollectionUtils.isEmpty(purchaseOrderDOs)) {
      return PagedResult.getErrDataResult(ServerResultCode.PURCHASE_SUB_ORDER_NOT_EXIT,
          "purchase order is not exit with this query");
    }
    List<PurchaseOrderDTO> purchaseOrderDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(purchaseOrderDOs, purchaseOrderDTOs, PurchaseOrderDTO.class);
    Integer totalNum = myPurchaseOrderDOMapper.count(query);
    return PagedResult.getSuccDataResult(purchaseOrderDTOs, totalNum,
        getTotalPage(totalNum, query.getPageSize()), query.getPageSize(), query.getPageNum());
  }

  /**
   * 获取分页数量
   * 
   * @param number 数据总数量
   * @param pageSize 分页大小
   * @return
   */
  private Integer getTotalPage(Integer number, Integer pageSize) {
    Integer totalPage = 1;
    if (null == number) {
      number = 0;
    }
    if (null == pageSize) {
      return totalPage;
    }
    if (number % pageSize != 0) {
      totalPage = (number / pageSize) + 1;
    } else {
      totalPage = number / pageSize;
    }
    return totalPage;
  }

  @Override
  public Result<Boolean> updatePurchaseOrder(PurchaseOrderDTO purchaseOrderDTO) {
    if (null == purchaseOrderDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "purchaseSubOrderDTO is null");
    }
    PurchaseOrderDO purchaseOrderDO = new PurchaseOrderDO();
    BeanCopierUtils.copyProperties(purchaseOrderDTO, purchaseOrderDO);
    if(purchaseOrderDTO.getDeliveryStatus().shortValue() >= PurchaseDeliveryStatusConstants.STATUS_STORAGE_PART.shortValue()){
      PurchaseQuery query = new PurchaseQuery();
      query.setParentPurchaseId(purchaseOrderDTO.getPurchaseId());
      Result<Map<String, Object>> purchaseResult = querySubPurchaseList(query);
      if(null != purchaseResult && purchaseResult.getSuccess() && null != purchaseResult.getModule()){
        List<PurchaseSubOrderDTO> purchaseSubOrderDTOs = (List<PurchaseSubOrderDTO>) purchaseResult.getModule().get(SUB_PURCHASE_LIST_KEY);
        long actualAmount = 0;
        for(PurchaseSubOrderDTO purchaseSubOrderDTO : purchaseSubOrderDTOs){
          if(null != purchaseSubOrderDTO.getSettlementPrice()){
            actualAmount = new BigDecimal(actualAmount).add(new BigDecimal(purchaseSubOrderDTO.getSettlementPrice())).longValue();
          }
        }
        purchaseOrderDO.setActualAmount(actualAmount);
      }
    }
    Integer updateNum = myPurchaseOrderDOMapper.updateByPrimaryKeySelective(purchaseOrderDO);
    Boolean isUpdate = updateNum > 0 ? true : false;
    return Result.getSuccDataResult(isUpdate);
  }

  @Override
  public Result<Boolean> insertSubPurchase(PurchaseSubOrderDTO purchaseSubOrderDTO) {
    if(null == purchaseSubOrderDTO){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "the param is null");
    }
    PurchaseSubOrderDO purchaseSubOrderDO = new PurchaseSubOrderDO();
    BeanCopierUtils.copyProperties(purchaseSubOrderDTO, purchaseSubOrderDO);
    Integer insertSubOrderNum = myPurchaseSubOrderDOMapper.insert(Arrays.asList(purchaseSubOrderDO));
    Boolean result = (insertSubOrderNum > 0 ? true : false);
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<Boolean> batchInsertSubPurchase(List<PurchaseSubOrderDTO> purchaseSubOrderDTOs) {
    if(CollectionUtils.isEmpty(purchaseSubOrderDTOs)){
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "the param is null");
    }
    List<PurchaseSubOrderDO> purchaseSubOrderDOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(purchaseSubOrderDTOs, purchaseSubOrderDOs, PurchaseSubOrderDO.class);
    Integer insertSubOrderNum = myPurchaseSubOrderDOMapper.insert(purchaseSubOrderDOs);
    Boolean result = insertSubOrderNum > 0 ? true : false;
    return Result.getSuccDataResult(result);
  }



}
