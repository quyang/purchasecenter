package com.xianzaishi.purchasecenter.component.user;

import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.bill.BillService;
import com.xianzaishi.purchasecenter.client.bill.dto.BillDetailsDTO;
import com.xianzaishi.purchasecenter.client.bill.dto.BillInformationDTO;
import com.xianzaishi.purchasecenter.client.bill.dto.BillInsertDTO;
import com.xianzaishi.purchasecenter.client.bill.dto.BillUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.UserBillDTO;
import com.xianzaishi.purchasecenter.dal.bill.dao.BillDOMapper;
import com.xianzaishi.purchasecenter.dal.bill.dataobject.BillDO;
import com.xianzaishi.purchasecenter.dal.bill.dataobject.BillUserDO;
import com.xianzaishi.purchasecenter.dal.user.dao.UserDOMapper;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author quyang 2017-2-13 15:14:36
 */
@Service("billService")
public class BillServiceImpl implements BillService {

  private static final Logger logger = Logger.getLogger(BillServiceImpl.class);

  @Autowired
  private UserDOMapper userDOMapper;

  @Autowired
  private BillDOMapper billDOMapper;

  @Override
  public Result<Boolean> insertBillInfo(UserBillDTO dto) {
    try {
      if (dto == null || (long) dto.getBillNumber() <= 0 || (long) dto.getMoney() <= 0
          || (long) dto.getOrderId() <= 0 || (long) dto.getUserId() <= 0) {
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数异常");
      }

      logger.error(
          "BillDOMMapper.java orderId=" + dto.getOrderId() + ", userId=" + dto.getUserId()
              + ", money="
              + dto.getMoney() + ", creattime=" + dto.getGmtCreate() + ", modifiedTime=" + dto
              .getGmtModified() + ", contribution =" + dto.getContribution() + ", billNumber=" + dto
              .getBillNumber());

      BillDO billDO = new BillDO();
      BeanCopierUtils.copyProperties(dto, billDO);
      int insert = billDOMapper.insert(billDO);
      if (1 != insert) {
        logger.error("插入数据失败 BillDOMMapper.java");
        Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "插入数据失败");
      }
      return Result.getSuccDataResult(true);
    } catch (Exception e) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }
  }


  @Override
  public Result<Boolean> updateBillInfo(BillInformationDTO dto) {
    try {
      if (null == dto) {
        return Result
            .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "修改发票信息失败，请添加数据");
      }

      logger.error("参数dto billNumber="+dto.getBillNumber()+", billMoney="+dto.getBillNumber()+", contribuion="+dto.getContribution()+"="+dto.getUserId()+"="+dto.getPhoneNumber());

        BillDO billDO = new BillDO();
        billDO.setBillNumber(dto.getBillNumber());
        billDO.setBillMoneyLong(getRighMoney(dto.getBillMoney()));
        billDO.setUserId(dto.getUserId());
        billDO.setContribution(dto.getContribution());

      logger.error("参数billDO billNumber="+billDO.getBillNumber()+", billMoney="+billDO.getBillNumber()+", contribuion="+billDO.getContribution()+"="+billDO.getBillMoneyLong());

      int insert = billDOMapper.updateListDO(billDO);
      logger.error("结果结果结果结果结果结果结果="+insert);
      if (insert != 1) {
        logger.error("更新数据失败 BillDOMMapper.java="+insert);
        Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "修改发票信息失败");
      }
      return Result.getSuccDataResult(true);
    } catch (Exception e) {
      return Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }
  }

  @Override
  public Result<BillInformationDTO> selectBillInfoByOrderId(Long oid) {
    try {
      if (null == oid || (long) oid <= 0) {
        return Result
            .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询发票数据失败,订单id异常");
      }
      BillInformationDTO billDO = billDOMapper.selectBillByOrderId(oid);
      return Result.getSuccDataResult(billDO);
    } catch (Exception e) {
      return Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }

  }

  @Override
  public Result<BillInformationDTO> selectBillInfoByUid(Long uid) {
    try {
      if (null == uid || (long) uid <= 0) {
        return Result
            .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询用户发票信息失败，用户id异常");
      }

      BillDO bill = billDOMapper.selectById(uid);
      BillInformationDTO billInformationDTO = new BillInformationDTO();
      BeanCopierUtils.copyProperties(bill, billInformationDTO);
      return Result.getSuccDataResult(billInformationDTO);
    } catch (Exception e) {
      return Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }
  }

  @Override
  public Result<BillInformationDTO> selectBillInfoByBillNum(Long billNumber) {
    try {
      if (null == billNumber || (long) billNumber < 0) {
        return Result
            .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询用户发票信息失败，用户发票id异常");
      }
      BillDO billDO = billDOMapper.selectByBillNumber(billNumber);
      BillInformationDTO billInformationDTO = new BillInformationDTO();
      BeanCopierUtils.copyProperties(billDO, billInformationDTO);
      return Result.getSuccDataResult(billInformationDTO);
    } catch (Exception e) {
      return Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }
  }

  @Override
  public Result<Boolean> hasBillByOrderId(Long orderId) {
    try {
      if (null == orderId || (long) orderId < 0) {
        return Result
            .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"判断是否开发票失败");
      }
      BillInformationDTO info = billDOMapper.selectBillByOrderId(orderId);
      if (null != info && null != info.getOrderId()) {
        long id = (long) info.getOrderId();
        if (id == (long) orderId) {
          return Result
              .getSuccDataResult(true);
        }
      }
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询失败");
    } catch (Exception e) {
      logger.error("BillService.java 查询发票信息失败");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, e.getMessage());
    }
  }

  @Override
  public Result<Boolean> bactchBillVO(BillInsertDTO vo) {
    if (null == vo || null == vo.getOrders() || vo.getOrders().size() == 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "请求参数异常");
    }

    List<BillDetailsDTO> orders = vo.getOrders();
    String contribution = vo.getContribution();//订单描述
    Long billNumber = vo.getBillNumber();//发票号
    int size = orders.size();//订单数量
    String operator = vo.getOperator();//操作员

    BigDecimal billMoney = new BigDecimal("0");
    List<BillUserDO> billUserList = new ArrayList<>();
    for (int i = 0; i < orders.size(); i++) {
      BillDetailsDTO detailsDTO = orders.get(i);
      if (null == detailsDTO) {
        continue;
      }
      BillUserDO billUserDTO = new BillUserDO();

      Integer userId = detailsDTO.getUserId();//用户id
      Long orderId = detailsDTO.getOrderId();//订单id

      double v = Double.parseDouble(detailsDTO.getMoney()) * 100;
      String substring = String.valueOf(v).substring(0, String.valueOf(v).indexOf("."));

      billUserDTO.setOrderId(orderId);
      billUserDTO.setUserId(userId);
      billUserDTO.setMoney(Long.parseLong(substring));
      billUserDTO.setGmtCreate(getCurrentDate());
      billUserDTO.setGmtModified(getCurrentDate());
      billUserDTO.setContribution(contribution);
      billUserDTO.setBillNumber(billNumber);
      billUserDTO.setOrderCount(size);
      billUserDTO.setTag(1);
      billUserDTO.setOperator(operator);

      billMoney = billMoney
          .add(new BigDecimal(String.valueOf(substring)));//发票金额

      if (i == (size - 1)) {
        //设置发票金额
        BillUserDO userDTO = new BillUserDO();
        BeanCopierUtils.copyProperties(billUserDTO, userDTO);
        userDTO.setTag(0);
        userDTO.setOrderId(null);
        userDTO.setMoney(null);
        userDTO.setOperator(operator);

        billUserList.add(userDTO);
      }

      billUserList.add(billUserDTO);
    }

    for (int j = 0; j < billUserList.size(); j++) {
      BillUserDO billUserDO = billUserList.get(j);
      billUserDO.setBillMoney(billMoney.longValue());
    }

    int i = billDOMapper.batchBillVO(billUserList);
    if (i == (vo.getOrders().size() + 1)) {
      return Result.getSuccDataResult(true);
    }
    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
        "开发票失败" );

  }

  @Override
  public Result<List<BillUserDTO>> getBillList(Integer userId) {

    if ( null == userId || (int)userId <= 0 ) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "查询用户发票失败");
    }

    List<BillDO> billDOS = billDOMapper.queryBillList(userId);

    if (null != billDOS && billDOS.size() > 0) {

      for (int i = 0; i <billDOS.size() ; i++) {

      }

    }


    if (null != billDOS ) {
//      return Result.getSuccDataResult(billDOS);
    }

    return null;
  }

  /**
   * 获取当前date
   */
  public Date getCurrentDate() {
    try {
      //创建时间和修改时间
      SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      String format = sf.format(new Date());
      return sf.parse(format);
    } catch (ParseException e) {
      e.printStackTrace();
      return null;
    }
  }

  /**
   * 将string类型的金额转为long类型
   * @param money
   * @return
   */
  public Long getRighMoney(String money) {
    if (StringUtils.isEmpty(money)) {
      return -1L;
    }
    double v = Double.parseDouble(money) * 100;
    return Long.parseLong(String.valueOf(v).substring(0, String.valueOf(v).indexOf(".")));
  }


}
