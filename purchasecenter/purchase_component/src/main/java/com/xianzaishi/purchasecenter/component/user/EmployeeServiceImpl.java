package com.xianzaishi.purchasecenter.component.user;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.user.EmployeeService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO.UserTypeConstants;
import com.xianzaishi.purchasecenter.client.user.dto.EmployeeDTO;
import com.xianzaishi.purchasecenter.client.user.query.EmployeeQuery;
import com.xianzaishi.purchasecenter.dal.user.dao.EmployeeDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.UserDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dataobject.EmployeeDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.UserDO;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 雇员接口，包括单条查询和批量查询员工信息，插入和更新员工信息表
 * 
 * @author dongpo
 * 
 */
@Service("employeeservice")
public class EmployeeServiceImpl implements EmployeeService {

  @Autowired
  private EmployeeDOMapper employeeDOMapper;

  @Autowired
  private UserDOMapper userDOMapper;

  @Override
  public Result<EmployeeDTO> queryEmployeeById(Integer userId) {
    if (null == userId || userId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error,userId:" + userId);
    }
    EmployeeDO employeeDO = employeeDOMapper.selectById(userId);
    if (null == employeeDO) {
      return Result.getErrDataResult(ServerResultCode.EMPLOYEE_ERROR_CODE_NOT_EXIT,
          "query employee is null,userId:" + userId);
    }
    EmployeeDTO result = new EmployeeDTO();
    BeanCopierUtils.copyProperties(employeeDO, result);
    UserDO user = userDOMapper.selectById(userId);
    BeanCopierUtils.copyProperties(user, result);
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<EmployeeDTO> queryEmployeeByName(String userName) {
    if (StringUtils.isEmpty(userName)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error,userName:" + userName);
    }
    EmployeeDO employeeDO = employeeDOMapper.selectByName(userName);
    if (null == employeeDO) {
      return Result.getErrDataResult(ServerResultCode.EMPLOYEE_ERROR_CODE_NOT_EXIT,
          "query employee is null,userName:" + userName);
    }
    EmployeeDTO result = new EmployeeDTO();
    BeanCopierUtils.copyProperties(employeeDO, result);
    UserDO user = userDOMapper.selectById(result.getUserId());
    BeanCopierUtils.copyProperties(user, result);
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<List<EmployeeDTO>> queryEmployeeByQuery(EmployeeQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is null");
    }
    List<EmployeeDO> tmp = employeeDOMapper.select(query);
    if (CollectionUtils.isEmpty(tmp)) {
      return Result.getErrDataResult(
          ServerResultCode.EMPLOYEE_ERROR_CODE_NOT_EXIT,
          ",query.organizationId:"
              + query.getOrganizationId());
    }
    List<EmployeeDTO> result = Lists.newArrayList();
    BeanCopierUtils.copyListBean(tmp, result, EmployeeDTO.class);
    
    List<Integer> idList = Lists.newArrayList();
    Map<Integer, EmployeeDTO> userIdMap = Maps.newHashMap();
    for(EmployeeDTO employee:result){
      idList.add(employee.getUserId());
      userIdMap.put(employee.getUserId(), employee);
    }
    
    List<UserDO> userList = userDOMapper.selectIdList(idList);
    for(UserDO tmpUser:userList){
      EmployeeDTO emp = userIdMap.get(tmpUser.getUserId());
      if(null != emp){
        BeanCopierUtils.copyProperties(tmpUser, emp);
      }
    }
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<Integer> insertEmployee(EmployeeDTO employeeDto) {
    if (null == employeeDto) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert parameter is null");
    }

    if (StringUtils.isEmpty(employeeDto.getName()) || StringUtils.isEmpty(employeeDto.getPwd())
        || employeeDto.getPhone() <= 0) {
      return Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert parameter error" + ",name:" + employeeDto.getName() + ",Pwd:"
              + employeeDto.getPwd() + ",Phone:" + employeeDto.getPhone());
    }
    UserDO user = this.getUserDOByEmployeeDTO(employeeDto);
    int insertCount = userDOMapper.insert(user);
    if (insertCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert user db error");
    }

    int userId = user.getUserId();
    employeeDto.setUserId(userId);

    EmployeeDO employeeDO = new EmployeeDO();
    BeanCopierUtils.copyProperties(employeeDto, employeeDO);
    insertCount = employeeDOMapper.insert(employeeDO);
    if (insertCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert employee db error");
    }
    return Result.getSuccDataResult(userId);
  }

  @Override
  public Result<Boolean> updateEmployee(EmployeeDTO employeeDto) {
    if (null == employeeDto) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert parameter is null");
    }

    if (StringUtils.isEmpty(employeeDto.getName()) || null == employeeDto.getPhone()
        || employeeDto.getPhone() <= 0 || null == employeeDto.getUserId()
        || employeeDto.getUserId() <= 0) {
      return Result.getErrDataResult(
          ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "insert parameter error" + ",name:" + employeeDto.getName() + ",Pwd:"
              + employeeDto.getPwd() + ",Phone:" + employeeDto.getPhone());
    }

    UserDO dbUser = userDOMapper.selectById(employeeDto.getUserId());
    if (!dbUser.getRole().equals(employeeDto.getRole())) {
      dbUser.setRole(employeeDto.getRole());
      int update = userDOMapper.update(dbUser);
      if (update != 1) {
        return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update user db error"
            + ",UserId:" + employeeDto.getUserId());
      }
    }

    EmployeeDO employeeDO = new EmployeeDO();
    BeanCopierUtils.copyProperties(employeeDto, employeeDO);
    int updateCount = employeeDOMapper.update(employeeDO);
    if (updateCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update employee db error");
    }
    return Result.getSuccDataResult(true);
  }

  /**
   * 查询userDO
   * @param phone
   * @return
   */
  @Override
  public Result<BackGroundUserDTO> queryUserDOByPhone(Long phone) {
    if (null == phone) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT, "请求参数不正确");
    }

    UserDO userDO = userDOMapper.selectByPhone(phone);
    BackGroundUserDTO user = new BackGroundUserDTO();
    BeanCopierUtils.copyProperties(userDO,user);
    return Result.getSuccDataResult(user);
  }

  /**
   * 获取一个用户基本信息
   * 
   * @return
   */
  private UserDO getUserDOByEmployeeDTO(EmployeeDTO employeeDto) {
    UserDO user = new UserDO();
    Date now = new Date();
    user.setName(employeeDto.getName());
    user.setUserType(UserTypeConstants.USER_EMPLOYEE);
    user.setGmtCreate(now);
    user.setGmtModified(now);
    user.setRole(employeeDto.getRole());
    user.setPwd(employeeDto.getPwd());
    user.setPhone(employeeDto.getPhone());
    return user;
  }
}
