package com.xianzaishi.customercenter.component;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.customercenter.client.customerservice.CustomerService;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO;
import com.xianzaishi.customercenter.client.customerservice.dto.GiveCouponProcessDTO;
import com.xianzaishi.customercenter.client.customerservice.dto.ProcessDTO;
import com.xianzaishi.customercenter.client.customerservice.dto.ProcessDTO.ProcessTypeConstants;
import com.xianzaishi.customercenter.client.customerservice.dto.ProofDataDTO;
import com.xianzaishi.customercenter.client.customerservice.dto.RefundProcessDTO;
import com.xianzaishi.customercenter.client.customerservice.query.CustomerserviceQuery;
import com.xianzaishi.customercenter.dal.customerservice.dao.CustomerServiceTaskDOMapper;
import com.xianzaishi.customercenter.dal.customerservice.dataobject.CustomerServiceTaskDO;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.pic.PicConstants;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.wms.common.vo.SimpleResultVO;
import com.xianzaishi.wms.tmscore.domain.client.itf.IDistributionDomainClient;
import com.xianzaishi.wms.tmscore.vo.DistributionVO;

/**
 * 客服接口
 * 
 * @author zhancang
 */
@Service("customerservice")
public class CustomerServiceImpl implements CustomerService {

  private static final Logger logger = Logger.getLogger(CustomerServiceImpl.class);

  private static final String TYPE_SPLIT = ",";

  private static final char JSON_SPLIT = (char)7;

  private static final String JSON_TYPE_SPLIT = "|";

  @Autowired
  private CustomerServiceTaskDOMapper customerServiceTaskDOMapper;
  
  @Autowired
  private IDistributionDomainClient distributionDomainClient;


  @Override
  public Result<Long> insertCustomerServiceTask(CustomerServiceTaskDTO customerServiceTask) {
    if (null == customerServiceTask) {
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_PARAMETER_NULL_ERROR,
          "Insert parameter is null");
    }

    CustomerServiceTaskDO task = new CustomerServiceTaskDO();
    BeanCopierUtils.copyProperties(customerServiceTask, task);

    updateToDBObjToJson(customerServiceTask, task);

    int id = customerServiceTaskDOMapper.insert(task);
    if (id > 0) {
      // 获取配送单id
      SimpleResultVO<List<DistributionVO>> distributionResult =
          distributionDomainClient.getDistributionByOrderID(Long.valueOf(customerServiceTask
              .getBizId()));
      if (null != distributionResult && distributionResult.isSuccess()
          && CollectionUtils.isNotEmpty(distributionResult.getTarget())) {
        List<DistributionVO> distributionVOs = distributionResult.getTarget();
        for(DistributionVO distributionVO:distributionVOs){
//          DistributionVO distributionVO = distributionVOs.get(0);
          //关闭配送订单
          SimpleResultVO<Boolean> closeResult =
              distributionDomainClient.closeDistribution(distributionVO);
          if (null == closeResult || !closeResult.isSuccess() || null == closeResult.getTarget()
              || !closeResult.getTarget()) {
            logger.error("配送订单关闭失败，原因：" + JackSonUtil.getJson(closeResult));
          }
        }
      }
      return Result.getSuccDataResult(task.getId());
    } else {
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_INSERT_FAILED_ERROR,
          "Insert CustomerServiceTaskDO failed,bizId:" + customerServiceTask.getBizId());
    }
  }

  @Override
  public Result<Boolean> updateCustomerServiceTask(CustomerServiceTaskDTO customerServiceTask) {
    if (null == customerServiceTask) {
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_PARAMETER_NULL_ERROR,
          "Insert parameter is null");
    }

    CustomerServiceTaskDO task = new CustomerServiceTaskDO();
    BeanCopierUtils.copyProperties(customerServiceTask, task);


    updateToDBObjToJson(customerServiceTask, task);

    logger.error("task="+JackSonUtil.getJson(task));

    int count = customerServiceTaskDOMapper.update(task);
    if (count > 0) {
      return Result.getSuccDataResult(true);
    } else {
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_UPDATE_FAILED_ERROR,
          "Update CustomerServiceTaskDO failed,bizId:" + customerServiceTask.getBizId());
    }
  }


  @Override
  public Result<List<CustomerServiceTaskDTO>> queryCustomerServiceTask(CustomerserviceQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_PARAMETER_NULL_ERROR,
          "Query parameter is null");
    }

    logger.error("参数="+JackSonUtil.getJson(query));
    List<CustomerServiceTaskDO> dbResult = customerServiceTaskDOMapper.select(query);
    logger.error("查询结果="+JackSonUtil.getJson(dbResult));
    if (CollectionUtils.isEmpty(dbResult)) {
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_QUERY_RESULT_EMPTY_ERROR,
          "Query result is null");
    }

    List<CustomerServiceTaskDTO> result = Lists.newArrayList();
    for (int i = 0; i < dbResult.size(); i++) {
      CustomerServiceTaskDO tmpDbResult = dbResult.get(i);
      CustomerServiceTaskDTO tmpResult = new CustomerServiceTaskDTO();
      BeanCopierUtils.copyProperties(tmpDbResult, tmpResult);
      updateFromDBJsonToObj(tmpDbResult, tmpResult);
      result.add(tmpResult);
    }
    return Result.getSuccDataResult(result);
  }
  
  

  @Override
  public Result<Integer> queryCustomerServiceTaskCount(CustomerserviceQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_PARAMETER_NULL_ERROR,
          "Query parameter is null");
    }

    int dbResult = customerServiceTaskDOMapper.count(query);
    if(dbResult == 0){
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_QUERY_RESULT_EMPTY_ERROR,
          "Query result is null");
    }
    return Result.getSuccDataResult(dbResult);
  }

  @Override
  public Result<CustomerServiceTaskDTO> queryCustomerServiceTaskById(Long id) {
    if (null == id || id <= 0) {
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_PARAMETER_NULL_ERROR,
          "Query id is null");
    }

    CustomerServiceTaskDO dbResult = customerServiceTaskDOMapper.selectById(id);
    if (null == dbResult) {
      return Result.getErrDataResult(ServerResultCode.CUSTOMERSERVICE_QUERY_RESULT_EMPTY_ERROR,
          "Query result is null,query id:" + id);
    }
    CustomerServiceTaskDTO result = new CustomerServiceTaskDTO();
    BeanCopierUtils.copyProperties(dbResult, result);
    updateFromDBJsonToObj(dbResult, result);

    return Result.getSuccDataResult(result);
  }

  /**
   * 请求对象转换成json 省事就要写恶心的代码咯，哈哈哈哈哈哈哈
   * 
   * @param cProcessList
   * @return
   */
  private String getProcessJson(List<ProcessDTO> processList) {
    if (CollectionUtils.isEmpty(processList)) {
      return "";
    }
    String type = "";
    String jsonStr = "";
    for (ProcessDTO process : processList) {
      if (process instanceof RefundProcessDTO) {
        type = type + ProcessTypeConstants.PROCESS_REFUND + TYPE_SPLIT;
        jsonStr = jsonStr + JackSonUtil.getJson(process) + String.valueOf(JSON_SPLIT);
      } else if (process instanceof GiveCouponProcessDTO) {
        type = type + ProcessTypeConstants.PROCESS_GIVE_COUPON + TYPE_SPLIT;
        jsonStr = jsonStr + JackSonUtil.getJson(process) + String.valueOf(JSON_SPLIT);
      }
    }

    return type + JSON_TYPE_SPLIT + jsonStr;
  }

  /**
   * 用户提交的证据数据，格式话称json放库里面
   */
  private String getCustomerProof(List<ProofDataDTO> proofInfo, boolean isAdd) {
    if (CollectionUtils.isNotEmpty(proofInfo)) {
      for (ProofDataDTO data : proofInfo) {
        if (null != data.getType() && data.getType() == ProofDataDTO.PIC_FLAG) {
          String source = data.getSource();
          if (StringUtils.isNotEmpty(source) && !source.startsWith(PicConstants.PIC_PREFIX) && isAdd) {
            data.setSource(PicConstants.PIC_PREFIX + source);
          } else if (StringUtils.isNotEmpty(source) && source.startsWith(PicConstants.PIC_PREFIX) && !isAdd) {
            data.setSource(source.replaceFirst(PicConstants.PIC_PREFIX, ""));
          }
        }
      }
      return JackSonUtil.getJson(proofInfo);
    }
    return "";
  }

  /**
   * 将数据库中的json转成格式化对象返回前台
   */
  private List<ProcessDTO> getProcessList(String processStr) {
    if (StringUtils.isEmpty(processStr)) {
      return Collections.emptyList();
    }

    int splitIndex = processStr.indexOf(JSON_TYPE_SPLIT);
    if (splitIndex <= 0) {
      logger.error("Db customerServiceInfo error:" + processStr);
      return Collections.emptyList();
    }
    String typeStr = processStr.substring(0, splitIndex);
    String json = processStr.substring(splitIndex+1);
    if (StringUtils.isEmpty(json) || StringUtils.isEmpty(typeStr)) {
      logger.error("Db customerServiceInfo split error:" + processStr);
      return Collections.emptyList();
    }

    String[] typeArray = typeStr.split(TYPE_SPLIT);
    String[] jsonArray = json.split(String.valueOf(JSON_SPLIT));
    if (typeArray.length != jsonArray.length) {
      logger.error("Db customerServiceInfo format error:" + processStr);
      return Collections.emptyList();
    }

    List<ProcessDTO> result = Lists.newArrayList();
    for (int i = 0; i < jsonArray.length; i++) {
      String type = typeArray[i];
      String tmpProcessStr = jsonArray[i];
      if (ProcessTypeConstants.PROCESS_GIVE_COUPON.toString().equals(type)) {
        GiveCouponProcessDTO process =
            (GiveCouponProcessDTO) JackSonUtil.jsonToObject(tmpProcessStr,
                GiveCouponProcessDTO.class);
        result.add(process);
      } else if (ProcessTypeConstants.PROCESS_REFUND.toString().equals(type)) {
        RefundProcessDTO process =
            (RefundProcessDTO) JackSonUtil.jsonToObject(tmpProcessStr, RefundProcessDTO.class);
        result.add(process);
      }
    }
    return result;
  }


  /**
   * 用户提交的证据数据，json格式从数据库转到前台
   */
  private List<ProofDataDTO> getCustomerProofList(String proofInfo, boolean isAdd) {
    if (StringUtils.isEmpty(proofInfo)) {
      return Collections.emptyList();
    }

    List<ProofDataDTO> proofList =
        (List<ProofDataDTO>) JackSonUtil.jsonToList(proofInfo, ProofDataDTO.class);
    if (CollectionUtils.isEmpty(proofList)) {
      return Collections.emptyList();
    }
    for (ProofDataDTO data : proofList) {
      if (data.getType() == ProofDataDTO.PIC_FLAG) {
        String source = data.getSource();
        if (StringUtils.isNotEmpty(source) && !source.startsWith(PicConstants.PIC_PREFIX) && isAdd) {
          data.setSource(PicConstants.PIC_PREFIX + source);
        } else if (StringUtils.isNotEmpty(source) && source.startsWith(PicConstants.PIC_PREFIX) && !isAdd) {
          data.setSource(source.replaceFirst(PicConstants.PIC_PREFIX, ""));
        }
      }
    }
    return proofList;
  }

  /**
   * 更新到表里时，转换格式
   */
  private void updateToDBObjToJson(CustomerServiceTaskDTO customerServiceTask,
      CustomerServiceTaskDO task) {
    if (CollectionUtils.isNotEmpty(customerServiceTask.getCustomerRequireProcess())) {
      task.setCustomerRequire(getProcessJson(customerServiceTask.getCustomerRequireProcess()));
    }

    if (CollectionUtils.isNotEmpty(customerServiceTask.getServiceResponseProcess())) {
      task.setServiceResponse(getProcessJson(customerServiceTask.getServiceResponseProcess()));
    }

    if (CollectionUtils.isNotEmpty(customerServiceTask.getCustomerProofList())) {
      task.setCustomerProof(getCustomerProof(customerServiceTask.getCustomerProofList(), false));
    }
  }

  /**
   * 更新到前台请求时，转换格式
   */
  private void updateFromDBJsonToObj(CustomerServiceTaskDO task,
      CustomerServiceTaskDTO customerServiceTask) {
    if (StringUtils.isNotEmpty(task.getCustomerRequire())) {
      customerServiceTask.setCustomerRequireProcess(getProcessList(task.getCustomerRequire()));
    }

    if (StringUtils.isNotEmpty(task.getServiceResponse())) {
      customerServiceTask.setServiceResponseProcess(getProcessList(task.getServiceResponse()));
    }

    if (StringUtils.isNotEmpty(task.getCustomerProof())) {
      customerServiceTask.setCustomerProofList(getCustomerProofList(task.getCustomerProof(), true));
    }
  }
}
