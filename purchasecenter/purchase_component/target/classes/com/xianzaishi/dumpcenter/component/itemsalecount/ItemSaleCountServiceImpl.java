package com.xianzaishi.dumpcenter.component.itemsalecount;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.dumpcenter.client.itemsalecount.ItemSaleCountService;
import com.xianzaishi.dumpcenter.client.itemsalecount.dto.ItemSaleCountDTO;
import com.xianzaishi.dumpcenter.client.itemsalecount.query.ItemSaleCountQuery;
import com.xianzaishi.dumpcenter.dal.itemsalecount.dao.ItemSaleCountDOMapper;
import com.xianzaishi.dumpcenter.dal.itemsalecount.dataobject.ItemSaleCountDO;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;

@Service("itemSaleCountService")
public class ItemSaleCountServiceImpl implements ItemSaleCountService {

  @Autowired
  private ItemSaleCountDOMapper itemSaleCountDOMapper;

  @Override
  public Result<List<ItemSaleCountDTO>> queryItemSaleCountLast7Days(List<Long> skuIds) {
    if (CollectionUtils.isEmpty(skuIds)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query param is null");
    }
    List<Date> saleDays = Lists.newArrayList();
    Calendar calendar = Calendar.getInstance();
    for (int i = 1; i < 8; i++) {
      calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - i);
      Date today = calendar.getTime();
      saleDays.add(today);
    }
    ItemSaleCountQuery query = new ItemSaleCountQuery();
    query.setSkuIds(skuIds);
    query.setSaleDays(saleDays);

    // 1表示线上，2表示线下，获取线上销量
    query.setChannelType((short) 1);
    List<ItemSaleCountDO> itemSaleCountOnlineDOs =
        itemSaleCountDOMapper.selectItemSaleCountList(query);
    Map<Long, ItemSaleCountDO> itemSaleCountOnlineMapCache = Maps.newHashMap();
    // 合并线上商品销量
    if (CollectionUtils.isNotEmpty(itemSaleCountOnlineDOs)) {
      for (ItemSaleCountDO itemSaleCountDO : itemSaleCountOnlineDOs) {
        Long skuId = itemSaleCountDO.getSkuId();
        ItemSaleCountDO itemSaleCountDOCache = itemSaleCountOnlineMapCache.get(skuId);
        if (null == itemSaleCountDO.getSaleCount()) {
          itemSaleCountDO.setSaleCount(0.0);
        }
        if (null != itemSaleCountDOCache) {
          Double countTemp =
              new BigDecimal(Double.toString(itemSaleCountDO.getSaleCount()))
                  .add(new BigDecimal(Double.toString(itemSaleCountDOCache.getSaleCount())))
                  .setScale(5, BigDecimal.ROUND_DOWN).doubleValue();
          itemSaleCountDOCache.setSaleCount(countTemp);
        } else {
          itemSaleCountOnlineMapCache.put(skuId, itemSaleCountDO);
        }
      }
    }

    // 获取线下销量
    query.setChannelType((short) 2);
    List<ItemSaleCountDO> itemSaleCountOfflineDOs =
        itemSaleCountDOMapper.selectItemSaleCountList(query);
    Map<Long, ItemSaleCountDO> itemSaleCountOfflineMapCache = Maps.newHashMap();
    // 合并线下商品销量
    if (CollectionUtils.isNotEmpty(itemSaleCountOfflineDOs)) {
      for (ItemSaleCountDO itemSaleCountDO : itemSaleCountOfflineDOs) {
        Long skuId = itemSaleCountDO.getSkuId();
        ItemSaleCountDO itemSaleCountDOCache = itemSaleCountOfflineMapCache.get(skuId);
        if (null == itemSaleCountDO.getSaleCount()) {
          itemSaleCountDO.setSaleCount(0.0);
        }
        if (null != itemSaleCountDOCache) {
          Double countTemp =
              new BigDecimal(Double.toString(itemSaleCountDO.getSaleCount()))
                  .add(new BigDecimal(Double.toString(itemSaleCountDOCache.getSaleCount())))
                  .setScale(5, BigDecimal.ROUND_DOWN).doubleValue();
          itemSaleCountDOCache.setSaleCount(countTemp);
        } else {
          itemSaleCountOfflineMapCache.put(skuId, itemSaleCountDO);
        }
      }
    }

    // 设置返回值
    List<ItemSaleCountDTO> itemSaleCountDTOs = Lists.newArrayList();
    for (Long skuId : skuIds) {
      ItemSaleCountDTO itemSaleCountDTO = new ItemSaleCountDTO();
      itemSaleCountDTO.setSkuId(skuId);
      ItemSaleCountDO itemSaleCountOnlineCache = itemSaleCountOnlineMapCache.get(skuId);
      if (null != itemSaleCountOnlineCache) {
        itemSaleCountDTO.setSaleCountOnline(new BigDecimal(Double.toString(itemSaleCountOnlineCache
            .getSaleCount())).divide(new BigDecimal(7),6,BigDecimal.ROUND_HALF_UP)
            .doubleValue());
      } else {
        itemSaleCountDTO.setSaleCountOnline(0.0);
      }
      ItemSaleCountDO itemSaleCountOfflineCache = itemSaleCountOfflineMapCache.get(skuId);
      if (null != itemSaleCountOfflineCache) {
        itemSaleCountDTO.setSaleCountOffline(new BigDecimal(Double.toString(itemSaleCountOfflineCache
            .getSaleCount())).divide(new BigDecimal(7),6,BigDecimal.ROUND_HALF_UP)
            .doubleValue());
      } else {
        itemSaleCountDTO.setSaleCountOffline(0.0);
      }
      itemSaleCountDTOs.add(itemSaleCountDTO);
    }
    return Result.getSuccDataResult(itemSaleCountDTOs);
  }

}
