package com.xianzaishi.purchasecenter.component.logmonitor;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.logmonitor.LogMonitorService;
import com.xianzaishi.purchasecenter.client.logmonitor.dto.LogMonitorDTO;
import com.xianzaishi.purchasecenter.client.logmonitor.dto.LogMonitorDTO.LogMonitorDTOConstants;
import com.xianzaishi.purchasecenter.client.logmonitor.query.LogMonitorQuery;
import com.xianzaishi.purchasecenter.dal.logmonitor.dao.LogMonitorDOMapper;
import com.xianzaishi.purchasecenter.dal.logmonitor.dataobject.LogMonitorDO;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("logMonitorService")
public class LogMonitorServiceImpl implements LogMonitorService {

  @Autowired
  private LogMonitorDOMapper logMonitorDOMapper;

  private static final Logger logger = Logger.getLogger(LogMonitorServiceImpl.class);

  @Override
  public Result<Integer> insertLog(LogMonitorDTO logMonitorDTO) {
    if (null == logMonitorDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数对象为空");
    }

    LogMonitorDO logMonitorDO = new LogMonitorDO();
    BeanCopierUtils.copyProperties(logMonitorDTO, logMonitorDO);

    logger.error("插入的数据=" + JackSonUtil.getJson(logMonitorDO));

    return Result.getSuccDataResult(logMonitorDOMapper.insert(logMonitorDO));
  }

  @Override
  public Result<Boolean> updateLog(LogMonitorDTO logMonitorDTO) {
    if (null == logMonitorDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数对象为空");
    }
//    LogMonitorDO logMonitorDO = logMonitorDOMapper.selectByName(logMonitorDTO.getName());
//    if(null == logMonitorDO || null == logMonitorDO.getId()){
//      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "log is not exist,please insert it first");
//    }
    LogMonitorDO logMonitorDO = new LogMonitorDO();
    BeanCopierUtils.copyProperties(logMonitorDTO, logMonitorDO);
    logger.error("更新数据=" + JackSonUtil.getJson(logMonitorDO));
    Boolean updateResult = false;
    if (logMonitorDOMapper.updateByPrimaryKey(logMonitorDO) > 0) {
      updateResult = true;
    }
    return Result.getSuccDataResult(updateResult);
  }

  @Override
  public Result<List<LogMonitorDTO>> queryLog(LogMonitorQuery query) {
    if (null == query) {
      return Result
          .getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询参数对象为空");
    }

    LogMonitorDO logMonitorDO = new LogMonitorDO();
    BeanCopierUtils.copyProperties(query, logMonitorDO);

    List<LogMonitorDO> logMonitorDOList = logMonitorDOMapper.select(logMonitorDO);
    logger.error("查询信息=" + JackSonUtil.getJson(logMonitorDOList));
    if (CollectionUtils.isEmpty(logMonitorDOList)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询的监控信息不存在");
    }

    List<LogMonitorDTO> arrayList = new ArrayList<>();
    for (LogMonitorDO monitorDO : logMonitorDOList) {
      LogMonitorDTO logMonitorDTO = new LogMonitorDTO();
      BeanCopierUtils.copyProperties(monitorDO, logMonitorDTO);
      arrayList.add(logMonitorDTO);
    }

    logger.error("拷贝后的数据集合=" + JackSonUtil.getJson(arrayList));

    return Result.getSuccDataResult(arrayList);
  }

  @Override
  public Result<Boolean> insertOrUpdate(Short type,Boolean isOnlyUpdate) {
    if (null == type || type < 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "type参数错误");
    }

    LogMonitorDO monitorDO = new LogMonitorDO();
    monitorDO.setType(type);
    monitorDO.setStatus(LogMonitorDTOConstants.NEW_DATA);
    List<LogMonitorDO> monitorDOS = logMonitorDOMapper.select(monitorDO);
    if (null != monitorDOS && CollectionUtils.isNotEmpty(monitorDOS)) {
      //已经有监控数据
      LogMonitorDO logMonitorDO = monitorDOS.get(0);

      //只更新修改时间
      if (isOnlyUpdate) {
        int i = logMonitorDOMapper.updateByPrimaryKey(logMonitorDO);
        if (i == 1) {
          return Result.getSuccDataResult(true);
        }

        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"更新数据失败" );
      }

      logMonitorDO.setStatus((LogMonitorDTOConstants.OLD_DATA));
      int i = logMonitorDOMapper.updateByPrimaryKey(logMonitorDO);
      if (i == 1) {
        logMonitorDO.setStatus(LogMonitorDTOConstants.NEW_DATA);
        if (logMonitorDOMapper.insert(logMonitorDO) == 1) {
          return Result.getSuccDataResult(true);
        }
        return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,"插入新数据失败" );
      }
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新数据失败");
    } else {
      logger.error("没有监控信息");
      //没有监控数据
      int insert = logMonitorDOMapper.insert(monitorDO);
      if (1 == insert) {
        return Result.getSuccDataResult(true);
      }
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "插入新数据失败");
    }
  }

}
