package com.xianzaishi.purchasecenter.component.mail;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFSimpleShape;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.mail.MailService;
import com.xianzaishi.purchasecenter.client.mail.dto.SheetDTO;

@Service("mailService")
public class MailServiceImpl implements MailService {

  private static final Logger logger = Logger.getLogger(MailServiceImpl.class);

  private Properties props; // 系统属性
  private Session session; // 邮件会话对象
  private MimeMessage mimeMsg; // MIME邮件对象
  private Multipart mp; // Multipart对象,邮件内容,标题,附件等内容均添加到其中后再生成MimeMessage对象

  /**
   * Constructor
   * 
   * @param smtp 邮件发送服务器
   */
  public void init() {
    InputStream is =
        MailServiceImpl.class.getClassLoader().getResourceAsStream("mail-config.properties");
    props = System.getProperties();
    try {
      props.load(is);
      props.put("mail.smtp.auth", "true");
      props.put("mail.smtp.host", props.getProperty("mail.smtp"));
      props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
      props.setProperty("mail.smtp.port", "994");
      props.setProperty("mail.smtp.socketFactory.port", "994");
      props.put("username", props.getProperty("mail.username"));
      props.put("password", props.getProperty("mail.password"));
      session = Session.getDefaultInstance(props, null);
      // session.setDebug(true);
      mimeMsg = new MimeMessage(session);
      mp = new MimeMultipart();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Override
  public Result<Boolean> sendMail(String to, String copyto, String subject, String content,
      List<String> fileNames) {
    if (StringUtils.isEmpty(to) || StringUtils.isEmpty(subject)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "收件人或者标题不能为空");
    }
    init();
    String from = props.getProperty("mail.username");
    try {
      // 设置发信人
      mimeMsg.setFrom(new InternetAddress(from));
      // 设置接收人
      InternetAddress[] iaToListcs = InternetAddress.parse(to);
      if (StringUtils.isNotEmpty(copyto)) {
        InternetAddress[] iacopyToListcs = InternetAddress.parse(copyto);
        // 设置抄送人
        mimeMsg.setRecipients(Message.RecipientType.CC, iacopyToListcs);
      }
      mimeMsg.setRecipients(Message.RecipientType.TO, iaToListcs);
      // 设置主题
      mimeMsg.setSubject(subject);
      // 设置正文
      BodyPart bp = new MimeBodyPart();
      bp.setContent(content, "text/html;charset=utf-8");
      mp.addBodyPart(bp);
      // 设置附件
      if (CollectionUtils.isNotEmpty(fileNames)) {
        for (String fileName : fileNames) {
          bp = new MimeBodyPart();
          FileDataSource fileds = new FileDataSource(fileName);
          bp.setDataHandler(new DataHandler(fileds));
          bp.setFileName(MimeUtility.encodeText(fileds.getName(), "UTF-8", "B"));
          mp.addBodyPart(bp);
        }
      }
      mimeMsg.setContent(mp);
      mimeMsg.saveChanges();
      // 发送邮件
      if (props.get("mail.smtp.auth").equals("true")) {
        Transport transport = session.getTransport("smtp");
        transport.connect((String) props.get("mail.smtp.host"), (String) props.get("username"),
            (String) props.get("password"));
        transport.sendMessage(mimeMsg, mimeMsg.getRecipients(Message.RecipientType.TO));
        transport.sendMessage(mimeMsg, mimeMsg.getRecipients(Message.RecipientType.CC));
        transport.close();
      } else {
        Transport.send(mimeMsg);
      }
      logger.info("邮件发送成功");
    } catch (MessagingException | UnsupportedEncodingException e) {
      logger.error("发送邮件失败：" + e.getMessage());
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    }
    return Result.getSuccDataResult(true);
  }

  @Override
  public Result<Boolean> createAttachment(List<SheetDTO> dataList) {
    ByteArrayOutputStream out = null;
    try {
      // create WritableWorkbook
      XSSFWorkbook workbook = new XSSFWorkbook();
      XSSFCellStyle styleHeader = generateHeaderStyle(workbook);
      if (null != dataList) {
        List<String> columnNames = null;
        List<Map<String, Object>> rows = null;
        Map<String, Object> row = null;
        XSSFRow xssfRow = null;
        String titleActive = null;
        String datePattern = null;
        int offset = 0;

        for (SheetDTO sheetVo : dataList) {
          columnNames = sheetVo.getColumnNames();
          rows = sheetVo.getRows();
          titleActive = sheetVo.getTitleActive();
          datePattern = sheetVo.getDatePattern();
          // 生成表格
          XSSFSheet sheet = workbook.createSheet(sheetVo.getSheetName());
          int columnsSize = columnNames.size();
          XSSFCell cell = null;
          // 生成头部样式
          if (null != titleActive) {
            offset = 5;
            xssfRow = sheet.createRow(0);
            cell = xssfRow.createCell(0);
            cell.setCellStyle(styleHeader);
            cell.setCellValue(titleActive);
            CellRangeAddress region = new CellRangeAddress(0, 0, 0, columnsSize - 1);
            sheet.addMergedRegion(region);
            // 添加元素，订货日期
            xssfRow = sheet.createRow(1);
            cell = xssfRow.createCell(0);
            cell.setCellValue("订货日期：");
            if (null != sheetVo.getPurchaseDate()) {
              cell = xssfRow.createCell(1);
              cell.setCellValue(sheetVo.getPurchaseDate());
            }
            // 采购单号
            cell = xssfRow.createCell(columnsSize - 2);
            cell.setCellValue("采购单号：");
            if (null != sheetVo.getOrderId()) {
              cell = xssfRow.createCell(columnsSize - 1);
              cell.setCellValue(sheetVo.getOrderId());
            }
            // 送货地址
            xssfRow = sheet.createRow(2);
            cell = xssfRow.createCell(0);
            cell.setCellValue("送货地址：");
            if (null != sheetVo.getAddress()) {
              cell = xssfRow.createCell(1);
              cell.setCellValue(sheetVo.getAddress());
            }
            CellRangeAddress region2 = new CellRangeAddress(2, 2, 1, columnsSize - 1);
            sheet.addMergedRegion(region2);
            // 供应商名称
            xssfRow = sheet.createRow(3);
            cell = xssfRow.createCell(0);
            cell.setCellValue("供应商名称：");
            if (null != sheetVo.getSupplierName()) {
              cell = xssfRow.createCell(1);
              cell.setCellValue(sheetVo.getSupplierName());
            }
            CellRangeAddress region3 = new CellRangeAddress(3, 3, 1, columnsSize - 1);
            sheet.addMergedRegion(region3);
            xssfRow = sheet.createRow(4);
            for (int i = 0; i < columnsSize; i++) {
              // 列名称
              cell = xssfRow.createCell(i);
              cell.setCellType(XSSFCell.CELL_TYPE_STRING);
              cell.setCellValue(columnNames.get(i));
              cell.setCellStyle(generateCellDefaultStyle(workbook));
            }
            logger.info("RDS INFO! titile for sheet: " + sheetVo.getTitleActive());
          } else {
            offset = 1;
            xssfRow = sheet.createRow(0);
            for (int i = 0; i < columnsSize; i++) {
              // 列名称
              cell = xssfRow.createCell(i);
              cell.setCellType(XSSFCell.CELL_TYPE_STRING);
              cell.setCellValue(columnNames.get(i));
              cell.setCellStyle(generateCellDefaultStyle(workbook));
            }
          }

          // 列数据
          Object cellValue = null;
          int rowSize = rows.size();
          int rowIndex = 0;
          for (int i = 0; i < rowSize; i++) {
            rowIndex = i + offset;
            xssfRow = sheet.createRow(rowIndex);
            row = rows.get(i);
            for (int j = 0; j < columnsSize; j++) {
              cell = xssfRow.createCell(j);
              cellValue = row.get(columnNames.get(j));
              cell.setCellType(XSSFCell.CELL_TYPE_STRING);
              setCellValue(cell, cellValue, datePattern);
              cell.setCellStyle(generateCellDefaultStyle(workbook));
            }
          }
          // 总计
          xssfRow = sheet.createRow(rowIndex + 1);
          for(int i = 0; i < columnsSize; i++){
            cell = xssfRow.createCell(i);
            if(0 == i){
              cell.setCellValue("总计：");
            }else if ((columnsSize - 2) == i && null != sheetVo.getTotalPrice()){
              cell.setCellValue(sheetVo.getTotalPrice());
            }else if((columnsSize - 1) == i && null != sheetVo.getTotalCount()){
              cell.setCellValue(sheetVo.getTotalCount());
            }
            cell.setCellStyle(generateCellDefaultStyle(workbook));
          }
          // 设置列宽度
          for (int i = 0; i < columnsSize; i++) {
            if (i == 1) {
              sheet.setColumnWidth(i, 12 * 2 * 256);
            } else if (i == 3) {
              sheet.setColumnWidth(i, 4 * 2 * 256);
            } else {
              sheet.setColumnWidth(i, 6 * 2 * 256);
            }
          }
        }
      }
      SheetDTO sheetDTO = dataList.get(0);
      // 
      String path =
          new StringBuilder("/usr/works/purchasefile/采购单_").append(sheetDTO.getOrderId()).append(".xlsx").toString();
      // 创建文件流
      OutputStream stream = new FileOutputStream(path);
      // 写入数据
      workbook.write(stream);
      // 关闭文件流
      stream.close();
      return Result.getSuccDataResult(true);
    } catch (Exception e) {
      logger.error("RDS ERROR!", e);
      return Result.getErrDataResult(ServerResultCode.SERVER_ERROR, e.getMessage());
    } finally {
      try {
        if (null != out) {
          out.close();
        }
      } catch (IOException e) {
        logger.error("RDS ERROR!", e);
      }
    }
  }

  private void setCellValue(XSSFCell cell, Object cellValue, String datePattern) {
    if (null == cellValue) {
      return;
    }
    // Number is the superclass of classes BigDecimal, BigInteger, Byte, Double, Float, Integer,
    // Long, and Short.
    if (cellValue instanceof Number) {
      cell.setCellValue(((Number) cellValue).doubleValue());
    } else if (cellValue instanceof Date) {
      String cellValueString = null;
      Date date = (Date) cellValue;
      if (null != datePattern) {
        try {
          cellValueString = new SimpleDateFormat(datePattern).format(date);
        } catch (Exception e) {
          cellValueString = null;
        }
      }
      if (null == cellValueString) {
        cellValueString = new SimpleDateFormat(datePattern).format(new Date());
      }
      cell.setCellValue(cellValueString);
    } else {
      cell.setCellValue(cellValue.toString());
    }
  }

  private XSSFCellStyle generateHeaderStyle(XSSFWorkbook workbook) {
    XSSFCellStyle styleHeader = workbook.createCellStyle();
    styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
    styleHeader.setWrapText(false);
    XSSFFont fontHeader = (XSSFFont) workbook.createFont();
    fontHeader.setFontName("宋体");
    fontHeader.setBold(true);
    fontHeader.setBoldweight((short) 12);
    fontHeader.setFontHeightInPoints((short) 20);
    fontHeader.setColor(IndexedColors.BLACK.getIndex());
    styleHeader.setFont(fontHeader);
    styleHeader.setFillPattern(XSSFCellStyle.NO_FILL);
    return styleHeader;
  }

  private XSSFCellStyle generateCellDefaultStyle(XSSFWorkbook workbook) {
    XSSFCellStyle styleCell = workbook.createCellStyle();
    styleCell.setWrapText(false);
    XSSFFont fontCell = workbook.createFont();
    fontCell.setFontName("宋体");
    fontCell.setFontHeightInPoints((short) 11);
    styleCell.setFont(fontCell);
    styleCell.setAlignment(XSSFCellStyle.ALIGN_CENTER);
    styleCell.setFillPattern(XSSFCellStyle.NO_FILL);
    styleCell.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);// 左边框
    styleCell.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);// 右边框
    styleCell.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);// 上边框
    styleCell.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM); // 下边框
    return styleCell;
  }

}
