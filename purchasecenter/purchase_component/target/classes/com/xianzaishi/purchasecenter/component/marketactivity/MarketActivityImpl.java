package com.xianzaishi.purchasecenter.component.marketactivity;

import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.marketactivity.MarketActivityService;
import com.xianzaishi.purchasecenter.client.marketactivity.dto.MarketActivityDTO;
import com.xianzaishi.purchasecenter.client.marketactivity.query.MarketActivityQuery;
import com.xianzaishi.purchasecenter.dal.marketactivity.dao.MarketActivityDOMapper;
import com.xianzaishi.purchasecenter.dal.marketactivity.dateobject.MarketActivityDO;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by quyang on 2017/4/14.
 */
@Service("marketActivityService")
public class MarketActivityImpl implements MarketActivityService {

  private static final Logger logger = Logger.getLogger(MarketActivityImpl.class);

  @Autowired
  private MarketActivityDOMapper marketActivityDOMapper;


  @Override
  public Result<Boolean> insert(MarketActivityDTO marketActivityDTO) {
    if (null == marketActivityDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数对象为空");
    }

    MarketActivityDO marketActivityDO = new MarketActivityDO();
    BeanCopierUtils.copyProperties(marketActivityDTO, marketActivityDO);
    Integer insert = marketActivityDOMapper.insert(marketActivityDO);
    if ( null != insert && insert == 1) {
      return Result.getSuccDataResult(true);
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "插入数据失败");
  }

  @Override
  public Result<Boolean> update(MarketActivityDTO marketActivityDTO) {
    if (null == marketActivityDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数对象为空");
    }

    MarketActivityDO marketActivityDO = new MarketActivityDO();
    BeanCopierUtils.copyProperties(marketActivityDTO, marketActivityDO);
    Integer update = marketActivityDOMapper.update(marketActivityDO);
    if (1 == update) {
      return Result.getSuccDataResult(true);
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "更新失败");
  }

  @Override
  public Result<Boolean> delete(Integer id) {
    if (null == id ) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "活动id和名称不能同时为空");
    }
    Integer delete = marketActivityDOMapper.delete(id);
    if (1 == delete) {
      return Result.getSuccDataResult(true);
    }

    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "删除失败");
  }

  @Override
  public Result<Integer> queryCount(MarketActivityQuery marketActivityQuery) {
    if (null == marketActivityQuery) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数对象为空");
    }
    Integer integer = marketActivityDOMapper.queryCount(marketActivityQuery);
    if (null == integer || integer < 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询活动数量失败");
    }
    return Result.getSuccDataResult(integer);
  }

  @Override
  public Result<Boolean> time() {
    logger.error("活动轮询定时任务执行中");
    MarketActivityQuery marketActivityQuery = new MarketActivityQuery();
    List<MarketActivityDO> marketActivityDOS = marketActivityDOMapper
        .queryAll(marketActivityQuery);
    logger.error("查询到的活动=" + JackSonUtil.getJson(marketActivityDOS));
//    if (CollectionUtils.isNotEmpty(marketActivityDOS)) {
//      for (MarketActivityDO marketActivity : marketActivityDOS) {
//        if (null == marketActivity) {
//          continue;
//        }
//        Date date = new Date();
//        Date gmtStart = marketActivity.getGmtStart();
//        Date gmtEnd = marketActivity.getGmtEnd();
//        if (null == gmtStart || null == gmtEnd) {
//          continue;
//        }
//
//        //0 有效  1 无效
//        if (date.getTime() == gmtStart.getTime()) {//当前系统时间等于活动开始时间
//          //开启活动
//          if (!MarketActivityStatus.EFFECTIVE_STATUS.equals(marketActivity.getStatus())) {
//            marketActivity.setStatus(MarketActivityStatus.EFFECTIVE_STATUS);
//          }
//        }
//
//        if (date.getTime() > gmtEnd.getTime()) {//活动结束时间小于当前系统时间，关闭活动
//          //关闭活动
//          if (!MarketActivityStatus.UN_EFFECTIVE_STATUS.equals(marketActivity.getStatus())) {
//            marketActivity.setStatus(MarketActivityStatus.UN_EFFECTIVE_STATUS);
//          }
//        }
//
//        update(marketActivity);
//      }
//    }
    return Result.getSuccDataResult(true);
  }

  @Override
  public Result<List<MarketActivityDTO>> query(MarketActivityQuery marketActivityQuery) {
    if (null == marketActivityQuery) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "参数对象为空");
    }

    List<MarketActivityDO> marketActivityDOS = marketActivityDOMapper
        .query(marketActivityQuery);

    List<MarketActivityDTO> list = new ArrayList<>();
    if (null != marketActivityDOS && CollectionUtils.isNotEmpty(marketActivityDOS)) {
      for (MarketActivityDO activityDO : marketActivityDOS) {
        if (null == activityDO) {
          continue;
        }
        MarketActivityDTO activityDTO = new MarketActivityDTO();
        BeanCopierUtils.copyProperties(activityDO,activityDTO);
        list.add(activityDTO);
      }
    }
    if (CollectionUtils.isNotEmpty(list)) {
      return Result.getSuccDataResult(list);
    }
    return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR, "查询营销活动为空");
  }
}
