package com.xianzaishi.purchasecenter.component.role;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.role.RoleService;
import com.xianzaishi.purchasecenter.client.role.dto.RoleDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.dal.role.dao.PageRoleDOMapper;
import com.xianzaishi.purchasecenter.dal.role.dao.RoleDOMapper;
import com.xianzaishi.purchasecenter.dal.role.dataobject.PageRoleDO;
import com.xianzaishi.purchasecenter.dal.role.dataobject.RoleDO;

@Service("roleservice")
public class RoleServiceImpl implements RoleService {

  @Autowired
  private RoleDOMapper roleDOMapper;

  @Autowired
  private PageRoleDOMapper pageRoleDOMapper;

  @Override
  public Result<RoleDTO> queryRoleById(Integer role) {
    RoleDO roleDO = roleDOMapper.selectRoleById(role);
    if (null == roleDO) {
      return Result.getErrDataResult(ServerResultCode.ROLE_ERROR_CODE_NOT_EXIT, "query db is null");
    }
    RoleDTO roleDTO = new RoleDTO();
    BeanCopierUtils.copyProperties(roleDO, roleDTO);
    return Result.getSuccDataResult(roleDTO);
  }

  @Override
  public Result<RoleDTO> queryRoleByName(String name) {
    RoleDO roleDO = roleDOMapper.selectRoleByName(name);
    if (null == roleDO) {
      return Result.getErrDataResult(ServerResultCode.ROLE_ERROR_CODE_NOT_EXIT, "query db is null");
    }
    RoleDTO roleDTO = new RoleDTO();
    BeanCopierUtils.copyProperties(roleDO, roleDTO);
    return Result.getSuccDataResult(roleDTO);
  }

  @Override
  public Result<Integer> insertRole(RoleDTO roleDto) {
    if (null == roleDto) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parmeter is null");
    }
    RoleDO roleDO = new RoleDO();
    BeanCopierUtils.copyProperties(roleDto, roleDO);
    int insertCount = roleDOMapper.insertRole(roleDO);
    if (1 == insertCount) {
      return Result.getSuccDataResult(roleDO.getRoleId());
    }
    return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert role failed");
  }

  @Override
  public Result<Boolean> updateRole(RoleDTO roleDto) {
    if (null == roleDto) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "parmeter is null");
    }
    RoleDO roleDO = new RoleDO();
    BeanCopierUtils.copyProperties(roleDto, roleDO);
    return Result.getSuccDataResult(roleDOMapper.updateRole(roleDO) == 1);
  }

  @Override
  public Result<List<RoleDTO>> queryRoleList(String pageId) {
    PageRoleDO roleInfo = pageRoleDOMapper.select(pageId);
    if (null == roleInfo || StringUtils.isEmpty(roleInfo.getRole())) {
      return Result.getErrDataResult(ServerResultCode.ROLE_ERROR_CODE_NOT_EXIT,
          "page not need role,pageId:" + pageId);
    }

    String roleArray[] = roleInfo.getRole().split(BackGroundUserDTO.ROLE_SPLIT);
    List<Integer> roleList = Lists.newArrayList();
    for (String tmp : roleArray) {
      if (StringUtils.isNotEmpty(tmp) && StringUtils.isNumeric(tmp)) {
        roleList.add(Integer.valueOf(tmp));
      }
    }
    if (CollectionUtils.isEmpty(roleList)) {
      return Result.getErrDataResult(ServerResultCode.ROLE_ERROR_CODE_NOT_EXIT,
          "page not need role,pageId:" + pageId);
    }

    List<RoleDO> roleListtmpResult = roleDOMapper.selectRole(roleList);
    if (CollectionUtils.isEmpty(roleListtmpResult)) {
      return Result.getErrDataResult(ServerResultCode.ROLE_ERROR_CODE_NOT_EXIT,
          "page not need role,pageId:" + pageId);
    }

    List<RoleDTO> result = Lists.newArrayList();
    BeanCopierUtils.copyListBean(roleListtmpResult, result, RoleDTO.class);
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<Boolean> addRole(String pageId, Integer roleId) {
    PageRoleDO roleInfo = pageRoleDOMapper.select(pageId);
    RoleDO roleDO = roleDOMapper.selectRoleById(roleId);
    if (null == roleDO) {
      return Result.getErrDataResult(ServerResultCode.ROLE_ERROR_CODE_NOT_EXIT, "query db is null");
    }
    if(null == roleInfo){
      roleInfo = new PageRoleDO();
      roleInfo.setRole(String.valueOf(roleId));
      roleInfo.setUrl(pageId);
      int insertCount = pageRoleDOMapper.insert(roleInfo);
      if(insertCount == 1){
        return Result.getSuccDataResult(true);
      }else{
        return Result.getErrDataResult(-1, "Insert failed");
      }
    }
    String roleInfoStr = roleInfo.getRole();
    if(StringUtils.isEmpty(roleInfoStr)){
      roleInfoStr = roleId+ BackGroundUserDTO.ROLE_SPLIT ;
    }else{
      String roleIdTmpArray[] = roleInfoStr.split(BackGroundUserDTO.ROLE_SPLIT);
      List<String> roleList = Arrays.asList(roleIdTmpArray);
      if (!roleList.contains(String.valueOf(roleId))) {
        roleInfoStr = roleInfoStr + BackGroundUserDTO.ROLE_SPLIT + roleId;
      }
    }
    
    roleInfo.setRole(roleInfoStr);
    return Result.getSuccDataResult(pageRoleDOMapper.update(roleInfo) == 1);
  }

  // @Override
  // public Result<Boolean> createAndAddRole(String pageId, RoleDTO role) {
  // if(null == role){
  // return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
  // "parmeter is null");
  // }
  //
  // RoleDO roleDO = roleDOMapper.selectRoleById(role.getRoleId());
  // if(null == roleDO){
  // BeanCopierUtils.copyProperties(role, target);
  // }
  // return null;
  // }

  @Override
  public Result<Boolean> romoveRole(String pageId, Integer roleId) {
    PageRoleDO roleInfo = pageRoleDOMapper.select(pageId);
    if (null == roleInfo || StringUtils.isEmpty(roleInfo.getRole())) {
      return Result.getErrDataResult(ServerResultCode.ROLE_ERROR_CODE_NOT_EXIT,
          "page not set role,pageId:" + pageId);
    }

    String roleArray[] = roleInfo.getRole().split(BackGroundUserDTO.ROLE_SPLIT);
    List<Integer> roleList = Lists.newArrayList();
    for (String tmp : roleArray) {
      if (StringUtils.isNotEmpty(tmp) && StringUtils.isNumeric(tmp)) {
        roleList.add(Integer.valueOf(tmp));
      }
    }
    if (CollectionUtils.isEmpty(roleList)) {
      return Result.getErrDataResult(ServerResultCode.ROLE_ERROR_CODE_NOT_EXIT,
          "page not set role,pageId:" + pageId);
    }
    String newRoleInfo = "";
    for (Integer id : roleList) {
      if (!id.equals(roleId)) {
        newRoleInfo = newRoleInfo + id.intValue() + BackGroundUserDTO.ROLE_SPLIT;
      }
    }
    roleInfo.setRole(newRoleInfo);
    return Result.getSuccDataResult(pageRoleDOMapper.update(roleInfo) == 1);
  }
}
