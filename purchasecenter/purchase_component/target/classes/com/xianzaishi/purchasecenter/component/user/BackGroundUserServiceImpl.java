package com.xianzaishi.purchasecenter.component.user;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.query.BaseQuery;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.user.BackGroundUserService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.query.BackGroundUserQuery;
import com.xianzaishi.purchasecenter.dal.user.dao.UserDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dataobject.UserDO;

/**
 * 基础用户表
 * 
 * @author zhancang
 * 
 */
@Service("backgrounduserservice")
public class BackGroundUserServiceImpl implements BackGroundUserService {

  private static final Logger logger = Logger.getLogger(BackGroundUserServiceImpl.class);

  @Autowired
  private UserDOMapper userDOMapper;

  @Override
  public Result<Boolean> updateUserDTO(BackGroundUserDTO userDTO) {
    if (null == userDTO || userDTO.getUserId() == null || userDTO.getUserId() <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is null or userId is null");
    }
    UserDO userDO = new UserDO();
    BeanCopierUtils.copyProperties(userDTO, userDO);
    int count = userDOMapper.update(userDO);
    if (count != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update user db error");
    } else {
      return Result.getSuccDataResult(true);
    }
  }

  @Override
  public Result<BackGroundUserDTO> queryUserDTOByToken(String token) {
    if (StringUtils.isEmpty(token)) {
      logger.error("Queery token input parameter is null");
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error,token:" + token);
    }

    UserDO userDO = userDOMapper.selectByToken(token);
    if (null == userDO) {
      logger.error("Queery token input get db result is null");
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "query user is null,token:" + token);
    }

    BackGroundUserDTO backGroundUserDTO = new BackGroundUserDTO();
    BeanCopierUtils.copyProperties(userDO, backGroundUserDTO);
    logger.error("Queery token input get db result is null");
    return Result.getSuccDataResult(backGroundUserDTO);
  }

  @Override
  public Result<BackGroundUserDTO> queryUserDTOByName(String userName) {
    if (StringUtils.isEmpty(userName)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error,userName:" + userName);
    }

    UserDO userDO = userDOMapper.selectByName(userName);
    if (null == userDO) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "query user is null,userName:" + userName);
    }

    BackGroundUserDTO backGroundUserDTO = new BackGroundUserDTO();
    BeanCopierUtils.copyProperties(userDO, backGroundUserDTO);
    return Result.getSuccDataResult(backGroundUserDTO);
  }

  @Override
  public Result<BackGroundUserDTO> queryUserDTOById(Integer userId) {
    if (null == userId || userId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error,userId:" + userId);
    }

    UserDO userDO = userDOMapper.selectById(userId);
    if (null == userDO) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "query user is null,userId:" + userId);
    }

    BackGroundUserDTO backGroundUserDTO = new BackGroundUserDTO();
    BeanCopierUtils.copyProperties(userDO, backGroundUserDTO);
    return Result.getSuccDataResult(backGroundUserDTO);
  }

  @Override
  public Result<List<BackGroundUserDTO>> queryUserDTOByIdList(List<Integer> userIdList) {
    if (null == userIdList || CollectionUtils.isEmpty(userIdList)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error,userIdList is null");
    }

    List<UserDO> userDOList = userDOMapper.selectIdList(userIdList);
    if (null == userDOList || CollectionUtils.isEmpty(userDOList)) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "query user is empty");
    }

    List<BackGroundUserDTO> result = Lists.newArrayList();
    BeanCopierUtils.copyListBean(userDOList, result, BackGroundUserDTO.class);
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<BackGroundUserDTO> queryUserDTOByPhone(Long phone) {
    if (null == phone || phone <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query parameter is error,phone:" + phone);
    }

    UserDO userDO = userDOMapper.selectByPhone(phone);
    if (null == userDO) {
      return Result.getErrDataResult(ServerResultCode.BACKGROUDUSER_ERROR_CODE_NOT_EXIT,
          "query user is null,phone:" + phone);
    }

    BackGroundUserDTO backGroundUserDTO = new BackGroundUserDTO();
    BeanCopierUtils.copyProperties(userDO, backGroundUserDTO);
    return Result.getSuccDataResult(backGroundUserDTO);
  }

  @Override
  public PagedResult<List<BackGroundUserDTO>> queryUserDTOByQuery(BackGroundUserQuery query) {
    if (null == query) {
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query param is null");
    }
    Integer pageSize = query.getPageSize();
    Integer pageNum = query.getPageNum();
    if (null == pageSize) {
      pageSize = BaseQuery.DEFAULT_PAGE_SIZE;
    }
    if (null == pageNum) {
      pageNum = BaseQuery.DEFAULT_PAGE_NUM;
    }
    List<UserDO> userDOs = userDOMapper.selectByQuery(query);
    List<BackGroundUserDTO> backGroundUserDTOs = Lists.newArrayList();
    BeanCopierUtils.copyListBean(userDOs, backGroundUserDTOs, BackGroundUserDTO.class);
    Integer totalNum = userDOMapper.count(query);
    return PagedResult.getSuccDataResult(backGroundUserDTOs, totalNum,
        getTotalPage(totalNum, pageSize), pageSize, pageNum);
  }

  @Override
  public Result<Integer> queryUserDTOCountByQuery(BackGroundUserQuery query) {
    if (null == query) {
      return PagedResult.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "query param is null");
    }
    Integer count = userDOMapper.count(query);
    return Result.getSuccDataResult(count);
  }

  /**
   * 获取分页数量
   * 
   * @param number 商品数量
   * @param pageSize 分页大小
   * @return
   */
  private Integer getTotalPage(Integer number, Integer pageSize) {
    Integer totalPage = 1;
    if (number % pageSize != 0) {
      totalPage = (number / pageSize) + 1;
    } else {
      totalPage = number / pageSize;
    }
    return totalPage;
  }

}
