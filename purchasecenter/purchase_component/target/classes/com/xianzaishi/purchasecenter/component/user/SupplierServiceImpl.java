package com.xianzaishi.purchasecenter.component.user;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.beancopier.BeanCopierUtils;
import com.xianzaishi.itemcenter.common.pic.PicUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.itemcenter.common.result.ServerResultCode;
import com.xianzaishi.purchasecenter.client.user.SupplierService;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO;
import com.xianzaishi.purchasecenter.client.user.dto.BackGroundUserDTO.UserTypeConstants;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierCompanyDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierContactDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierFinanceDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierLicensesDTO;
import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierSkuDTO;
import com.xianzaishi.purchasecenter.client.user.query.SupplierQuery;
import com.xianzaishi.purchasecenter.dal.user.dao.UserDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.supplier.SupplierCompanyDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.supplier.SupplierContactDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.supplier.SupplierDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.supplier.SupplierFinanceDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.supplier.SupplierLicensesDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.supplier.SupplierSkuDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dataobject.UserDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierCompanyDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierContactDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierFinanceDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierLicensesDO;

@Service("supplierservice")
public class SupplierServiceImpl implements SupplierService {

  @Autowired
  private UserDOMapper userDOMapper;

  @Autowired
  private SupplierCompanyDOMapper supplierCompanyDOMapper;

  @Autowired
  private SupplierContactDOMapper supplierContactDOMapper;

  @Autowired
  private SupplierDOMapper supplierDOMapper;

  @Autowired
  private SupplierFinanceDOMapper supplierFinanceDOMapper;

  @Autowired
  private SupplierLicensesDOMapper supplierLicensesDOMapper;

  @Autowired
  private SupplierSkuDOMapper supplierSkuDOMapper;

  @Override
  public Result<Integer> insertSupplier(SupplierDTO supplierDTO) {
    if (null == supplierDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input supplierDTO is null");
    } else if (null == supplierDTO.getSupplierContactDTO()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input SupplierContactDTO is null");
    } else if (null == supplierDTO.getSupplierCompanyDTO()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input SupplierCompanyDTO is null");
    } else if (null == supplierDTO.getSupplierFinanceDTO()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input SupplierFinanceDTO is null");
    } else if (CollectionUtils.isEmpty(supplierDTO.getSupplierLicensesDTOList())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input SupplierLicensesDTO is null");
    }

    BackGroundUserDTO baseUserDTO = supplierDTO;
    UserDO user = new UserDO();
    BeanCopierUtils.copyProperties(baseUserDTO, user);

    int insertCount = userDOMapper.insert(user);
    if (insertCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert user db error");
    }

    int supplierId = user.getUserId();

    SupplierCompanyDO companyDO = new SupplierCompanyDO();
    BeanCopierUtils.copyProperties(supplierDTO.getSupplierCompanyDTO(), companyDO);
    companyDO.setSupplierId(supplierId);
    int companyCount = supplierCompanyDOMapper.insert(companyDO);
    if (companyCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "insert suppliercompany db error");
    }

    SupplierContactDO contactDO = new SupplierContactDO();
    BeanCopierUtils.copyProperties(supplierDTO.getSupplierContactDTO(), contactDO);
    contactDO.setSupplierId(supplierId);
    int contactCount = supplierContactDOMapper.insert(contactDO);
    if (contactCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "insert suppliercontact db error");
    }

    SupplierFinanceDO financeDO = new SupplierFinanceDO();
    BeanCopierUtils.copyProperties(supplierDTO.getSupplierFinanceDTO(), financeDO);
    financeDO.setSupplierId(supplierId);
    int financeCount = supplierFinanceDOMapper.insert(financeDO);
    if (financeCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "insert supplierfinance db error");
    }

    List<SupplierLicensesDO> licenseList = Lists.newArrayList();
    BeanCopierUtils.copyListBean(supplierDTO.getSupplierLicensesDTOList(), licenseList,
        SupplierLicensesDO.class);
    for (int i = 0; i < licenseList.size(); i++) {
      SupplierLicensesDO licensesDO = licenseList.get(i);
      licensesDO.setSupplierId(supplierId);
      licensesDO.setLicensesPic(PicUtil.joinPicStr(supplierDTO.getSupplierLicensesDTOList().get(i)
          .getLicensesPicList()));
    }
    int licensesDOCount = supplierLicensesDOMapper.insertBatch(licenseList);
    if (licensesDOCount != licenseList.size()) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "insert supplierlicenses db error");
    }

    SupplierDO supplierDO = new SupplierDO();
    BeanCopierUtils.copyProperties(supplierDTO, supplierDO);
    supplierDO.setUserId(supplierId);
    int supplierCount = supplierDOMapper.insert(supplierDO);
    if (supplierCount != 1) {
      return Result
          .getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "insert supplierdo db error");
    }
    return Result.getSuccDataResult(supplierId);
  }

  @Override
  public Result<Boolean> updateSupplier(SupplierDTO supplierDTO) {
    if (null == supplierDTO) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input supplierDTO is null");
    } else if (null == supplierDTO.getSupplierContactDTO()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input SupplierContactDTO is null");
    } else if (null == supplierDTO.getSupplierCompanyDTO()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input SupplierCompanyDTO is null");
    } else if (null == supplierDTO.getSupplierFinanceDTO()) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input SupplierFinanceDTO is null");
    } else if (CollectionUtils.isEmpty(supplierDTO.getSupplierLicensesDTOList())) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input SupplierLicensesDTO is null");
    }

    BackGroundUserDTO baseUserDTO = supplierDTO;
    UserDO user = new UserDO();
    BeanCopierUtils.copyProperties(baseUserDTO, user);
    int updateCount = userDOMapper.update(user);
    if (updateCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update user db error");
    }

    SupplierCompanyDO companyDO = new SupplierCompanyDO();
    BeanCopierUtils.copyProperties(supplierDTO.getSupplierCompanyDTO(), companyDO);
    int companyCount = supplierCompanyDOMapper.update(companyDO);
    if (companyCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "update suppliercompany db error");
    }

    SupplierContactDO contactDO = new SupplierContactDO();
    BeanCopierUtils.copyProperties(supplierDTO.getSupplierContactDTO(), contactDO);
    int contactCount = supplierContactDOMapper.update(contactDO);
    if (contactCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "update suppliercontact db error");
    }

    SupplierFinanceDO financeDO = new SupplierFinanceDO();
    BeanCopierUtils.copyProperties(supplierDTO.getSupplierFinanceDTO(), financeDO);
    int financeCount = supplierFinanceDOMapper.update(financeDO);
    if (financeCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "update supplierfinance db error");
    }

    List<SupplierLicensesDO> licenseList = Lists.newArrayList();
    BeanCopierUtils.copyListBean(supplierDTO.getSupplierLicensesDTOList(), licenseList,
        SupplierLicensesDO.class);
    
    for (int i = 0; i < licenseList.size(); i++) {
      SupplierLicensesDO licensesDO = licenseList.get(i);
      licensesDO.setLicensesPic(PicUtil.joinPicStr(supplierDTO.getSupplierLicensesDTOList().get(i)
          .getLicensesPicList()));
    }
    
    int licensesDOCount = supplierLicensesDOMapper.updateBatch(licenseList);
    if (licensesDOCount != 1) {
      return Result.getErrDataResult(ServerResultCode.SERVER_DB_ERROR,
          "insert supplierlicenses db error");
    }

    SupplierDO supplierDO = new SupplierDO();
    BeanCopierUtils.copyProperties(supplierDTO, supplierDO);
    int supplierCount = supplierDOMapper.update(supplierDO);
    if (supplierCount != 1) {
      return Result
          .getErrDataResult(ServerResultCode.SERVER_DB_ERROR, "update supplierdo db error");
    }
    return Result.getSuccDataResult(true);
  }

  @Override
  public Result<List<SupplierDTO>> querySupplier(SupplierQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input query is null");
    }
    List<SupplierDO> tmpSupplierDOResult = supplierDOMapper.select(query);
    if (CollectionUtils.isEmpty(tmpSupplierDOResult)) {
      return Result.getErrDataResult(ServerResultCode.SUPPLIER_ERROR_CODE_NOT_EXIT,
          "supplier not exit with this query:" + JackSonUtil.getJson(query));
    }

    List<SupplierDTO> result = Lists.newArrayList();
    BeanCopierUtils.copyListBean(tmpSupplierDOResult, result, SupplierDTO.class);

    List<Integer> supplierIds = Lists.newArrayList();
    Map<Integer, SupplierDTO> supplierDOM = Maps.newHashMap();
    for (SupplierDTO supplier : result) {
      supplierIds.add(supplier.getUserId());
      supplierDOM.put(supplier.getUserId(), supplier);
      supplier.setUserType(UserTypeConstants.USER_SUPPLIER);
    }
    
    // 取基础用户信息
    List<UserDO> userList = userDOMapper.selectIdList(supplierIds);
    for(UserDO tmpUser:userList){
      SupplierDTO emp = supplierDOM.get(tmpUser.getUserId());
      if(null != emp){
        BeanCopierUtils.copyProperties(tmpUser, emp);
      }
    }

    // 取公司信息
    if (query.getIncludeCompanyInfo()) {
      List<SupplierCompanyDO> companyList = supplierCompanyDOMapper.selectIdList(supplierIds);
      if (CollectionUtils.isNotEmpty(companyList)) {
        List<SupplierCompanyDTO> tmpResult = Lists.newArrayList();
        BeanCopierUtils.copyListBean(companyList, tmpResult, SupplierCompanyDTO.class);
        for (SupplierCompanyDTO company : tmpResult) {
          int supplierId = company.getSupplierId();
          SupplierDTO supplier = supplierDOM.get(supplierId);
          if (null != supplier) {
            supplier.setSupplierCompanyDTO(company);
          }
        }
      }
    }

    // 取联系人信息
    if (query.getIncludeContactInfo()) {
      List<SupplierContactDO> contactList = supplierContactDOMapper.selectIdList(supplierIds);
      if (CollectionUtils.isNotEmpty(contactList)) {
        List<SupplierContactDTO> tmpResult = Lists.newArrayList();
        BeanCopierUtils.copyListBean(contactList, tmpResult, SupplierContactDTO.class);
        for (SupplierContactDTO contact : tmpResult) {
          int supplierId = contact.getSupplierId();
          SupplierDTO supplier = supplierDOM.get(supplierId);
          if (null != supplier) {
            supplier.setSupplierContactDTO(contact);
          }
        }
      }
    }

    // 取财务信息
    if (query.getIncludeFinanceInfo()) {
      List<SupplierFinanceDO> financeList = supplierFinanceDOMapper.selectIdList(supplierIds);
      if (CollectionUtils.isNotEmpty(financeList)) {
        List<SupplierFinanceDTO> tmpResult = Lists.newArrayList();
        BeanCopierUtils.copyListBean(financeList, tmpResult, SupplierFinanceDTO.class);
        for (SupplierFinanceDTO finance : tmpResult) {
          int supplierId = finance.getSupplierId();
          SupplierDTO supplier = supplierDOM.get(supplierId);
          if (null != supplier) {
            supplier.setSupplierFinanceDTO(finance);
          }
        }
      }
    }

    // 取各种资质信息
    if (query.getIncludeLicensesInfo()) {
      List<SupplierLicensesDO> licensesList = supplierLicensesDOMapper.selectIdList(supplierIds);
      if (CollectionUtils.isNotEmpty(licensesList)) {
        List<SupplierLicensesDTO> tmpResult = Lists.newArrayList();
        BeanCopierUtils.copyListBean(licensesList, tmpResult, SupplierLicensesDTO.class);

        // 按照不同供应商分组
        Map<Integer, List<SupplierLicensesDTO>> licenseListMap = Maps.newHashMap();
        for (int i=0;i<tmpResult.size();i++) {
          SupplierLicensesDTO finance = tmpResult.get(i);
          finance.setLicensesPicList(PicUtil.splitPicStr(licensesList.get(i).getLicensesPic()));
          
          int supplierId = finance.getSupplierId();
          List<SupplierLicensesDTO> tmpList = licenseListMap.get(supplierId);
          if (CollectionUtils.isEmpty(tmpList)) {
            tmpList = Lists.newArrayList();
            tmpList.add(finance);
            licenseListMap.put(supplierId, tmpList);
          } else {
            tmpList.add(finance);
          }
        }

        // 按照不同供应商设置
        for (Integer supplierId : licenseListMap.keySet()) {
          SupplierDTO supplier = supplierDOM.get(supplierId);
          if (null != supplier) {
            supplier.setSupplierLicensesDTOList(licenseListMap.get(supplierId));
          }
        }
      }
    }

    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<SupplierDTO> querySupplierById(int userId, boolean includeAll) {
    if (userId <= 0) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input userId is 0");
    }

    SupplierDTO result = getSupplierInfo(userId);
    if (null == result) {
      return Result.getErrDataResult(ServerResultCode.SUPPLIER_ERROR_CODE_NOT_EXIT,
          "supplier not exit,userId：" + userId);
    }
    
    if (includeAll) {
      UserDO user = userDOMapper.selectById(userId);
      BeanCopierUtils.copyProperties(user, result);
      
      result.setSupplierCompanyDTO(this.getCompanyInfo(userId));
      result.setSupplierContactDTO(this.getContactInfo(userId));
      result.setSupplierFinanceDTO(this.getFinanceInfo(userId));
      result.setSupplierLicensesDTOList(this.getLicensesInfo(userId));
    }
    return Result.getSuccDataResult(result);
  }

  @Override
  public Result<SupplierDTO> querySupplierByName(String name, boolean includeAll) {
    if (StringUtils.isEmpty(name)) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input name is null");
    }

    SupplierDO supplier = supplierDOMapper.selectByName(name);
    if (null == supplier) {
      return Result.getErrDataResult(ServerResultCode.SUPPLIER_ERROR_CODE_NOT_EXIT,
          "supplier not exit,name：" + name);
    }

    SupplierDTO result = new SupplierDTO();
    BeanCopierUtils.copyProperties(supplier, result);
    result.setUserType(UserTypeConstants.USER_SUPPLIER);
    int userId = result.getUserId();

    if (includeAll) {
      UserDO user = userDOMapper.selectById(userId);
      BeanCopierUtils.copyProperties(user, result);
      
      result.setSupplierCompanyDTO(this.getCompanyInfo(userId));
      result.setSupplierContactDTO(this.getContactInfo(userId));
      result.setSupplierFinanceDTO(this.getFinanceInfo(userId));
      result.setSupplierLicensesDTOList(this.getLicensesInfo(userId));
    }
    return Result.getSuccDataResult(result);
  }

  /**
   * 查询基本信息
   * 
   * @param supplier
   */
  private SupplierDTO getSupplierInfo(int supplierId) {
    if (supplierId > 0) {
      SupplierDO supplier = supplierDOMapper.selectById(supplierId);
      if (null != supplier) {
        SupplierDTO result = new SupplierDTO();
        BeanCopierUtils.copyProperties(supplier, result);
        result.setUserType(UserTypeConstants.USER_SUPPLIER);
        return result;
      }
    }
    return null;
  }

  /**
   * 查询公司信息
   * 
   * @param supplier
   */
  private SupplierCompanyDTO getCompanyInfo(int supplierId) {
    if (supplierId > 0) {
      SupplierCompanyDO company = supplierCompanyDOMapper.select(supplierId);
      if (null != company) {
        SupplierCompanyDTO result = new SupplierCompanyDTO();
        BeanCopierUtils.copyProperties(company, result);
        return result;
      }
    }
    return null;
  }

  /**
   * 查询联系人信息
   * 
   * @param supplier
   */
  private SupplierContactDTO getContactInfo(int supplierId) {
    if (supplierId > 0) {
      SupplierContactDO contact = supplierContactDOMapper.select(supplierId);
      if (null != contact) {
        SupplierContactDTO result = new SupplierContactDTO();
        BeanCopierUtils.copyProperties(contact, result);
        return result;
      }
    }
    return null;
  }

  /**
   * 查询财务信息
   * 
   * @param supplier
   */
  private SupplierFinanceDTO getFinanceInfo(int supplierId) {
    if (supplierId > 0) {
      SupplierFinanceDO finance = supplierFinanceDOMapper.select(supplierId);
      if (null != finance) {
        SupplierFinanceDTO result = new SupplierFinanceDTO();
        BeanCopierUtils.copyProperties(finance, result);
        return result;
      }
    }
    return null;
  }

  /**
   * 查询资质信息信息
   * 
   * @param supplier
   */
  private List<SupplierLicensesDTO> getLicensesInfo(int supplierId) {
    if (supplierId > 0) {
      List<SupplierLicensesDO> license = supplierLicensesDOMapper.select(supplierId);
      if (null != license) {
        List<SupplierLicensesDTO> result = Lists.newArrayList();
        BeanCopierUtils.copyListBean(license, result, SupplierLicensesDTO.class);
        for(int i=0;i<result.size();i++){
          SupplierLicensesDTO tmp = result.get(i);
          tmp.setLicensesPicList(PicUtil.splitPicStr(license.get(i).getLicensesPic()));
        }
        return result;
      }
    }
    return null;
  }
  
  
  @Override
  public Result<Integer> querySupplierCount(SupplierQuery query) {
    if (null == query) {
      return Result.getErrDataResult(ServerResultCode.ERROR_CODE_PARAMETER_ERROR,
          "Input query is null");
    }
    Integer count = supplierCompanyDOMapper.count(query);
    return Result.getSuccDataResult(count);
  }

  @Override
  public Result<Boolean> insertSupplierSkuRelation(SupplierSkuDTO supplierSkuDTO) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Result<Boolean> updateSupplierSkuRelation(SupplierSkuDTO supplierSkuDTO) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Result<SupplierSkuDTO> querySupplierSkuRelation(int supplierId, long skuId) {
    // TODO Auto-generated method stub
    return null;
  }

  @Override
  public Result<List<SupplierDTO>> querySkuSupplierList(long skuId) {
    // TODO Auto-generated method stub
    return null;
  }
}
