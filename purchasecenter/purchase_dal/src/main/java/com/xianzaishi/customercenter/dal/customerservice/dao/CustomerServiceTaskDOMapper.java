package com.xianzaishi.customercenter.dal.customerservice.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.customercenter.client.customerservice.query.CustomerserviceQuery;
import com.xianzaishi.customercenter.dal.customerservice.dataobject.CustomerServiceTaskDO;

public interface CustomerServiceTaskDOMapper {

  int insert(CustomerServiceTaskDO customerServiceTask);

  int update(CustomerServiceTaskDO customerServiceTask);

  CustomerServiceTaskDO selectById(Long id);

  List<CustomerServiceTaskDO> select(@Param("query")CustomerserviceQuery query);

  int count(@Param("query")CustomerserviceQuery query);
}
