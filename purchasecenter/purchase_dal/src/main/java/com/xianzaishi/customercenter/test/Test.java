package com.xianzaishi.customercenter.test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.xianzaishi.purchasecenter.dal.purchase.dao.PurchaseSubOrderDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.EmployeeDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.UserDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dao.supplier.SupplierDOMapper;
import com.xianzaishi.purchasecenter.dal.user.dataobject.EmployeeDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.UserDO;
import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierDO;

public class Test {

	public static void main(String[] args) throws IOException {
		//读取配置文件
				//全局配置文件的路径
				String resource = "SqlMapConfig.xml";
		InputStream inputStream = Resources.getResourceAsStream(resource);
		System.out.println("inputStream"+inputStream.available());
				
				//创建SqlSessionFactory
				SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
				
				//创建SqlSession
				SqlSession sqlSession = sqlSessionFactory.openSession();
				  List<Integer>  arratList=new ArrayList<>();
				  arratList.add(60);
				  arratList.add(61);
				  arratList.add(62);
				//调用SqlSession的增删改查方法
				//第一个参数：表示statement的唯一标示
				  SupplierDOMapper mapper = sqlSession.getMapper(SupplierDOMapper.class);
				  List<SupplierDO> selectIdList = mapper.selectIdList(arratList);
				System.out.println(selectIdList.size());
				  //关闭资源
				sqlSession.close();
	}

}
