package com.xianzaishi.purchasecenter.dal.bill.dataobject;

import com.xianzaishi.purchasecenter.client.user.dto.UserBillDTO;

/**
 * 
 * @author quyang 2017-2-13 11:53:16
 *
 */
public class BillDO extends UserBillDTO {

  /**
   * 
   */
  private static final long serialVersionUID = 3803046973000426097L;

  public static long getSerialVersionUID() {
    return serialVersionUID;
  }


  private String billMoney;

  private Long billMoneyLong;

  public Long getBillMoneyLong() {
    return billMoneyLong;
  }

  public void setBillMoneyLong(Long billMoneyLong) {
    this.billMoneyLong = billMoneyLong;
  }

  public String getBillMoney() {
    return billMoney;
  }

  public void setBillMoney(String billMoney) {
    this.billMoney = billMoney;
  }
}