package com.xianzaishi.purchasecenter.dal.equipment.dataobject;

import java.util.Date;

public class EquipmentDO {
  /**
   * This field was generated by MyBatis Generator. This field corresponds to the database column
   * equipment.equipment_id
   * 
   * @mbggenerated
   */
  private Integer equipmentId;

  /**
   * This field was generated by MyBatis Generator. This field corresponds to the database column
   * equipment.name
   * 
   * @mbggenerated
   */
  private String name;

  /**
   * This field was generated by MyBatis Generator. This field corresponds to the database column
   * equipment.status
   * 
   * @mbggenerated
   */
  private Short status;

  /**
   * This field was generated by MyBatis Generator. This field corresponds to the database column
   * equipment.introduction
   * 
   * @mbggenerated
   */
  private String introduction;

  /**
   * This field was generated by MyBatis Generator. This field corresponds to the database column
   * equipment.gmt_create
   * 
   * @mbggenerated
   */
  private Date gmtCreate;

  /**
   * This field was generated by MyBatis Generator. This field corresponds to the database column
   * equipment.gmt_modified
   * 
   * @mbggenerated
   */
  private Date gmtModified;

  /**
   * This field was generated by MyBatis Generator. This field corresponds to the database column
   * equipment.cost
   * 
   * @mbggenerated
   */
  private Integer cost;

  /**
   * This field was generated by MyBatis Generator. This field corresponds to the database column
   * equipment.equipment_code
   * 
   * @mbggenerated
   */
  private String equipmentCode;

  /**
   * This method was generated by MyBatis Generator. This method returns the value of the database
   * column equipment.equipment_id
   * 
   * @return the value of equipment.equipment_id
   * 
   * @mbggenerated
   */
  public Integer getEquipmentId() {
    return equipmentId;
  }

  /**
   * This method was generated by MyBatis Generator. This method sets the value of the database
   * column equipment.equipment_id
   * 
   * @param equipmentId the value for equipment.equipment_id
   * 
   * @mbggenerated
   */
  public void setEquipmentId(Integer equipmentId) {
    this.equipmentId = equipmentId;
  }

  /**
   * This method was generated by MyBatis Generator. This method returns the value of the database
   * column equipment.name
   * 
   * @return the value of equipment.name
   * 
   * @mbggenerated
   */
  public String getName() {
    return name;
  }

  /**
   * This method was generated by MyBatis Generator. This method sets the value of the database
   * column equipment.name
   * 
   * @param name the value for equipment.name
   * 
   * @mbggenerated
   */
  public void setName(String name) {
    this.name = name == null ? null : name.trim();
  }

  /**
   * This method was generated by MyBatis Generator. This method returns the value of the database
   * column equipment.status
   * 
   * @return the value of equipment.status
   * 
   * @mbggenerated
   */
  public Short getStatus() {
    return status;
  }

  /**
   * This method was generated by MyBatis Generator. This method sets the value of the database
   * column equipment.status
   * 
   * @param status the value for equipment.status
   * 
   * @mbggenerated
   */
  public void setStatus(Short status) {
    this.status = status;
  }

  /**
   * This method was generated by MyBatis Generator. This method returns the value of the database
   * column equipment.introduction
   * 
   * @return the value of equipment.introduction
   * 
   * @mbggenerated
   */
  public String getIntroduction() {
    return introduction;
  }

  /**
   * This method was generated by MyBatis Generator. This method sets the value of the database
   * column equipment.introduction
   * 
   * @param introduction the value for equipment.introduction
   * 
   * @mbggenerated
   */
  public void setIntroduction(String introduction) {
    this.introduction = introduction == null ? null : introduction.trim();
  }

  /**
   * This method was generated by MyBatis Generator. This method returns the value of the database
   * column equipment.gmt_create
   * 
   * @return the value of equipment.gmt_create
   * 
   * @mbggenerated
   */
  public Date getGmtCreate() {
    return gmtCreate;
  }

  /**
   * This method was generated by MyBatis Generator. This method sets the value of the database
   * column equipment.gmt_create
   * 
   * @param gmtCreate the value for equipment.gmt_create
   * 
   * @mbggenerated
   */
  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  /**
   * This method was generated by MyBatis Generator. This method returns the value of the database
   * column equipment.gmt_modified
   * 
   * @return the value of equipment.gmt_modified
   * 
   * @mbggenerated
   */
  public Date getGmtModified() {
    return gmtModified;
  }

  /**
   * This method was generated by MyBatis Generator. This method sets the value of the database
   * column equipment.gmt_modified
   * 
   * @param gmtModified the value for equipment.gmt_modified
   * 
   * @mbggenerated
   */
  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }

  /**
   * This method was generated by MyBatis Generator. This method returns the value of the database
   * column equipment.cost
   * 
   * @return the value of equipment.cost
   * 
   * @mbggenerated
   */
  public Integer getCost() {
    return cost;
  }

  /**
   * This method was generated by MyBatis Generator. This method sets the value of the database
   * column equipment.cost
   * 
   * @param cost the value for equipment.cost
   * 
   * @mbggenerated
   */
  public void setCost(Integer cost) {
    this.cost = cost;
  }

  /**
   * This method was generated by MyBatis Generator. This method returns the value of the database
   * column equipment.equipment_code
   * 
   * @return the value of equipment.equipment_code
   * 
   * @mbggenerated
   */
  public String getEquipmentCode() {
    return equipmentCode;
  }

  /**
   * This method was generated by MyBatis Generator. This method sets the value of the database
   * column equipment.equipment_code
   * 
   * @param equipmentCode the value for equipment.equipment_code
   * 
   * @mbggenerated
   */
  public void setEquipmentCode(String equipmentCode) {
    this.equipmentCode = equipmentCode == null ? null : equipmentCode.trim();
  }
}
