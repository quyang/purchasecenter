package com.xianzaishi.purchasecenter.dal.logmonitor.dataobject;

import java.util.Date;

public class LogMonitorDO {

  /**
   * 主键id
   */
  private Integer id;

  private String name;

  private String info;

  private Date gmtCreate;

  private Date gmtModified;

  /**
   * 监控事件类型
   */
  private Short type;

  /**
   * 数据状态 1 表示最新   0 表示旧数据
   */
  private Short status;

  public Short getStatus() {
    return status;
  }

  public void setStatus(Short status) {
    this.status = status;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }


  public Short getType() {
    return type;
  }

  public void setType(Short type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }


  public void setName(String name) {
    this.name = name == null ? null : name.trim();
  }


  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info == null ? null : info.trim();
  }

  public Date getGmtCreate() {
    return gmtCreate;
  }

  public void setGmtCreate(Date gmtCreate) {
    this.gmtCreate = gmtCreate;
  }

  public Date getGmtModified() {
    return gmtModified;
  }

  public void setGmtModified(Date gmtModified) {
    this.gmtModified = gmtModified;
  }
}