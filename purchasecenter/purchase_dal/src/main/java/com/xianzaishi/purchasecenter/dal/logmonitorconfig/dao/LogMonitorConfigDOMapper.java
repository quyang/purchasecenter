package com.xianzaishi.purchasecenter.dal.logmonitorconfig.dao;

import com.xianzaishi.purchasecenter.dal.logmonitorconfig.dataobject.LogMonitorConfigDO;
import java.util.List;

/**
 * quyang
 */
public interface LogMonitorConfigDOMapper {


    /**
     * 查询监控配置信息
     * @param LogMonitorConfigDO
     * @return
     */
    List<LogMonitorConfigDO> select(LogMonitorConfigDO LogMonitorConfigDO);


    /**
     * 更新配置信息
     * @param logMonitorConfigDO
     * @return
     */
    Integer update(LogMonitorConfigDO logMonitorConfigDO);


    /**
     * 插入配置信息
     * @param logMonitorConfigDO
     * @return
     */
    Integer insert(LogMonitorConfigDO logMonitorConfigDO);

}