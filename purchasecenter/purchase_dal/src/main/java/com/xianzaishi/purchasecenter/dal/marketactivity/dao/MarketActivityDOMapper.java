package com.xianzaishi.purchasecenter.dal.marketactivity.dao;

import com.xianzaishi.purchasecenter.client.marketactivity.query.MarketActivityQuery;
import com.xianzaishi.purchasecenter.dal.marketactivity.dateobject.MarketActivityDO;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * Created by quyang on 2017/4/14.
 */
public interface MarketActivityDOMapper {

  Integer insert( MarketActivityDO marketActivityDO);

  Integer update(MarketActivityDO marketActivityDO);

  List<MarketActivityDO> query(@Param("query") MarketActivityQuery marketActivityQuery);

  Integer delete(Integer id);

  Integer queryCount(@Param("query")MarketActivityQuery marketActivityQuery);

  List<MarketActivityDO> queryAll(MarketActivityQuery marketActivityQuery);
}
