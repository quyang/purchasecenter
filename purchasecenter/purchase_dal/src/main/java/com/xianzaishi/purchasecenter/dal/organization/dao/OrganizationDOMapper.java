package com.xianzaishi.purchasecenter.dal.organization.dao;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.dal.organization.dataobject.OrganizationDO;

public interface OrganizationDOMapper {

  public OrganizationDO select(@Param("organizationId") Integer organizationId);

}
