package com.xianzaishi.purchasecenter.dal.purchasereturn.dataobject;

import com.xianzaishi.purchasecenter.client.purchasereturn.dto.PurchaseReturnOrderDTO;

public class PurchaseReturnOrderDO extends PurchaseReturnOrderDTO {

  private static final long serialVersionUID = 1L;

  /**
   * 合并参数 TODO
   */
  private String checkinfo;

  public String getCheckinfo() {
    return checkinfo;
  }

  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo == null ? null : checkinfo.trim();
  }
}
