package com.xianzaishi.purchasecenter.dal.role.dao;

import com.xianzaishi.purchasecenter.dal.role.dataobject.PageRoleDO;

public interface PageRoleDOMapper {

  int insert(PageRoleDO record);
  
  PageRoleDO select(String url);
  
  int update(PageRoleDO record);

}
