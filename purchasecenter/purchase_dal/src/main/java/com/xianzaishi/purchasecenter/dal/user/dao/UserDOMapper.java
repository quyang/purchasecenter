package com.xianzaishi.purchasecenter.dal.user.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.client.user.query.BackGroundUserQuery;
import com.xianzaishi.purchasecenter.dal.user.dataobject.UserDO;

public interface UserDOMapper {

  int insert(UserDO userDO);

  UserDO selectById(@Param("userId") Integer userId);

  UserDO selectByName(@Param("name") String name);

  UserDO selectByToken(@Param("token") String token);

  UserDO selectByPhone(@Param("phone") Long phone);
  
  List<UserDO> selectIdList(@Param("userIdList") List<Integer> userIdList);
  
  /**
   * 根据查询条件查询用户信息
   * @param query
   * @return
   */
  List<UserDO> selectByQuery(@Param("query") BackGroundUserQuery query);
  
  int update(UserDO userDO);

  /**
   * 根据查询条件查询用户数量
   * @param query
   * @return
   */
  int count(@Param("query") BackGroundUserQuery query);
}
