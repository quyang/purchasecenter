package com.xianzaishi.purchasecenter.dal.user.dao.supplier;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierContactDO;

public interface SupplierContactDOMapper {

  int insert(SupplierContactDO supplierContactDO);

  SupplierContactDO select(@Param("supplierId") Integer supplierId);

  List<SupplierContactDO> selectIdList(@Param("supplierIdList") List<Integer> supplierId);
  
  int update(SupplierContactDO supplierContactDO);
}
