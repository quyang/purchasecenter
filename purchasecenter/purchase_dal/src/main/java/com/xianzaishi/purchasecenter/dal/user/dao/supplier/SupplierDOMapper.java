package com.xianzaishi.purchasecenter.dal.user.dao.supplier;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.client.user.query.SupplierQuery;
import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierDO;

public interface SupplierDOMapper {

  int insert(SupplierDO supplierDO);

  List<SupplierDO> selectIdList(@Param("supplierIdList") List<Integer> supplierId);
  
  SupplierDO selectById(@Param("userId") Integer userId);

  SupplierDO selectByName(@Param("name") String name);
  
  List<SupplierDO> select(@Param("query") SupplierQuery query);

  int update(SupplierDO supplierDO);

}
