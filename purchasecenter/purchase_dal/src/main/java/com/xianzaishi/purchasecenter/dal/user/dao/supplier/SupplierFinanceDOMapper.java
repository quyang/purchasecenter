package com.xianzaishi.purchasecenter.dal.user.dao.supplier;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierFinanceDO;

public interface SupplierFinanceDOMapper {

  int insert(SupplierFinanceDO supplierFinanceDO);

  SupplierFinanceDO select(@Param("supplierId") Integer supplierId);

  List<SupplierFinanceDO> selectIdList(@Param("supplierIdList") List<Integer> supplierId);
  
  int update(SupplierFinanceDO supplierFinanceDO);
}
