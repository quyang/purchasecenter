package com.xianzaishi.purchasecenter.dal.user.dataobject.supplier;

import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierLicensesDTO;

public class SupplierLicensesDO extends SupplierLicensesDTO{
  
  private static final long serialVersionUID = 1L;

  private String licensesPic;
  
  private String attribute;
  
  public String getLicensesPic() {
    return licensesPic;
  }

  public void setLicensesPic(String licensesPic) {
    this.licensesPic = licensesPic;
  }

  public String getAttribute() {
    return attribute;
  }

  public void setAttribute(String attribute) {
    this.attribute = attribute;
  }
}