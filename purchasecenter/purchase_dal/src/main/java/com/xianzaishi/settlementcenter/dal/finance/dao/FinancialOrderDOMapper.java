package com.xianzaishi.settlementcenter.dal.finance.dao;

import com.xianzaishi.settlementcenter.dal.finance.dataobject.FinancialOrderDO;

public interface FinancialOrderDOMapper {
  int deleteByPrimaryKey(Integer financialId);

  int insert(FinancialOrderDO record);

  int insertSelective(FinancialOrderDO record);

  FinancialOrderDO selectByPrimaryKey(Integer financialId);

  int updateByPrimaryKeySelective(FinancialOrderDO record);

  int updateByPrimaryKey(FinancialOrderDO record);
}
