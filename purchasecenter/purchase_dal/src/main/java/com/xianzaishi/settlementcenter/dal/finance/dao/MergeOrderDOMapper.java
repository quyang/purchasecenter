package com.xianzaishi.settlementcenter.dal.finance.dao;

import com.xianzaishi.settlementcenter.dal.finance.dataobject.MergeOrderDO;

public interface MergeOrderDOMapper {
  int deleteByPrimaryKey(Integer mergeId);

  int insert(MergeOrderDO record);

  int insertSelective(MergeOrderDO record);

  MergeOrderDO selectByPrimaryKey(Integer mergeId);

  int updateByPrimaryKeySelective(MergeOrderDO record);

  int updateByPrimaryKey(MergeOrderDO record);
}
