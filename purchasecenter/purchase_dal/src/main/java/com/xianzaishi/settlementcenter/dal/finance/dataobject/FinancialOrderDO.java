package com.xianzaishi.settlementcenter.dal.finance.dataobject;

import com.xianzaishi.settlement.client.finance.dto.FinancialOrderDTO;

/**
 * 财务流水记录
 * 
 * @author zhancang
 *
 */
public class FinancialOrderDO extends FinancialOrderDTO{
  
  private static final long serialVersionUID = 1L;

}
