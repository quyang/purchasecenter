package com.xianzaishi.settlementcenter.dal.finance.dataobject;

import com.xianzaishi.settlement.client.finance.dto.MergeOrderDTO;

public class MergeOrderDO extends MergeOrderDTO{

  private static final long serialVersionUID = 1L;
  
  //TODO 合并财务合并源交易记录和目标交易记录金额快照
  private String attribute;

  public String getAttribute() {
    return attribute;
  }

  public void setAttribute(String attribute) {
    this.attribute = attribute;
  }
}
