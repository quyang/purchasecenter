package com.xianzaishi.customercenter.dal.customerservice.dataobject;

import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO;

public class CustomerServiceTaskDO extends CustomerServiceTaskDTO{

  private static final long serialVersionUID = 1L;

  private String customerRequire;

  private String serviceResponse;

  private String customerProof;
  
  public String getCustomerRequire() {
    return customerRequire;
  }

  public void setCustomerRequire(String customerRequire) {
    this.customerRequire = customerRequire;
  }

  public String getServiceResponse() {
    return serviceResponse;
  }

  public void setServiceResponse(String serviceResponse) {
    this.serviceResponse = serviceResponse;
  }

  public String getCustomerProof() {
    return customerProof;
  }

  public void setCustomerProof(String customerProof) {
    this.customerProof = customerProof;
  }
  
}
