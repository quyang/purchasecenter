package com.xianzaishi.dumpcenter.dal.itemsalecount.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.dumpcenter.client.itemsalecount.query.ItemSaleCountQuery;
import com.xianzaishi.dumpcenter.dal.itemsalecount.dataobject.ItemSaleCountDO;

public interface ItemSaleCountDOMapper {
  
  List<ItemSaleCountDO> selectItemSaleCountList(@Param("query") ItemSaleCountQuery query);
}