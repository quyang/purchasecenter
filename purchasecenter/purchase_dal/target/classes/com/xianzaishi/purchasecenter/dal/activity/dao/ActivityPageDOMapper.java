package com.xianzaishi.purchasecenter.dal.activity.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.itemcenter.common.query.BaseQuery;
import com.xianzaishi.purchasecenter.dal.activity.dataobject.ActivityPageDO;

public interface ActivityPageDOMapper {
  Integer setMaxPageId();
  
  Integer setMaxSortId();
  
  int insert(ActivityPageDO record);
  
  List<ActivityPageDO> select(@Param("pageId")Integer pageId, @Param("stepIdList")List<Integer> stepIdList);
  
  List<ActivityPageDO> selectall();
      
  int update(ActivityPageDO record);
  
  /**
   * 分页查询
   * @param query
   * @return
   */
  List<ActivityPageDO> selectByPaging(@Param("query")BaseQuery query);
  
  /**
   * 查询活动页数量
   * @return
   */
  int selectCount();
  
  /**
   * 根据楼层id删除数据
   * @param stepId
   * @return
   */
  int deleteByStepId(Integer stepId);
  
}
