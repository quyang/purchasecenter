package com.xianzaishi.purchasecenter.dal.activity.dataobject;

import com.xianzaishi.purchasecenter.client.activity.dto.ActivityPageDTO;

public class ActivityPageDO extends ActivityPageDTO{
 
  /**
   * 楼层id
   */
  private Integer stepId;
  
  /**
   * 楼层数据合并
   */
  private String stepData;

  /**
   * 楼层排序
   */
  private Integer stepSort;

  /**
   * 楼层类型
   */
  private Integer stepType;

  /**
   * 楼层数据备份
   */
  private String stepDataBk;

  public Integer getStepId() {
    return stepId;
  }

  public void setStepId(Integer stepId) {
    this.stepId = stepId;
  }

  public String getStepData() {
    return stepData;
  }

  public void setStepData(String stepData) {
    this.stepData = stepData;
  }

  public Integer getStepSort() {
    return stepSort;
  }

  public void setStepSort(Integer stepSort) {
    this.stepSort = stepSort;
  }

  public Integer getStepType() {
    return stepType;
  }

  public void setStepType(Integer stepType) {
    this.stepType = stepType;
  }

  public String getStepDataBk() {
    return stepDataBk;
  }

  public void setStepDataBk(String stepDataBk) {
    this.stepDataBk = stepDataBk;
  }
}
