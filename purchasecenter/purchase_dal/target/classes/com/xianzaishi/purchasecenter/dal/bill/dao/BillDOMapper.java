package com.xianzaishi.purchasecenter.dal.bill.dao;

import com.xianzaishi.purchasecenter.client.bill.dto.BillInformationDTO;
import com.xianzaishi.purchasecenter.dal.bill.dataobject.BillDO;
import com.xianzaishi.purchasecenter.dal.bill.dataobject.BillUserDO;
import java.util.List;
import org.apache.ibatis.annotations.Param;

/**
 * 用户发票
 * @author quyang 2017-2-13 11:54:25
 *
 */
public interface BillDOMapper {

  int insert(BillDO billDO);

  /**
   * 根据用户id查询发票信息
   * @param userId
   * @return
   */
  BillDO selectById(@Param("userId") Long userId);


  /**
   * 根据用户发票号查询
   * @param billNumber
   * @return
   */
  BillDO selectByBillNumber(@Param("billNumber") Long billNumber);

//  List<UserDO> selectIdList(@Param("userIdList") List<Integer> userIdList);

  /**
   * 修改发票数据数据
   * @param listDO
   * @return
   */
  int updateListDO(BillDO listDO);

  /**
   * 根据订单id查询某条发票信息
   * @param orderId
   * @return
   */
  BillInformationDTO selectBillByOrderId(Long orderId);

  /**
   * 批量插入数据
   */
  int batchBillVO(List<BillUserDO> billUserList);

  /**
   * 查询用户发票列表
   * @param userId
   * @return
   */
  List<BillDO> queryBillList(Integer userId);
}
