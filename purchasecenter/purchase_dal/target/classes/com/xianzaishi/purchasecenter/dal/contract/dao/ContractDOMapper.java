package com.xianzaishi.purchasecenter.dal.contract.dao;

import com.xianzaishi.purchasecenter.dal.contract.dataobject.ContractDO;

/**
 * 合同接口
 * @author dongpo
 *
 */
public interface ContractDOMapper {

  public ContractDO selectContractByContractId(Integer contractId);

  public ContractDO selectContractBySupplierId(Integer supplierId);

  public int insertContract(ContractDO contractDO);

  public int updateContract(ContractDO contractDO);
}
