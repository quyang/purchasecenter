package com.xianzaishi.purchasecenter.dal.equipment.dao;

import java.util.List;

import com.xianzaishi.purchasecenter.dal.equipment.dataobject.EmployeeEquipmentDO;

public interface EmployeeEquipmentDOMapper {

  public List<EmployeeEquipmentDO> selectByUserId(Integer userId);

  public int insertEmployeeEquipment(EmployeeEquipmentDO emEquipmentDO);
}
