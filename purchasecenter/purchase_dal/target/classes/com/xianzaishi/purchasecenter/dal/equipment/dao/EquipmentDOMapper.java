package com.xianzaishi.purchasecenter.dal.equipment.dao;

import java.util.List;

import com.xianzaishi.purchasecenter.dal.equipment.dataobject.EquipmentDO;

public interface EquipmentDOMapper {

  public EquipmentDO selectEquipmentById(Integer equipmentId);

  public List<EquipmentDO> selectEquipmentByName(String name);

  public int insertEquipment(EquipmentDO equipmentDO);

  public int updateEquipment(EquipmentDO equipmentDO);
}
