package com.xianzaishi.purchasecenter.dal.logmonitor.dao;

import com.xianzaishi.purchasecenter.dal.logmonitor.dataobject.LogMonitorDO;
import java.util.List;

public interface LogMonitorDOMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table log_monitor
     *
     * @mbggenerated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table log_monitor
     *
     * @mbggenerated
     */
    int insert(LogMonitorDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table log_monitor
     *
     * @mbggenerated
     */
    int insertSelective(LogMonitorDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table log_monitor
     *
     * @mbggenerated
     */
    LogMonitorDO selectByPrimaryKey(Integer id);
    
    /**
     * 根据监控名查询监控信息
     * @param name
     * @return
     */
    LogMonitorDO selectByName(String name);
    
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table log_monitor
     *
     * @mbggenerated
     */
    int updateByPrimaryKeySelective(LogMonitorDO record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table log_monitor
     *
     * @mbggenerated
     */
    int updateByPrimaryKey(LogMonitorDO record);

    /**
     * 查询监控信息
     * @param logMonitorDO
     * @return
     */
    List<LogMonitorDO> select(LogMonitorDO logMonitorDO);

}