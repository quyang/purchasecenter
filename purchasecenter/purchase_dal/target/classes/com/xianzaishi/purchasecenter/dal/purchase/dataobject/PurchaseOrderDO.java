package com.xianzaishi.purchasecenter.dal.purchase.dataobject;

import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;

public class PurchaseOrderDO extends PurchaseOrderDTO {

  private static final long serialVersionUID = -1116486080918989153L;
  
  /**
   * 子采购单id列表 TODO
   */
  private String subOrderId;

  public String getSubOrderId() {
    return subOrderId;
  }

  public void setSubOrderId(String subOrderId) {
    this.subOrderId = subOrderId == null ? null : subOrderId.trim();
  }
}
