package com.xianzaishi.purchasecenter.dal.purchase.dataobject;

import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseQualityDTO;

public class PurchaseQualityDO extends PurchaseQualityDTO {

  private static final long serialVersionUID = -4063687833232613928L;

  /**
   * 图片合并 TODO
   */
  private String pic;

  public String getPic() {
    return pic;
  }

  public void setPic(String pic) {
    this.pic = pic == null ? null : pic.trim();
  }

}
