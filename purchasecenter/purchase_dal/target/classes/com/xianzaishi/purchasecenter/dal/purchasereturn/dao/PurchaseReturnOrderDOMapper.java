package com.xianzaishi.purchasecenter.dal.purchasereturn.dao;

import com.xianzaishi.purchasecenter.dal.purchasereturn.dataobject.PurchaseReturnOrderDO;

public interface PurchaseReturnOrderDOMapper {
  int deleteByPrimaryKey(Integer returnId);

  int insert(PurchaseReturnOrderDO record);

  int insertSelective(PurchaseReturnOrderDO record);

  PurchaseReturnOrderDO selectByPrimaryKey(Integer returnId);

  int updateByPrimaryKeySelective(PurchaseReturnOrderDO record);

  int updateByPrimaryKey(PurchaseReturnOrderDO record);
}
