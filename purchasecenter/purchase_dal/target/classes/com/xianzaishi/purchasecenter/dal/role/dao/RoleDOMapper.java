package com.xianzaishi.purchasecenter.dal.role.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.dal.role.dataobject.RoleDO;

public interface RoleDOMapper {

  public List<RoleDO> selectRole(@Param("roleIdList") List<Integer> roleIdList);

  public RoleDO selectRoleById(@Param("roleId") Integer roleId);

  public RoleDO selectRoleByName(@Param("name") String name);

  public int insertRole(RoleDO roleDO);

  public int updateRole(RoleDO roleDO);
}
