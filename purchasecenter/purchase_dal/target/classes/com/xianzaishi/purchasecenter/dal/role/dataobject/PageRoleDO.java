package com.xianzaishi.purchasecenter.dal.role.dataobject;

public class PageRoleDO {
  private String url;

  private String role;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url == null ? null : url.trim();
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role == null ? null : role.trim();
  }
}
