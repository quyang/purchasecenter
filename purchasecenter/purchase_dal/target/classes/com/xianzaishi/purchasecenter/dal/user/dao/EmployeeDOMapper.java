package com.xianzaishi.purchasecenter.dal.user.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.client.user.query.EmployeeQuery;
import com.xianzaishi.purchasecenter.dal.user.dataobject.EmployeeDO;

public interface EmployeeDOMapper {

  int insert(EmployeeDO employeeDO);

  EmployeeDO selectById(@Param("userId") Integer userId);

  EmployeeDO selectByName(@Param("name") String name);

  List<EmployeeDO> select(@Param("query") EmployeeQuery query);
  
  List<EmployeeDO>  selectByAllId(@Param("employIdList") List<Integer> id);

  int update(EmployeeDO employeeDO);
}
