package com.xianzaishi.purchasecenter.dal.user.dao.supplier;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.client.user.query.SupplierQuery;
import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierCompanyDO;

public interface SupplierCompanyDOMapper {

  int insert(SupplierCompanyDO supplierCompanyDO);

  SupplierCompanyDO select(@Param("supplierId") Integer supplierId);

  List<SupplierCompanyDO> selectIdList(@Param("supplierIdList") List<Integer> supplierId);
  
  List<SupplierCompanyDO> selectByCompany(@Param("companyName") String companyName);

  int update(SupplierCompanyDO supplierCompanyDO);
  
  int count(@Param("query") SupplierQuery query);
}
