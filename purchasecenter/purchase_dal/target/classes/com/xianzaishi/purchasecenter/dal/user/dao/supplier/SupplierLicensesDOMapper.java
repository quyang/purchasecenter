package com.xianzaishi.purchasecenter.dal.user.dao.supplier;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierLicensesDO;

public interface SupplierLicensesDOMapper {

  int insert(SupplierLicensesDO supplierLicensesDO);

  int insertBatch(@Param("licensesDOList") List<SupplierLicensesDO> supplierLicensesDOList);
  
  List<SupplierLicensesDO> select(@Param("supplierId") Integer supplierId);

  List<SupplierLicensesDO> selectIdList(@Param("supplierIdList") List<Integer> supplierId);
  
  int update(SupplierLicensesDO supplierLicensesDO);
  
  int updateBatch(@Param("licensesDOList") List<SupplierLicensesDO> supplierLicensesDOList);
}
