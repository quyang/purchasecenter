package com.xianzaishi.purchasecenter.dal.user.dao.supplier;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.xianzaishi.purchasecenter.dal.user.dataobject.supplier.SupplierSkuDO;

public interface SupplierSkuDOMapper {

  int insert(SupplierSkuDO supplierSkuDO);

  SupplierSkuDO selectById(@Param("skuId") Long skuId, @Param("supplierId") Integer supplierId);

  List<SupplierSkuDO> select(@Param("skuId") Long skuId);

  int update(SupplierSkuDO supplierSkuDO);
}
