package com.xianzaishi.purchasecenter.dal.user.dataobject.supplier;

import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierDTO;

public class SupplierDO extends SupplierDTO{

  private static final long serialVersionUID = 1L;
  
  private String checkinfo;
  
  public String getCheckinfo() {
    return checkinfo;
  }

  //TODO join SupplierDTO property
  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo == null ? null : checkinfo.trim();
  }
}