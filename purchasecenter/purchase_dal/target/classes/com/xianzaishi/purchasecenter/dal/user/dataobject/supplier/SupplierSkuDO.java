package com.xianzaishi.purchasecenter.dal.user.dataobject.supplier;

import com.xianzaishi.purchasecenter.client.user.dto.supplier.SupplierSkuDTO;

public class SupplierSkuDO extends  SupplierSkuDTO {

  private static final long serialVersionUID = 1L;

  /**
   * 审核原因，TODO 转化字段聚合
   */
  private String checkinfo;

  public String getCheckinfo() {
    return checkinfo;
  }

  public void setCheckinfo(String checkinfo) {
    this.checkinfo = checkinfo;
  }
  
}
