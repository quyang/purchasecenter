package com.xianzaishi.itemcentrer;

import com.caucho.hessian.client.HessianProxyFactory;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.logmonitor.LogMonitorService;
import com.xianzaishi.purchasecenter.client.logmonitor.dto.LogMonitorDTO;
import com.xianzaishi.purchasecenter.client.marketactivity.MarketActivityService;
import com.xianzaishi.purchasecenter.client.marketactivity.dto.MarketActivityDTO;
import com.xianzaishi.purchasecenter.client.marketactivity.query.MarketActivityQuery;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import org.junit.Test;

public class AppTest {


  public static void main(String[] args) throws MalformedURLException {
    /*
     * <servlet> <!-- 配置 HessianServlet，Servlet的名字随便配置，例如这里配置成ServiceServlet-->
     * <servlet-name>ServiceServlet</servlet-name>
     * <servlet-class>com.caucho.hessian.server.HessianServlet</servlet-class>
     * 
     * <!-- 配置接口的具体实现类 --> <init-param> <param-name>service-class</param-name>
     * <param-value>gacl.hessian.service.impl.ServiceImpl</param-value> </init-param> </servlet>
     * <!-- 映射 HessianServlet的访问URL地址--> <servlet-mapping>
     * <servlet-name>ServiceServlet</servlet-name> <url-pattern>/ServiceServlet</url-pattern>
     * </servlet-mapping>
     */
    // 在服务器端的web.xml文件中配置的HessianServlet映射的访问URL地址
    String url = "http://localhost/purchasecenter/hessian/logMonitorService";
    HessianProxyFactory factory = new HessianProxyFactory();
    LogMonitorService service = (LogMonitorService) factory
        .create(LogMonitorService.class, url);// 创建IService接口的实例对象
    // String i = service.sayHello("111111");
    // System.out.println(i);
//    List<Integer> userIdList = Lists.newArrayList();
//    userIdList.add(10);
//    Result<List<BackGroundUserDTO>> userResult =
//        service.queryUserDTOByIdList(userIdList);
    LogMonitorDTO logMonitorDTO = new LogMonitorDTO();
    logMonitorDTO.setName("1号店服务器");
    logMonitorDTO.setInfo("abcddsfaldsa");
    Result<Integer> result = service.insertLog(logMonitorDTO);
    System.out.println(JackSonUtil.getJson(result));
  }


  @Test
  public void MarketActivityServiceInsert() {
    String url = "http://localhost:8088/purchasecenter/hessian/marketActivityService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      MarketActivityService service = (MarketActivityService) factory
          .create(MarketActivityService.class, url);

      MarketActivityDTO marketActivityDTO = new MarketActivityDTO();
      marketActivityDTO.setGmtStart(new Date());
      marketActivityDTO.setGmtEnd(new Date());
      marketActivityDTO.setType((short) 2);
      marketActivityDTO.setName("asdddd");
      marketActivityDTO.setStatus((short) 3);
      Result<Boolean> insert = service.insert(marketActivityDTO);
      System.out.println(JackSonUtil.getJson(insert));
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }


  @Test
  public void MarketActivityServiceQuery() {
    String url = "http://localhost:8088/purchasecenter/hessian/marketActivityService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      MarketActivityService service = (MarketActivityService) factory
          .create(MarketActivityService.class, url);
      MarketActivityQuery marketActivityQuery = new MarketActivityQuery();
      marketActivityQuery.setType((short)8);
      Result<List<MarketActivityDTO>> query = service.query(marketActivityQuery);
      System.out.println(JackSonUtil.getJson(query));
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }
  
  @Test
  public void MarketActivityServicequeryCount() {
    String url = "http://localhost:8088/purchasecenter/hessian/marketActivityService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      MarketActivityService service = (MarketActivityService) factory
          .create(MarketActivityService.class, url);
      Result<Integer> query = service.queryCount(MarketActivityQuery.queryByStartAndEndTime(getRightDate("2016-10-09 09:09:09"),getRightDate("2017-04-27 14:15:10")));
      System.out.println(JackSonUtil.getJson(query));
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }
  
  public Date getRightDate(String date) {
      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      try {
        return sdf.parse(date);
      } catch (ParseException e) {
        e.printStackTrace();
      }
      
      return null;
  }


  @Test
  public void MarketActivityServiceUpdate() {
    String url = "http://localhost:8088/purchasecenter/hessian/marketActivityService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      MarketActivityService service = (MarketActivityService) factory
          .create(MarketActivityService.class, url);
      MarketActivityQuery marketActivityQuery = new MarketActivityQuery();
      marketActivityQuery.setName("nihao");
      Result<List<MarketActivityDTO>> query = service.query(marketActivityQuery);
      MarketActivityDTO activityDTO = query.getModule().get(0);
      activityDTO.setName("8888");
      Result<Boolean> update = service.update(activityDTO);

      System.out.println(JackSonUtil.getJson(update));

    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }


  @Test
  public void MarketActivityServiceDelete() {
    String url = "http://localhost:8088/purchasecenter/hessian/marketActivityService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      MarketActivityService service = (MarketActivityService) factory
          .create(MarketActivityService.class, url);
      Result<Boolean> update = service.delete(1);
      System.out.println(JackSonUtil.getJson(update));

    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }


  @Test
  public void MarketActivityServiceQueryByStartAndEnd() {
    String url = "http://localhost:8088/purchasecenter/hessian/marketActivityService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      MarketActivityService service = (MarketActivityService) factory
          .create(MarketActivityService.class, url);

      MarketActivityDTO marketActivityDTO = new MarketActivityDTO();

      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      Date start = null;
      Date end = null;
      try {
        start = sdf.parse("2017-04-14 01:00:00");
        end = sdf.parse("2017-04-19 01:00:00");
      } catch (ParseException e) {
        e.printStackTrace();
      }

      marketActivityDTO.setGmtStart(start);
      marketActivityDTO.setGmtEnd(end);
//      Result<List<MarketActivityDTO>> listResult = service.queryByStartAndEnd(marketActivityDTO);
//      System.out.println(JackSonUtil.getJson(listResult));
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  

}
