package com.xianzaishi.purchasecenter;

import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;

import com.caucho.hessian.client.HessianProxyFactory;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.mail.MailService;
import com.xianzaishi.purchasecenter.client.mail.dto.SheetDTO;

public class AppTest {


  public static void main(String[] args) throws MalformedURLException {
    /*
     * <servlet> <!-- 配置 HessianServlet，Servlet的名字随便配置，例如这里配置成ServiceServlet-->
     * <servlet-name>ServiceServlet</servlet-name>
     * <servlet-class>com.caucho.hessian.server.HessianServlet</servlet-class>
     * 
     * <!-- 配置接口的具体实现类 --> <init-param> <param-name>service-class</param-name>
     * <param-value>gacl.hessian.service.impl.ServiceImpl</param-value> </init-param> </servlet>
     * <!-- 映射 HessianServlet的访问URL地址--> <servlet-mapping>
     * <servlet-name>ServiceServlet</servlet-name> <url-pattern>/ServiceServlet</url-pattern>
     * </servlet-mapping>
     */
    // 在服务器端的web.xml文件中配置的HessianServlet映射的访问URL地址
     String url = "http://localhost/purchasecenter/hessian/mailService";
     HessianProxyFactory factory = new HessianProxyFactory();
     MailService service = (MailService) factory.create(MailService.class, url);// 创建IService接口的实例对象
     List<SheetDTO> sheetDTOs = Lists.newArrayList();
     SheetDTO sheetDTO = new SheetDTO();
     sheetDTO.setDatePattern("yyyy-MM-dd");
     List<String> columnNames = Lists.newArrayList();
     columnNames.add("列1");
     columnNames.add("列2");
     columnNames.add("列3");
     columnNames.add("列4");
     columnNames.add("列5");
     columnNames.add("列6");
     sheetDTO.setColumnNames(columnNames);
     sheetDTO.setSheetName("采购单");
     sheetDTO.setTitleActive("上海鲜在时精品生鲜会员店订单");
     sheetDTO.setAddress("上海浦东新区东方路1367号富都广场1F");
     sheetDTO.setOrderId(1);
     sheetDTO.setPurchaseDate("2017-05-03");
     List<Map<String, Object>> mapList = Lists.newArrayList();
     for(int i = 0; i < 2; i++){
       Map<String, Object> maps = Maps.newHashMap();
       maps.put("列1", "123ddddd");
       maps.put("列2", "12334444");
       maps.put("列3", "12334444");
       maps.put("列4", "12334444");
       maps.put("列5", "12334444");
       maps.put("列6", "12334444");
       mapList.add(maps);
     }
     sheetDTO.setRows(mapList);
     sheetDTOs.add(sheetDTO);
     Result<Boolean> result = service.createAttachment(sheetDTOs);
    System.out.println(JackSonUtil.getJson(result));
  }
}
