package com.xianzaishi.purchasecenter;

import com.caucho.hessian.client.HessianProxyFactory;
<<<<<<< HEAD
import com.xianzaishi.customercenter.client.customerservice.CustomerService;
import com.xianzaishi.customercenter.client.customerservice.dto.CustomerServiceTaskDTO;
import com.xianzaishi.customercenter.client.customerservice.query.CustomerserviceQuery;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import java.net.MalformedURLException;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.query.PurchaseQuery;
=======
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.PagedResult;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.logmonitor.LogMonitorService;
import com.xianzaishi.purchasecenter.client.purchase.PurchaseService;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.dto.PurchaseSubOrderDTO;
import com.xianzaishi.purchasecenter.client.purchase.query.PurchaseQuery;
import java.net.MalformedURLException;
>>>>>>> 8be544ef8a4dbb558a086ded2291730692937852
import java.util.Date;
import java.util.List;
import org.junit.Test;

/**
 * Created by quyang on 2017/3/29.
 */
public class MyTest {

  @Test
  public void customerServiceTest() {

    String url = "http://localhost:8088/purchasecenter/hessian/customerservice";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      CustomerService service = (CustomerService) factory.create(CustomerService.class, url);

      CustomerserviceQuery customerserviceQuery = new CustomerserviceQuery();
      customerserviceQuery.setBizId("100059");
      Result<List<CustomerServiceTaskDTO>> listResult = service
          .queryCustomerServiceTask(customerserviceQuery);
      System.out.println(JackSonUtil.getJson(listResult));

    } catch (MalformedURLException e) {
      e.printStackTrace();
      System.out.println("失败");
    }
  }


  @Test
  public void purchaseServiceQu() {
    String url = "http://localhost:8088/purchasecenter/hessian/purchaseService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      PurchaseService service = (PurchaseService) factory
          .create(PurchaseService.class, url);

      Result<List<PurchaseSubOrderDTO>> listResult = service.querySubPurchaseListByPurId(70);
      System.out.println("查询结果=" + JackSonUtil.getJson(listResult));

      List<PurchaseSubOrderDTO> module = listResult.getModule();

      for (PurchaseSubOrderDTO dto:module) {
        dto.setArriveDate(new Date());
        //更新子采购单
        Result<Boolean> booleanResult = service.updatePurchaseSubOrder(dto);
        System.out.println("更新结果=" + JackSonUtil.getJson(booleanResult));
      }

    } catch (MalformedURLException e) {
      System.out.println("MyTest.insertOrUpdate");
    }
  }


  @Test
  public void updatePurchaseOrder() {
    String url = "http://localhost:8088/purchasecenter/hessian/purchaseService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      PurchaseService service = (PurchaseService) factory
          .create(PurchaseService.class, url);

      PurchaseQuery purchaseQuery = new PurchaseQuery();
      purchaseQuery.setParentPurchaseId(70);
      System.out.println("查询条件=" + JackSonUtil.getJson(purchaseQuery));
      PagedResult<List<PurchaseOrderDTO>> listPagedResult = service.queryPurchase(purchaseQuery);
      System.out.println("查询结果=" + JackSonUtil.getJson(listPagedResult));

      PurchaseOrderDTO purchaseOrderDTO = listPagedResult.getModule().get(0);
      purchaseOrderDTO.setStorageId(123L);

      System.out.println("更新数据=" + JackSonUtil.getJson(purchaseOrderDTO));
      Result<Boolean> updatePurchaseOrder = service.updatePurchaseOrder(purchaseOrderDTO);
      System.out.println("更新结果=" + JackSonUtil.getJson(updatePurchaseOrder));

    } catch (MalformedURLException e) {
      System.out.println("MyTest.insertOrUpdate");
    }
  }

<<<<<<< HEAD
=======

  @Test
  public void purchaseServiceQu() {
    String url = "http://localhost:8088/purchasecenter/hessian/purchaseService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      PurchaseService service = (PurchaseService) factory
          .create(PurchaseService.class, url);

      Result<List<PurchaseSubOrderDTO>> listResult = service.querySubPurchaseListByPurId(70);
      System.out.println("查询结果=" + JackSonUtil.getJson(listResult));

      List<PurchaseSubOrderDTO> module = listResult.getModule();

      for (PurchaseSubOrderDTO dto:module) {
        dto.setArriveDate(new Date());
        //更新子采购单
        Result<Boolean> booleanResult = service.updatePurchaseSubOrder(dto);
        System.out.println("更新结果=" + JackSonUtil.getJson(booleanResult));
      }

    } catch (MalformedURLException e) {
      System.out.println("MyTest.insertOrUpdate");
    }
  }


  @Test
  public void updatePurchaseOrder() {
    String url = "http://localhost:8088/purchasecenter/hessian/purchaseService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      PurchaseService service = (PurchaseService) factory
          .create(PurchaseService.class, url);

      PurchaseQuery purchaseQuery = new PurchaseQuery();
      purchaseQuery.setParentPurchaseId(70);
      System.out.println("查询条件=" + JackSonUtil.getJson(purchaseQuery));
      PagedResult<List<PurchaseOrderDTO>> listPagedResult = service.queryPurchase(purchaseQuery);
      System.out.println("查询结果=" + JackSonUtil.getJson(listPagedResult));

      PurchaseOrderDTO purchaseOrderDTO = listPagedResult.getModule().get(0);
      purchaseOrderDTO.setStorageId(123L);

      System.out.println("更新数据=" + JackSonUtil.getJson(purchaseOrderDTO));
      Result<Boolean> updatePurchaseOrder = service.updatePurchaseOrder(purchaseOrderDTO);
      System.out.println("更新结果=" + JackSonUtil.getJson(updatePurchaseOrder));

    } catch (MalformedURLException e) {
      System.out.println("MyTest.insertOrUpdate");
    }
  }

>>>>>>> 8be544ef8a4dbb558a086ded2291730692937852
}
