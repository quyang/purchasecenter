package com.xianzaishi.purchasecentrer;

import com.caucho.hessian.client.HessianProxyFactory;
import com.xianzaishi.itemcenter.common.JackSonUtil;
import com.xianzaishi.itemcenter.common.result.Result;
import com.xianzaishi.purchasecenter.client.logmonitor.LogMonitorService;
import com.xianzaishi.purchasecenter.client.logmonitor.dto.LogMonitorDTO;
import com.xianzaishi.purchasecenter.client.logmonitor.query.LogMonitorQuery;
import com.xianzaishi.purchasecenter.client.logmonitorconfig.LogMonitorConfigService;
import com.xianzaishi.purchasecenter.client.logmonitorconfig.dto.LogMonitorConfigDTO;
import java.net.MalformedURLException;
import java.util.List;

/**
 * Hello world!
 */
public class App {

  public static void main(String[] args) {
    System.out.println("Hello World!");
    logMonitorConfigService();
//    // 在服务器端的web.xml文件中配置的HessianServlet映射的访问URL地址
    String url = "http://localhost:8088/purchasecenter/hessian/logMonitorService";
    HessianProxyFactory factory = new HessianProxyFactory();
    try {
      LogMonitorService orderService = (LogMonitorService) factory
          .create(LogMonitorService.class, url);
//
//      LogMonitorDTO logMonitorDTO = new LogMonitorDTO();
//      logMonitorDTO.setId(2);
//      logMonitorDTO.setName("dddddddddddddddddddddd");
//      logMonitorDTO.setStatus((short) 1);
//      Result<Boolean> updateLog = orderService.updateLog(logMonitorDTO);
//      System.out.println(updateLog);
////
//      LogMonitorQuery logMonitorQuery = new LogMonitorQuery();
//      logMonitorQuery.setName("quyang");
//
////      Result<List<LogMonitorDTO>> listResult = orderService.queryLog(logMonitorQuery);
//
//      LogMonitorDTO logMonitorDTO = new LogMonitorDTO();
//      logMonitorDTO.setName("quyang");
//      logMonitorDTO.setInfo("我是曲洋");
//      logMonitorDTO.setType((short)1);
//      Result<Integer> insertLog = orderService.insertLog(logMonitorDTO);

      LogMonitorQuery query = new LogMonitorQuery();
      query.setName("quyang");
      query.setStatus((short)1);
      Result<List<LogMonitorDTO>> queryLog = orderService.queryLog(query);

//      System.out.println(JackSonUtil.getJson(insertLog));
      System.out.println("查询="+JackSonUtil.getJson(queryLog));
    } catch (MalformedURLException e) {
      System.out.println("nihoa=" + e.getMessage());
      e.printStackTrace();
    }
  }


  public static void logMonitorConfigService() {
    System.out.println("Hello World!");
    // 在服务器端的web.xml文件中配置的HessianServlet映射的访问URL地址
    String url = "http://localhost:8088/purchasecenter/hessian/logMonitorConfigService";
    HessianProxyFactory factory = new HessianProxyFactory();
    LogMonitorConfigService service = null;
    try {
      service = (LogMonitorConfigService) factory.create(LogMonitorConfigService.class, url);
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    LogMonitorConfigDTO logMonitorConfigDTO = new LogMonitorConfigDTO();
//    logMonitorConfigDTO.setPhone("17742039272;17742039272;");
//    logMonitorConfigDTO.setTimeScope(2);
//    logMonitorConfigDTO.setType((short)2);
//    Result<Boolean> insert = service.insert(logMonitorConfigDTO);
//    System.out.println(JackSonUtil.getJson(insert));

    logMonitorConfigDTO.setType((short)5);
    Result<List<LogMonitorConfigDTO>> select = service.select(logMonitorConfigDTO);
    System.out.println(JackSonUtil.getJson(select));

    LogMonitorConfigDTO configDTO = select.getModule().get(0);
    configDTO.setPhone("17742039272;");

    Result<Boolean> update = service.update(configDTO);
    System.out.println(JackSonUtil.getJson(update));
  }


}
